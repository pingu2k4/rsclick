﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Parse;

namespace RSClick
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnStartup(StartupEventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
            Log.Info("App Startup.");
            base.OnStartup(e);

            ParseClient.Initialize("qBIhYOQRLldE2x74juZxdyq1iyFLIASp8iKrtcu2", "yDbcE70NrefwqNVDrUfs5BH4wslW10dSftOlWTdG");
        }

    }
}
