﻿using RSClick.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RSClick
{
    /// <summary>
    /// Interaction logic for DBManagement.xaml
    /// </summary>
    public partial class DBManagement : Window
    {
        public DBManagement()
        {
            InitializeComponent();
            DBManagementViewModel Data = new DBManagementViewModel(this);
            DataContext = Data;
        }
    }
}
