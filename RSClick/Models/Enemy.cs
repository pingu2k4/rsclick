﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RSClick.Models
{
    class Enemy : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Enemy()
        {
        }

        public Enemy(int EnemyID, string EnemyName, int EnemyMaxHP, int EnemyAttack, int EnemyDefence, int EnemyRange, int EnemyMagic, int EnemyBaseDPS, double EnemySplitXP, double EnemyHPXP, int EnemyMinGP, int EnemyMaxGP, weaknessEnum EnemyWeakness, attackStyleEnum EnemyAttackStyle, string EnemyImagePath)
        {
            ID = EnemyID;
            Name = EnemyName;
            Border = Color.FromRgb(0, 0, 0).ToString();
            MaxHP = EnemyMaxHP;

            AttackLevel = EnemyAttack;
            DefenceLevel = EnemyDefence;
            RangeLevel = EnemyRange;
            MagicLevel = EnemyMagic;

            BaseDPS = EnemyBaseDPS;

            SplitXP = EnemySplitXP;
            HPXP = EnemyHPXP;

            MinGP = EnemyMinGP;
            MaxGP = EnemyMaxGP;

            Weakness = EnemyWeakness;
            AttackStyle = EnemyAttackStyle;
            ImagePath = EnemyImagePath;
        }

        private int id;
        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("ID");
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private string border;
        public string Border
        {
            get
            {
                return border;
            }
            set
            {
                border = value;
                OnPropertyChanged("Border");
            }
        }

        private int maxHP;
        public int MaxHP
        {
            get
            {
                return maxHP;
            }
            set
            {
                maxHP = value;
                OnPropertyChanged("MaxHP");
            }
        }

        private int baseDPS;
        public int BaseDPS
        {
            get
            {
                return baseDPS;
            }
            set
            {
                baseDPS = value;
                OnPropertyChanged("BaseDPS");
            }
        }

        public enum weaknessEnum
        {
            None,
            Melee,
            Range,
            Magic
        }

        public enum attackStyleEnum
        {
            None,
            Melee,
            Range,
            Magic
        }

        private weaknessEnum weakness;
        public weaknessEnum Weakness
        {
            get
            {
                return weakness;
            }
            set
            {
                weakness = value;
                OnPropertyChanged("Weakness");
            }
        }

        private attackStyleEnum attackStyle;
        public attackStyleEnum AttackStyle
        {
            get
            {
                return attackStyle;
            }
            set
            {
                attackStyle = value;
                OnPropertyChanged("AttackStyle");
            }
        }

        private string imagePath;
        public string ImagePath
        {
            get
            {
                return "/RSClick;component/Images/Enemies/" + imagePath;
            }
            set
            {
                imagePath = value;
                OnPropertyChanged("ImagePath");
            }
        }

        private int attackLevel;
        public int AttackLevel
        {
            get
            {
                return attackLevel;
            }
            set
            {
                attackLevel = value;
                OnPropertyChanged("AttackLevel");
            }
        }

        private int defenceLevel;
        public int DefenceLevel
        {
            get
            {
                return defenceLevel;
            }
            set
            {
                defenceLevel = value;
                OnPropertyChanged("DefenceLevel");
            }
        }

        private int rangeLevel;
        public int RangeLevel
        {
            get
            {
                return rangeLevel;
            }
            set
            {
                rangeLevel = value;
                OnPropertyChanged("RangeLevel");
            }
        }

        private int magicLevel;
        public int MagicLevel
        {
            get
            {
                return magicLevel;
            }
            set
            {
                magicLevel = value;
                OnPropertyChanged("MagicLevel");
            }
        }

        private double splitXP;
        public double SplitXP
        {
            get
            {
                return splitXP;
            }
            set
            {
                splitXP = value;
                OnPropertyChanged("SplitXP");
            }
        }

        private double hpXP;
        public double HPXP
        {
            get
            {
                return hpXP;
            }
            set
            {
                hpXP = value;
                OnPropertyChanged("HPXP");
            }
        }

        private int minGP;
        public int MinGP
        {
            get
            {
                return minGP;
            }
            set
            {
                minGP = value;
                OnPropertyChanged("MinGP");
            }
        }

        private int maxGP;
        public int MaxGP
        {
            get
            {
                return maxGP;
            }
            set
            {
                maxGP = value;
                OnPropertyChanged("MaxGP");
            }
        }

        public static ObservableCollection<Enemy> GetAll()
        {
            Log.Debug("Enemies Loading...");
            ObservableCollection<Enemy> enemies = new ObservableCollection<Enemy>();

            try
            {
                var db = new SQLiteDatabase();
                DataTable dt = db.GetDataTable("Select * FROM Enemies");
                foreach (DataRow Row in dt.Rows)
                {
                    weaknessEnum TmpWeakness;
                    attackStyleEnum TmpAttackStyle;

                    #region Enum Switches
                    switch (Row["Weakness"].ToString())
                    {
                        case "None":
                            TmpWeakness = weaknessEnum.None;
                            break;
                        case "Melee":
                            TmpWeakness = weaknessEnum.Melee;
                            break;
                        case "Magic":
                            TmpWeakness = weaknessEnum.Magic;
                            break;
                        case "Range":
                            TmpWeakness = weaknessEnum.Range;
                            break;
                        default:
                            TmpWeakness = weaknessEnum.None;
                            Log.Error("GetAllEnemies switch on (Weakness) failed. Defualting None. Value used: " + Row["Weakness"].ToString());
                            break;
                    }
                    switch (Row["AttackStyle"].ToString())
                    {
                        case "None":
                            TmpAttackStyle = attackStyleEnum.None;
                            break;
                        case "Melee":
                            TmpAttackStyle = attackStyleEnum.Melee;
                            break;
                        case "Magic":
                            TmpAttackStyle = attackStyleEnum.Magic;
                            break;
                        case "Range":
                            TmpAttackStyle = attackStyleEnum.Range;
                            break;
                        default:
                            TmpAttackStyle = attackStyleEnum.None;
                            Log.Error("GetAllEnemies switch on (AttackStyle) failed. Defualting None. Value used: " + Row["AttackStyle"].ToString());
                            break;
                    }
                    #endregion

                    enemies.Add(new Enemy(
                        EnemyID: Convert.ToInt32(Row["ID"]),
                        EnemyName: Row["Name"].ToString(),
                        EnemyMaxHP: Convert.ToInt32(Row["MaxHP"]),
                        EnemyAttack: Convert.ToInt32(Row["Attack"]),
                        EnemyDefence: Convert.ToInt32(Row["Defence"]),
                        EnemyRange: Convert.ToInt32(Row["Range"]),
                        EnemyMagic: Convert.ToInt32(Row["Magic"]),
                        EnemyBaseDPS: Convert.ToInt32(Row["BaseDPS"]),
                        EnemySplitXP: Convert.ToDouble(Row["SplitXP"]),
                        EnemyHPXP: Convert.ToDouble(Row["HPXP"]),
                        EnemyMinGP: Convert.ToInt32(Row["MinGP"]),
                        EnemyMaxGP: Convert.ToInt32(Row["MaxGP"]),
                        EnemyWeakness: TmpWeakness,
                        EnemyAttackStyle: TmpAttackStyle,
                        EnemyImagePath: Row["ImagePath"].ToString()));
                }
            }
            catch (Exception err)
            {
                Log.Error("SQLITE LOAD ERR: " + err.Message, err);
            }

            Log.Debug("Enemies Loaded.");
            return enemies;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
