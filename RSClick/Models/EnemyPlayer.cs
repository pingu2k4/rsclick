﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSClick.Models
{
    class EnemyPlayer : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public EnemyPlayer()
        {

        }

        public EnemyPlayer(int EID, string EObjectID, bool EWilderness, int EWildernessLevel, int EAttack, int EStrength,
                           int EDefence, int EConstitution, int ERange, int EMagic, int ESummoning, int ECombat)
        {
            ID = EID;
            ObjectID = EObjectID;
            Wilderness = EWilderness;
            WildernessLevel = EWildernessLevel;
            Attack = EAttack;
            Strength = EStrength;
            Defence = EDefence;
            Constitution = EConstitution;
            Range = ERange;
            Magic = EMagic;
            Summoning = ESummoning;
            Combat = ECombat;
        }

        private int id;
        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("ID");
            }
        }

        private string objectID;
        public string ObjectID
        {
            get
            {
                return objectID;
            }
            set
            {
                objectID = value;
                OnPropertyChanged("ObjectID");
            }
        }

        private bool wilderness;
        public bool Wilderness
        {
            get
            {
                return wilderness;
            }
            set
            {
                wilderness = value;
                OnPropertyChanged("Wilderness");
            }
        }

        private int wildernessLevel;
        public int WildernessLevel
        {
            get
            {
                return wildernessLevel;
            }
            set
            {
                wildernessLevel = value;
                OnPropertyChanged("WildernessLevel");
            }
        }

        private int attack;
        public int Attack
        {
            get
            {
                return attack;
            }
            set
            {
                attack = value;
                OnPropertyChanged("Attack");
            }
        }

        private int strength;
        public int Strength
        {
            get
            {
                return strength;
            }
            set
            {
                strength = value;
                OnPropertyChanged("Strength");
            }
        }

        private int defence;
        public int Defence
        {
            get
            {
                return defence;
            }
            set
            {
                defence = value;
                OnPropertyChanged("Defence");
            }
        }

        private int constitution;
        public int Constitution
        {
            get
            {
                return constitution;
            }
            set
            {
                constitution = value;
                OnPropertyChanged("Constitution");
            }
        }

        private int range;
        public int Range
        {
            get
            {
                return range;
            }
            set
            {
                range = value;
                OnPropertyChanged("Range");
            }
        }

        private int magic;
        public int Magic
        {
            get
            {
                return magic;
            }
            set
            {
                magic = value;
                OnPropertyChanged("Magic");
            }
        }

        private int summoning;
        public int Summoning
        {
            get
            {
                return summoning;
            }
            set
            {
                summoning = value;
                OnPropertyChanged("Summoning");
            }
        }

        private int combat;
        public int Combat
        {
            get
            {
                return combat;
            }
            set
            {
                combat = value;
                OnPropertyChanged("Combat");
            }
        }

        public string BuyButtonText
        {
            get
            {
                return "Attack " + ObjectID + "!";
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
