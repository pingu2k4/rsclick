﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSClick.Models
{
    class Equipment : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Equipment()
        {

        }

        public Equipment(int EID, string EName, Category ECategory, Type EType, Slot ESlot,
            ReqSkill ERequiredSkill, int ERequiredLevel, int EDamage, int EAccuracy, int EArmour, int ELifePoints,
            int EPrayer, int EStrBonus, int ERangeBonus, int EMagicBonus, int ECost,
            string EExamine, string EImagePath)
        {
            ID = EID;
            Name = EName;
            EquipmentCategory = ECategory;
            EquipmentType = EType;
            EquipmentSlot = ESlot;
            RequiredSkill = ERequiredSkill;
            RequiredLevel = ERequiredLevel;
            Damage = EDamage;
            Accuracy = EAccuracy;
            Armour = EArmour;
            LifePoints = ELifePoints;
            Prayer = EPrayer;
            StrBonus = EStrBonus;
            RangeBonus = ERangeBonus;
            MagicBonus = EMagicBonus;
            Cost = ECost;
            Examine = EExamine;
            ImagePath = EImagePath;
        }

        private int id;
        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("ID");
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public enum Category
        {
            Bronze,
            Misc
        }
        private Category equipmentCategory;
        public Category EquipmentCategory
        {
            get
            {
                return equipmentCategory;
            }
            set
            {
                equipmentCategory = value;
                OnPropertyChanged("EquipmentCategory");
            }
        }

        public enum Type
        {
            Melee,
            Range,
            Magic
        }
        private Type equipmentType;
        public Type EquipmentType
        {
            get
            {
                return equipmentType;
            }
            set
            {
                equipmentType = value;
                OnPropertyChanged("EquipmentType");
            }
        }

        public enum Slot
        {
            Aura,
            Head,
            Pocket,
            Cape,
            Neck,
            Ammo,
            Weapon,
            Body,
            Shield,
            Legs,
            Hands,
            Feet,
            Ring,
            Twohand
        }
        private Slot equipmentSlot;
        public Slot EquipmentSlot
        {
            get
            {
                return equipmentSlot;
            }
            set
            {
                equipmentSlot = value;
                OnPropertyChanged("EquipmentSlot");
            }
        }

        public enum ReqSkill
        {
            Attack,
            Defence
        }
        private ReqSkill requiredSkill;
        public ReqSkill RequiredSkill
        {
            get
            {
                return requiredSkill;
            }
            set
            {
                requiredSkill = value;
                OnPropertyChanged("RequiredSkill");
            }
        }

        private int requiredLevel;
        public int RequiredLevel
        {
            get
            {
                return requiredLevel;
            }
            set
            {
                requiredLevel = value;
                OnPropertyChanged("RequiredLevel");
            }
        }

        private int damage;
        public int Damage
        {
            get
            {
                return damage;
            }
            set
            {
                damage = value;
                OnPropertyChanged("Damage");
            }
        }

        private int accuracy;
        public int Accuracy
        {
            get
            {
                return accuracy;
            }
            set
            {
                accuracy = value;
                OnPropertyChanged("Accuracy");
            }
        }

        private int armour;
        public int Armour
        {
            get
            {
                return armour;
            }
            set
            {
                armour = value;
                OnPropertyChanged("Armour");
            }
        }

        private int lifePoints;
        public int LifePoints
        {
            get
            {
                return lifePoints;
            }
            set
            {
                lifePoints = value;
                OnPropertyChanged("LifePoints");
            }
        }

        private int prayer;
        public int Prayer
        {
            get
            {
                return prayer;
            }
            set
            {
                prayer = value;
                OnPropertyChanged("Prayer");
            }
        }

        private int strBonus;
        public int StrBonus
        {
            get
            {
                return strBonus;
            }
            set
            {
                strBonus = value;
                OnPropertyChanged("StrBonus");
            }
        }

        private int rangeBonus;
        public int RangeBonus
        {
            get
            {
                return rangeBonus;
            }
            set
            {
                rangeBonus = value;
                OnPropertyChanged("rangeBonus");
            }
        }

        private int magicBonus;
        public int MagicBonus
        {
            get
            {
                return magicBonus;
            }
            set
            {
                magicBonus = value;
                OnPropertyChanged("MagicBonus");
            }
        }

        private int cost;
        public int Cost
        {
            get
            {
                return cost;
            }
            set
            {
                cost = value;
                OnPropertyChanged("Cost");
            }
        }

        private string examine;
        public string Examine
        {
            get
            {
                return examine;
            }
            set
            {
                examine = value;
                OnPropertyChanged("Examine");
            }
        }

        private string imagePath;
        public string ImagePath
        {
            get
            {
                return "/RSClick;component/Images/Armoury/" + imagePath;
            }
            set
            {
                imagePath = value;
                OnPropertyChanged("ImagePath");
            }
        }

        public string ClassImage
        {
            get
            {
                string val;
                switch (EquipmentType)
                {
                    case Type.Melee:
                        val = "/RSClick;component/Images/Skills/Attack.png";
                        break;
                    case Type.Range:
                        val = "/RSClick;component/Images/Skills/Range.png";
                        break;
                    case Type.Magic:
                        val = "/RSClick;component/Images/Skills/Magic.png";
                        break;
                    default:
                        Log.Error("ClassImage fell through the switch! Needs updating for new Class. Defaulting to attack.");
                        val = "/RSClick;component/Images/Skills/Attack.png";
                        break;
                }
                return val;
            }
            set { }
        }

        public string SlotString
        {
            get
            {
                string val;
                switch (EquipmentSlot)
                {
                    case Slot.Aura:
                        val = "Aura";
                        break;
                    case Slot.Head:
                        val = "Head";
                        break;
                    case Slot.Pocket:
                        val = "Pocket";
                        break;
                    case Slot.Cape:
                        val = "Cape";
                        break;
                    case Slot.Neck:
                        val = "Neck";
                        break;
                    case Slot.Ammo:
                        val = "Ammo";
                        break;
                    case Slot.Weapon:
                        val = "Weapon";
                        break;
                    case Slot.Body:
                        val = "Body";
                        break;
                    case Slot.Shield:
                        val = "Shield";
                        break;
                    case Slot.Twohand:
                        val = "Two-Handed";
                        break;
                    case Slot.Legs:
                        val = "Legs";
                        break;
                    case Slot.Hands:
                        val = "Hands";
                        break;
                    case Slot.Feet:
                        val = "Feet";
                        break;
                    case Slot.Ring:
                        val = "Ring";
                        break;
                    default:
                        Log.Error("SlotString fell through the switch! Needs updating for new weapon slots. Defaulting to Main Hand.");
                        val = "Main Hand";
                        break;
                }
                return val;
            }
            set { }
        }

        public string BuyNowButtonText
        {
            get
            {
                return "Buy Now! (" + String.Format("{0:n0}", Cost) + " GP)";
            }
            set { }
        }

        public string ReqSkillImage
        {
            get
            {
                string val;
                switch (RequiredSkill)
                {
                    case ReqSkill.Attack:
                        val = "/RSClick;component/Images/Skills/Attack.png";
                        break;
                    case ReqSkill.Defence:
                        val = "/RSClick;component/Images/Skills/Defence.png";
                        break;
                    default:
                        Log.Error("ReqSkillImage fell through the switch! Needs updating for new Class. Defaulting to attack.");
                        val = "/RSClick;component/Images/Skills/Attack.png";
                        break;
                }
                return val;
            }
        }

        public static ObservableCollection<Equipment> GetAllWeapons()
        {

            Log.Debug("Begin Loading Weapons...");
            ObservableCollection<Equipment> weapons = new ObservableCollection<Equipment>();

            try
            {
                var db = new SQLiteDatabase();
                DataTable dt = db.GetDataTable("Select * FROM Weapons");
                foreach (DataRow Row in dt.Rows)
                {
                    Category TmpCat;
                    Type TmpType;
                    Slot TmpSlot;
                    ReqSkill TmpReqSkill;

                    #region Enum Switches
                    switch (Row["Category"].ToString())
                    {
                        case "Bronze":
                            TmpCat = Category.Bronze;
                            break;
                        case "Misc":
                            TmpCat = Category.Misc;
                            break;
                        default:
                            TmpCat = Category.Misc;
                            Log.Error("GetAllWeapons switch on Value(2) (Category) failed. Defualting Misc. Value used: " + Row["Category"].ToString());
                            break;
                    }

                    switch (Row["Type"].ToString())
                    {
                        case "Melee":
                            TmpType = Type.Melee;
                            break;
                        case "Range":
                            TmpType = Type.Range;
                            break;
                        case "Magic":
                            TmpType = Type.Magic;
                            break;
                        default:
                            TmpType = Type.Melee;
                            Log.Error("GetAllWeapons switch on Value(3) (Type) failed. Defualting Melee. Value used: " + Row["Type"].ToString());
                            break;
                    }

                    switch (Row["Slot"].ToString())
                    {
                        case "Aura":
                            TmpSlot = Slot.Aura;
                            break;
                        case "Head":
                            TmpSlot = Slot.Head;
                            break;
                        case "Pocket":
                            TmpSlot = Slot.Pocket;
                            break;
                        case "Cape":
                            TmpSlot = Slot.Cape;
                            break;
                        case "Neck":
                            TmpSlot = Slot.Neck;
                            break;
                        case "Ammo":
                            TmpSlot = Slot.Ammo;
                            break;
                        case "Weapon":
                            TmpSlot = Slot.Weapon;
                            break;
                        case "Body":
                            TmpSlot = Slot.Body;
                            break;
                        case "Shield":
                            TmpSlot = Slot.Shield;
                            break;
                        case "Legs":
                            TmpSlot = Slot.Legs;
                            break;
                        case "Hands":
                            TmpSlot = Slot.Hands;
                            break;
                        case "Feet":
                            TmpSlot = Slot.Feet;
                            break;
                        case "Ring":
                            TmpSlot = Slot.Ring;
                            break;
                        case "Twohand":
                            TmpSlot = Slot.Twohand;
                            break;
                        default:
                            TmpSlot = Slot.Weapon;
                            Log.Error("GetAllWeapons switch on Value(4) (Slot) failed. Defualting Weapon. Value used: " + Row["Slot"].ToString());
                            break;
                    }

                    switch (Row["RequiredSkill"].ToString())
                    {
                        case "Attack":
                            TmpReqSkill = ReqSkill.Attack;
                            break;
                        case "Defence":
                            TmpReqSkill = ReqSkill.Defence;
                            break;
                        default:
                            TmpReqSkill = ReqSkill.Attack;
                            Log.Error("GetAllWeapons switch on Value(5) (Req Skill) failed. Defualting Attack. Value used: " + Row["RequiredSkill"].ToString());
                            break;
                    }
                    #endregion

                    weapons.Add(new Equipment(
                        EID: Convert.ToInt32(Row["ID"]),
                        EName: Row["Name"].ToString(),
                        ECategory: TmpCat,
                        EType: TmpType,
                        ESlot: TmpSlot,
                        ERequiredSkill: TmpReqSkill,
                        ERequiredLevel: Convert.ToInt32(Row["RequiredLevel"]),
                        EDamage: Convert.ToInt32(Row["Damage"]),
                        EAccuracy: Convert.ToInt32(Row["Accuracy"]),
                        EArmour: Convert.ToInt32(Row["Armour"]),
                        ELifePoints: Convert.ToInt32(Row["LifePoints"]),
                        EPrayer: Convert.ToInt32(Row["Prayer"]),
                        EStrBonus: Convert.ToInt32(Row["StrBonus"]),
                        ERangeBonus: Convert.ToInt32(Row["RangeBonus"]),
                        EMagicBonus: Convert.ToInt32(Row["MagicBonus"]),
                        ECost: Convert.ToInt32(Row["Cost"]),
                        EExamine: Row["Examine"].ToString(),
                        EImagePath: "Weapons/" + Row["ImagePath"].ToString()
                        ));
                }
            }
            catch (Exception err)
            {
                Log.Error("SQLITE LOAD ERR: " + err.Message, err);
            }

            Log.Debug("Weapons Loaded.");
            return weapons;
        }

        public static ObservableCollection<Equipment> GetAllArmour()
        {
            Log.Debug("Begin Loading Armour...");
            ObservableCollection<Equipment> armours = new ObservableCollection<Equipment>();

            try
            {
                var db = new SQLiteDatabase();
                DataTable dt = db.GetDataTable("Select * FROM Armours");
                foreach (DataRow Row in dt.Rows)
                {
                    Category TmpCat;
                    Type TmpType;
                    Slot TmpSlot;
                    ReqSkill TmpReqSkill;

                    #region Enum Switches
                    switch (Row["Category"].ToString())
                    {
                        case "Bronze":
                            TmpCat = Category.Bronze;
                            break;
                        case "Misc":
                            TmpCat = Category.Misc;
                            break;
                        default:
                            TmpCat = Category.Misc;
                            Log.Error("GetAllWeapons switch on Value(2) (Category) failed. Defualting Misc. Value used: " + Row["Category"].ToString());
                            break;
                    }

                    switch (Row["Type"].ToString())
                    {
                        case "Melee":
                            TmpType = Type.Melee;
                            break;
                        case "Range":
                            TmpType = Type.Range;
                            break;
                        case "Magic":
                            TmpType = Type.Magic;
                            break;
                        default:
                            TmpType = Type.Melee;
                            Log.Error("GetAllWeapons switch on Value(3) (Type) failed. Defualting Melee. Value used: " + Row["Type"].ToString());
                            break;
                    }

                    switch (Row["Slot"].ToString())
                    {
                        case "Aura":
                            TmpSlot = Slot.Aura;
                            break;
                        case "Head":
                            TmpSlot = Slot.Head;
                            break;
                        case "Pocket":
                            TmpSlot = Slot.Pocket;
                            break;
                        case "Cape":
                            TmpSlot = Slot.Cape;
                            break;
                        case "Neck":
                            TmpSlot = Slot.Neck;
                            break;
                        case "Ammo":
                            TmpSlot = Slot.Ammo;
                            break;
                        case "Weapon":
                            TmpSlot = Slot.Weapon;
                            break;
                        case "Body":
                            TmpSlot = Slot.Body;
                            break;
                        case "Shield":
                            TmpSlot = Slot.Shield;
                            break;
                        case "Legs":
                            TmpSlot = Slot.Legs;
                            break;
                        case "Hands":
                            TmpSlot = Slot.Hands;
                            break;
                        case "Feet":
                            TmpSlot = Slot.Feet;
                            break;
                        case "Ring":
                            TmpSlot = Slot.Ring;
                            break;
                        case "Twohand":
                            TmpSlot = Slot.Twohand;
                            break;
                        default:
                            TmpSlot = Slot.Weapon;
                            Log.Error("GetAllWeapons switch on Value(4) (Slot) failed. Defualting Weapon. Value used: " + Row["Slot"].ToString());
                            break;
                    }

                    switch (Row["RequiredSkill"].ToString())
                    {
                        case "Attack":
                            TmpReqSkill = ReqSkill.Attack;
                            break;
                        case "Defence":
                            TmpReqSkill = ReqSkill.Defence;
                            break;
                        default:
                            TmpReqSkill = ReqSkill.Attack;
                            Log.Error("GetAllWeapons switch on Value(5) (Req Skill) failed. Defualting Attack. Value used: " + Row["RequiredSkill"].ToString());
                            break;
                    }
                    #endregion

                    armours.Add(new Equipment(
                        EID: Convert.ToInt32(Row["ID"]),
                        EName: Row["Name"].ToString(),
                        ECategory: TmpCat,
                        EType: TmpType,
                        ESlot: TmpSlot,
                        ERequiredSkill: TmpReqSkill,
                        ERequiredLevel: Convert.ToInt32(Row["RequiredLevel"]),
                        EDamage: Convert.ToInt32(Row["Damage"]),
                        EAccuracy: Convert.ToInt32(Row["Accuracy"]),
                        EArmour: Convert.ToInt32(Row["Armour"]),
                        ELifePoints: Convert.ToInt32(Row["LifePoints"]),
                        EPrayer: Convert.ToInt32(Row["Prayer"]),
                        EStrBonus: Convert.ToInt32(Row["StrBonus"]),
                        ERangeBonus: Convert.ToInt32(Row["RangeBonus"]),
                        EMagicBonus: Convert.ToInt32(Row["MagicBonus"]),
                        ECost: Convert.ToInt32(Row["Cost"]),
                        EExamine: Row["Examine"].ToString(),
                        EImagePath: "Armour/" + Row["ImagePath"].ToString()
                        ));
                }
            }
            catch (Exception err)
            {
                Log.Error("SQLITE LOAD ERR: " + err.Message, err);
            }

            Log.Debug("Armour Loaded.");
            return armours;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
