﻿using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSClick.Models
{
    class Familiar : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Familiar()
        {
        }

        public Familiar(int FID, string FName, int FCost, double FCostModifier, int FLevel, int FDPS, string FImagePath, int FQuantity)
        {
            ID = FID;
            Name = FName;
            Cost = FCost;
            CostModifier = FCostModifier;
            Level = FLevel;
            DPS = FDPS;
            ImagePath = FImagePath;
            Quantity = FQuantity;
        }

        private int id;
        public int ID
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
                OnPropertyChanged("ID");
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private int cost;
        public int Cost
        {
            get
            {
                return cost;
            }
            set
            {
                cost = value;
                OnPropertyChanged("Cost");
            }
        }

        private double costModifier;
        public double CostModifier
        {
            get
            {
                return costModifier;
            }
            set
            {
                costModifier = value;
                OnPropertyChanged("CostModifier");
            }
        }

        private int level;
        public int Level
        {
            get
            {
                return level;
            }
            set
            {
                level = value;
                OnPropertyChanged("Level");
            }
        }

        private int dps;
        public int DPS
        {
            get
            {
                return dps;
            }
            set
            {
                dps = value;
                OnPropertyChanged("DPS");
            }
        }

        private string imagePath;
        public string ImagePath
        {
            get
            {
                return "/RSClick;component/Images/Familiars/" + imagePath;
            }
            set
            {
                imagePath = value;
                OnPropertyChanged("ImagePath");
            }
        }

        private int quantity;
        public int Quantity
        {
            get
            {
                return quantity;
            }
            set
            {
                quantity = value;
                OnPropertyChanged("Quantity");
                OnPropertyChanged("TotalDPS");
                OnPropertyChanged("BuyButtonText");
            }
        }

        public long Next1Cost
        {
            get
            {
                return Convert.ToInt64(Math.Round(Cost * Math.Pow(CostModifier, Quantity)));
            }
            set { }
        }

        public long Next10Cost
        {
            get
            {
                return Convert.ToInt64(Math.Round((Cost * (Math.Pow(CostModifier, Quantity + 10) - Math.Pow(CostModifier, Quantity))) / (CostModifier - 1)));
            }
            set { }
        }

        public long Next25Cost
        {
            get
            {
                return Convert.ToInt64(Math.Round((Cost * (Math.Pow(CostModifier, Quantity + 25) - Math.Pow(CostModifier, Quantity))) / (CostModifier - 1)));
            }
            set { }
        }

        public long Next100Cost
        {
            get
            {
                return Convert.ToInt64(Math.Round((Cost * (Math.Pow(CostModifier, Quantity + 100) - Math.Pow(CostModifier, Quantity))) / (CostModifier - 1)));
            }
            set { }
        }

        public long TotalDPS
        {
            get
            {
                return DPS * Quantity;
            }
            set { }
        }

        public string BuyButtonText
        {
            get
            {
                switch (BuyQuantity)
                {
                    case 1:
                        return "Buy 1: (" + String.Format("{0:n0}", Next1Cost) + " GP)";
                    case 10:
                        return "Buy 10: (" + String.Format("{0:n0}", Next10Cost) + " GP)";
                    case 25:
                        return "Buy 25: (" + String.Format("{0:n0}", Next25Cost) + " GP)";
                    case 100:
                        return "Buy 100: (" + String.Format("{0:n0}", Next100Cost) + " GP)";
                }
                return "Buy Now! (" + String.Format("{0:n0}", Cost) + " GP)";
            }
            set { }
        }

        private int buyQuantity = 1;
        public int BuyQuantity
        {
            get
            {
                return buyQuantity;
            }
            set
            {
                buyQuantity = value;
                OnPropertyChanged("BuyQuantity");
                OnPropertyChanged("BuyButtonText");
            }
        }

        public static ObservableCollection<Familiar> GetAllFamiliars()
        {
            Log.Debug("Begin Loading Familiars...");
            ObservableCollection<Familiar> familiars = new ObservableCollection<Familiar>();

            try
            {
                var db = new SQLiteDatabase();
                DataTable dt = db.GetDataTable("Select * FROM Familiars");
                foreach (DataRow Row in dt.Rows)
                {
                    familiars.Add(new Familiar(
                        FID: Convert.ToInt32(Row["ID"]),
                        FName: Row["Name"].ToString(),
                        FCost: Convert.ToInt32(Row["Cost"]),
                        FCostModifier: Convert.ToDouble(Row["CostModifier"]),
                        FLevel: Convert.ToInt32(Row["Level"]),
                        FDPS: Convert.ToInt32(Row["DPS"]),
                        FImagePath: Row["ImagePath"].ToString(),
                        FQuantity: Convert.ToInt32(Row["Quantity"])
                        ));
                }
            }
            catch (Exception err)
            {
                Log.Error("SQLITE LOAD ERR: " + err.Message, err);
            }

            return familiars;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
