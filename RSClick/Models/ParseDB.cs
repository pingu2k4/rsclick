﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parse;
using log4net;
using System.ComponentModel;
using System.Collections.ObjectModel;
using RSClick.ViewModels;

namespace RSClick.Models
{
    class ParseDB : INotifyPropertyChanged
    {
        public static ParseObject ParseUser;
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ParseDB(bool x)
        {

        }

        public ParseDB()
        {
            StartParseDB();

            Ready = true;
        }

        public async void StartParseDB()
        {
            await Start();
            await GetPlayerList();
            Log.Debug("Made it out of StartParseDB!");
        }

        public static bool Ready = false;

        public static bool Wilderness = false;
        public static bool WildernessAwarded = true;
        public static DateTime WildernessEntry = DateTime.MinValue;
        public static DateTime WildernessExit = DateTime.MinValue;
        public static DateTime WildernessGranted = DateTime.MinValue;
        public static int WildernessLevel = 0;
        public static double WildernessReward = 0;

        private static async void PropertyUpdate()
        {
            await UpdateDB();
        }

        private static int attack = 0;
        public static int Attack
        {
            get
            {
                return attack;
            }
            set
            {
                if (value != attack)
                {
                    int x = attack;
                    attack = value;
                    if (x != 0)
                    {
                        PropertyUpdate();
                    }
                }
            }
        }

        private static int strength = 0;
        public static int Strength
        {
            get
            {
                return strength;
            }
            set
            {
                if (value != strength)
                {
                    int x = strength;
                    strength = value;
                    if (x != 0)
                    {
                        PropertyUpdate();
                    }
                }
            }
        }

        private static int defence = 0;
        public static int Defence
        {
            get
            {
                return defence;
            }
            set
            {
                if (value != defence)
                {
                    int x = defence;
                    defence = value;
                    if (x != 0)
                    {
                        PropertyUpdate();
                    }
                }
            }
        }

        private static int constitution = 0;
        public static int Constitution
        {
            get
            {
                return constitution;
            }
            set
            {
                if (value != constitution)
                {
                    int x = constitution;
                    constitution = value;
                    if (x != 0)
                    {
                        PropertyUpdate();
                    }
                }
            }
        }

        private static int range = 0;
        public static int Range
        {
            get
            {
                return range;
            }
            set
            {
                if (value != range)
                {
                    int x = range;
                    range = value;
                    if (x != 0)
                    {
                        PropertyUpdate();
                    }
                }
            }
        }

        private static int magic = 0;
        public static int Magic
        {
            get
            {
                return magic;
            }
            set
            {
                if (value != magic)
                {
                    int x = magic;
                    magic = value;
                    if (x != 0)
                    {
                        PropertyUpdate();
                    }
                }
            }
        }

        private static int summoning = 0;
        public static int Summoning
        {
            get
            {
                return summoning;
            }
            set
            {
                if (value != summoning)
                {
                    int x = summoning;
                    summoning = value;
                    if (x != 0)
                    {
                        PropertyUpdate();
                    }
                }
            }
        }

        private static int combat = 0;
        public static int Combat
        {
            get
            {
                return combat;
            }
            set
            {
                if (value != combat)
                {
                    combat = value;
                }
            }
        }

        public static void SetStats(int Att, int Str, int Def, int HP, int Rng, int Mage, int Summ, int Cbt)
        {
            Attack = Att;
            Strength = Str;
            Defence = Def;
            Constitution = HP;
            Range = Rng;
            Magic = Mage;
            Summoning = Summ;
            Combat = Cbt;
        }

        private async Task<string> Start()
        {
            //Following line does some kind of analyitcal stuff?
            //await ParsePush.SubscribeAsync("");
            Log.Debug("Starting ParseDB...");
            if (Settings.ParseSettings.Default.UserID != "NAN")
            {
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Users");
                Log.Info("UserID != NAN");
                try
                {
                    ParseObject result = await query.GetAsync(Settings.ParseSettings.Default.UserID);
                }
                catch
                {
                    Log.Error("Current User ID Does not exist! Creating a new one...");
                    Settings.ParseSettings.Default.UserID = "NAN";
                }
            }

            if (Settings.ParseSettings.Default.UserID == "NAN")
            {
                Log.Info("Creating new user = Current user = NAN");
                Log.Debug("Testing that it really is NAN: " + Settings.ParseSettings.Default.UserID);

                ParseObject Users = new ParseObject("Users");
                Users["Wilderness"] = false;
                Users["WildernessAwarded"] = true;
                Users["WildernessEntry"] = DateTime.MinValue;
                Users["WildernessExit"] = DateTime.MinValue;
                Users["WildernessLevel"] = 0;
                Users["WildernessReward"] = 0;
                Users["WildernessGranted"] = DateTime.MinValue;
                Users["Attack"] = Attack;
                Users["Strength"] = Strength;
                Users["Defence"] = Defence;
                Users["Constitution"] = Constitution;
                Users["Range"] = Range;
                Users["Magic"] = Magic;
                Users["Summoning"] = Summoning;
                Users["Combat"] = Combat;
                Users.SaveAsync();
                Settings.ParseSettings.Default.UserID = Users.ObjectId;

                ParseUser = Users;
            }
            else
            {
                Log.Info("User found!");

                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Users");
                ParseUser = await query.GetAsync(Settings.ParseSettings.Default.UserID);

                Wilderness = (bool)ParseUser["Wilderness"];
                WildernessAwarded = (bool)ParseUser["WildernessAwarded"];
                WildernessEntry = (DateTime)ParseUser["WildernessEntry"];
                WildernessExit = (DateTime)ParseUser["WildernessExit"];
                WildernessLevel = Convert.ToInt32(ParseUser["WildernessLevel"]);
                WildernessReward = Convert.ToDouble(ParseUser["WildernessReward"]);
                WildernessGranted = (DateTime)(ParseUser["WildernessGranted"]);
                Attack = Convert.ToInt32(ParseUser["Attack"]);
                Strength = Convert.ToInt32(ParseUser["Strength"]);
                Defence = Convert.ToInt32(ParseUser["Defence"]);
                Constitution = Convert.ToInt32(ParseUser["Constitution"]);
                Range = Convert.ToInt32(ParseUser["Range"]);
                Magic = Convert.ToInt32(ParseUser["Magic"]);
                Summoning = Convert.ToInt32(ParseUser["Summoning"]);
                Combat = Convert.ToInt32(ParseUser["Combat"]);
            }

            if (!(bool)ParseUser["Wilderness"] && !(bool)ParseUser["WildernessAwarded"] && Convert.ToDouble(ParseUser["WildernessReward"]) > 0)
            {
                ShowReward = true;
            }

            return "";
        }

        public static async Task UpdateDB()
        {
            ParseUser["Wilderness"] = Wilderness;
            ParseUser["WildernessAwarded"] = WildernessAwarded;
            ParseUser["WildernessEntry"] = WildernessEntry;
            ParseUser["WildernessExit"] = WildernessExit;
            ParseUser["WildernessLevel"] = WildernessLevel;
            ParseUser["WildernessReward"] = WildernessReward;
            ParseUser["WildernessGranted"] = WildernessGranted;
            ParseUser["Attack"] = Attack;
            ParseUser["Strength"] = Strength;
            ParseUser["Defence"] = Defence;
            ParseUser["Constitution"] = Constitution;
            ParseUser["Range"] = Range;
            ParseUser["Magic"] = Magic;
            ParseUser["Summoning"] = Summoning;
            ParseUser["Combat"] = Combat;
            await ParseUser.SaveAsync();
            return;
        }

        public static double CalculateReward()
        {
            if(ParseUser == null)
            {
                return 0d;
            }
            if (!(bool)ParseUser["Wilderness"])
            {
                return 0d;
            }

            DateTime LastUpdate;
            DateTime EndTime;
            double Reward = 0;
            int Seconds = 0;

            if ((DateTime)ParseUser["WildernessGranted"] > (DateTime)ParseUser["WildernessEntry"])
            {
                LastUpdate = (DateTime)ParseUser["WildernessGranted"];
            }
            else
            {
                LastUpdate = (DateTime)ParseUser["WildernessEntry"];
            }

            if (((DateTime)ParseUser["WildernessExit"] > (DateTime)ParseUser["WildernessEntry"]) || WildernessExit > (DateTime)ParseUser["WildernessEntry"])
            {
                EndTime = (DateTime)ParseUser["WildernessExit"];
                if (WildernessExit > EndTime)
                {
                    EndTime = WildernessExit;
                }
            }
            else
            {
                EndTime = DateTime.UtcNow;
            }

            TimeSpan TimeDifference = EndTime - LastUpdate;

            Seconds = (int)TimeDifference.TotalSeconds;

            double WildernessModifier = Math.Sqrt(Math.Sqrt(Math.Sqrt(Convert.ToDouble(ParseUser["WildernessLevel"])))) - 0.99;

            Reward = Seconds * WildernessModifier;

            return Convert.ToDouble(ParseUser["WildernessReward"]) + Reward;
        }

        public static async Task GrantReward()
        {
            if (!(bool)ParseUser["Wilderness"])
            {
                return;
            }

            WildernessReward = CalculateReward();

            WildernessGranted = DateTime.UtcNow;

            await UpdateDB();
        }

        private static bool showReward = false;
        public static bool ShowReward
        {
            get
            {
                return showReward;
            }
            set
            {
                showReward = value;
                NotifyStaticPropertyChanged("ShowReward");
            }
        }

        public static void RewardReward() //Possibly, the worst name ever!
        {
            ShowReward = true;
        }

        private static ObservableCollection<EnemyPlayer> enemyPlayers;
        public static ObservableCollection<EnemyPlayer> EnemyPlayers
        {
            get
            {
                return enemyPlayers;
            }
            set
            {
                enemyPlayers = value;
                NotifyStaticPropertyChanged("EnemyPlayers");
            }
        }

        public async static Task GetPlayerList()
        {
            Log.Debug("Begin Loading Players...");
            ObservableCollection<EnemyPlayer> players = new ObservableCollection<EnemyPlayer>();

            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Users")
                .WhereLessThanOrEqualTo("WildernessLevel", WildernessLevel)
                .WhereNotEqualTo("objectId", Settings.ParseSettings.Default.UserID)
                .WhereEqualTo("Wilderness", true)
                .Limit(1000);

            var x = await query.FindAsync();

            int id = 0;
            foreach (ParseObject player in x)
            {
                if (Math.Abs(Convert.ToInt32(player["Combat"]) - Combat) <= Convert.ToInt32(player["WildernessLevel"]))
                {
                    players.Add(new EnemyPlayer(
                        EID: id,
                        EObjectID: player.ObjectId,
                        EWilderness: (bool)player["Wilderness"],
                        EWildernessLevel: Convert.ToInt32(player["WildernessLevel"]),
                        EAttack: Convert.ToInt32(player["Attack"]),
                        EStrength: Convert.ToInt32(player["Strength"]),
                        EDefence: Convert.ToInt32(player["Defence"]),
                        EConstitution: Convert.ToInt32(player["Constitution"]),
                        ERange: Convert.ToInt32(player["Range"]),
                        EMagic: Convert.ToInt32(player["Magic"]),
                        ESummoning: Convert.ToInt32(player["Summoning"]),
                        ECombat: Convert.ToInt32(player["Combat"])
                    ));
                    id++;
                }
            }

            EnemyPlayers = players;

        }

        public async static void PlayerDied()
        {
            await GrantReward();

            WildernessReward -= Math.Min(WildernessLevel, WildernessReward / 10);

            await UpdateDB();
        }

        public async static void EnemyDied(string enemyObjectID)
        {
            double EnemyReward = 0;

            await GrantReward();

            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Users");

            ParseObject Enemy = await query.GetAsync(enemyObjectID);

            EnemyReward = Convert.ToDouble(Enemy["WildernessReward"]);

            DateTime LastUpdate;
            DateTime EndTime;
            double Reward = 0;
            int Seconds = 0;

            if ((DateTime)Enemy["WildernessGranted"] > (DateTime)Enemy["WildernessEntry"])
            {
                LastUpdate = (DateTime)Enemy["WildernessGranted"];
            }
            else
            {
                LastUpdate = (DateTime)Enemy["WildernessEntry"];
            }

            if ((DateTime)Enemy["WildernessExit"] > (DateTime)Enemy["WildernessEntry"])
            {
                EndTime = (DateTime)Enemy["WildernessExit"];
            }
            else
            {
                EndTime = DateTime.UtcNow;
            }

            TimeSpan TimeDifference = EndTime - LastUpdate;

            Seconds = (int)TimeDifference.TotalSeconds;

            double WildernessModifier = Math.Sqrt(Math.Sqrt(Math.Sqrt(Convert.ToDouble(Enemy["WildernessLevel"])))) - 0.99;

            Reward = Seconds * WildernessModifier;

            Enemy["WildernessReward"] = Convert.ToDouble(Enemy["WildernessReward"]) + Reward - Math.Min(Convert.ToDouble(Enemy["WildernessLevel"]), Convert.ToDouble(Enemy["WildernessReward"]) / 10);

            Enemy["WildernesGranted"] = DateTime.UtcNow;

            Enemy["WildernessExit"] = DateTime.UtcNow;

            Enemy["WildernessLevel"] = 0;

            Enemy["Wilderness"] = false;

            await Enemy.SaveAsync();

            await GrantReward();

            WildernessReward += Math.Min(WildernessLevel, EnemyReward / 10);

            await UpdateDB();

        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged
         = delegate { };
        private static void NotifyStaticPropertyChanged(string propertyName)
        {
            StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
