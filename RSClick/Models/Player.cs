﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSClick.Models
{
    class Player : INotifyPropertyChanged
    {
        public double[] XPTable = new double[1280]; //XPTable will contain values for levels 1-127. (126 is highest reachable). XPTable[1] is 0, XPTable[2] is 83. 
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Player()
        {
            InitialLoad();
            ConstitutionXP = 1154;
            CombatAll = true;
        }

        private void InitialLoad()
        {
            Log.Debug("Loading Level limit boundaries...");
            double lvlExp = 0;
            double four = 4;
            for (double i = 1; i <= 1278; i++)
            {

                lvlExp += Convert.ToDouble(Math.Floor(i + (300 * Math.Pow(2, i / 7))));
                XPTable[Convert.ToInt32(i) + 1] = Convert.ToDouble(Math.Floor(lvlExp / four));

            }
            Log.Debug("Level Limit Boundaries loaded.");
        }

        #region Level Variables

        private double attackXP;
        public double AttackXP
        {
            get
            {
                return attackXP;
            }
            set
            {
                attackXP = value;
                OnPropertyChanged("AttackXP");
            }
        }

        private double strengthXP;
        public double StrengthXP
        {
            get
            {
                return strengthXP;
            }
            set
            {
                strengthXP = value;
                OnPropertyChanged("StrengthXP");
            }
        }

        private double defenceXP;
        public double DefenceXP
        {
            get
            {
                return defenceXP;
            }
            set
            {
                defenceXP = value;
                OnPropertyChanged("DefenceXP");
            }
        }

        private double constitutionXP;
        public double ConstitutionXP
        {
            get
            {
                return constitutionXP;
            }
            set
            {
                constitutionXP = value;
                OnPropertyChanged("ConstitutionXP");
            }
        }

        private double rangeXP;
        public double RangeXP
        {
            get
            {
                return rangeXP;
            }
            set
            {
                rangeXP = value;
                OnPropertyChanged("RangeXP");
            }
        }

        private double magicXP;
        public double MagicXP
        {
            get
            {
                return magicXP;
            }
            set
            {
                magicXP = value;
                OnPropertyChanged("MagicXP");
            }
        }

        private double summoningXP;
        public double SummoningXP
        {
            get
            {
                return summoningXP;
            }
            set
            {
                summoningXP = value;
                OnPropertyChanged("SummoningXP");
            }
        }

        #endregion

        private bool UpdateCombatStyle()
        {
            Log.Debug("Updating Combat Style.");
            combatAttack = false;
            combatAttackDefence = false;
            combatMelee = false;
            combatStrength = false;
            combatStrengthDefence = false;
            combatRange = false;
            combatRangeDefence = false;
            combatMagic = false;
            combatMagicDefence = false;
            combatAll = false;
            return true;
        }

        #region Combat Style Variables

        private bool combatAttack;
        public bool CombatAttack
        {
            get
            {
                return combatAttack;
            }
            set
            {
                combatAttack = UpdateCombatStyle();
                OnPropertyChanged("CombatAttack");
            }
        }

        private bool combatAttackDefence;
        public bool CombatAttackDefence
        {
            get
            {
                return combatAttackDefence;
            }
            set
            {
                combatAttackDefence = UpdateCombatStyle();
                OnPropertyChanged("CombatAttackDefence");
            }
        }

        private bool combatMelee;
        public bool CombatMelee
        {
            get
            {
                return combatMelee;
            }
            set
            {
                combatMelee = UpdateCombatStyle();
                OnPropertyChanged("CombatMelee");
            }
        }

        private bool combatStrength;
        public bool CombatStrength
        {
            get
            {
                return combatStrength;
            }
            set
            {
                combatStrength = UpdateCombatStyle();
                OnPropertyChanged("CombatStrength");
            }
        }

        private bool combatStrengthDefence;
        public bool CombatStrengthDefence
        {
            get
            {
                return combatStrengthDefence;
            }
            set
            {
                combatStrengthDefence = UpdateCombatStyle();
                OnPropertyChanged("CombatStrengthDefence");
            }
        }

        private bool combatRange;
        public bool CombatRange
        {
            get
            {
                return combatRange;
            }
            set
            {
                combatRange = UpdateCombatStyle();
                OnPropertyChanged("CombatRange");
            }
        }

        private bool combatRangeDefence;
        public bool CombatRangeDefence
        {
            get
            {
                return combatRangeDefence;
            }
            set
            {
                combatRangeDefence = UpdateCombatStyle();
                OnPropertyChanged("CombatRangeDefence");
            }
        }

        private bool combatMagic;
        public bool CombatMagic
        {
            get
            {
                return combatMagic;
            }
            set
            {
                combatMagic = UpdateCombatStyle();
                OnPropertyChanged("CombatMagic");
            }
        }

        private bool combatMagicDefence;
        public bool CombatMagicDefence
        {
            get
            {
                return combatMagicDefence;
            }
            set
            {
                combatMagicDefence = UpdateCombatStyle();
                OnPropertyChanged("CombatMagicDefence");
            }
        }

        private bool combatAll;
        public bool CombatAll
        {
            get
            {
                return combatAll;
            }
            set
            {
                combatAll = UpdateCombatStyle();
                OnPropertyChanged("CombatAll");
            }
        }

        #endregion

        #region Player Equipped

        private int currentWeaponSlot=-1;
        public int CurrentWeaponSlot
        {
            get
            {
                return currentWeaponSlot;
            }
            set
            {
                currentWeaponSlot = value;
                OnPropertyChanged("CurrentWeaponSlot");
            }
        }

        private int currentHeadSlot = -1;
        public int CurrentHeadSlot
        {
            get
            {
                return currentHeadSlot;
            }
            set
            {
                currentHeadSlot = value;
                OnPropertyChanged("CurrentHeadSlot");
            }
        }

        private int currentAuraSlot = -1;
        public int CurrentAuraSlot
        {
            get
            {
                return currentAuraSlot;
            }
            set
            {
                currentAuraSlot = value;
                OnPropertyChanged("CurrentAuraSlot");
            }
        }

        private int currentPocketSlot = -1;
        public int CurrentPocketSlot
        {
            get
            {
                return currentPocketSlot;
            }
            set
            {
                currentPocketSlot = value;
                OnPropertyChanged("CurrentPocketSlot");
            }
        }

        private int currentCapeSlot = -1;
        public int CurrentCapeSlot
        {
            get
            {
                return currentCapeSlot;
            }
            set
            {
                currentCapeSlot = value;
                OnPropertyChanged("CurrentCapeSlot");
            }
        }

        private int currentNeckSlot = -1;
        public int CurrentNeckSlot
        {
            get
            {
                return currentNeckSlot;
            }
            set
            {
                currentNeckSlot = value;
                OnPropertyChanged("CurrentNeckSlot");
            }
        }

        private int currentAmmoSlot = -1;
        public int CurrentAmmoSlot
        {
            get
            {
                return currentAmmoSlot;
            }
            set
            {
                currentAmmoSlot = value;
                OnPropertyChanged("CurrentAmmoSlot");
            }
        }

        private int currentBodySlot = -1;
        public int CurrentBodySlot
        {
            get
            {
                return currentBodySlot;
            }
            set
            {
                currentBodySlot = value;
                OnPropertyChanged("CurrentBodySlot");
            }
        }

        private int currentShieldSlot = -1;
        public int CurrentShieldSlot
        {
            get
            {
                return currentShieldSlot;
            }
            set
            {
                currentShieldSlot = value;
                OnPropertyChanged("CurrentShieldSlot");
            }
        }

        private int currentLegsSlot = -1;
        public int CurrentLegsSlot
        {
            get
            {
                return currentLegsSlot;
            }
            set
            {
                currentLegsSlot = value;
                OnPropertyChanged("CurrentLegsSlot");
            }
        }

        private int currentHandsSlot = -1;
        public int CurrentHandsSlot
        {
            get
            {
                return currentHandsSlot;
            }
            set
            {
                currentHandsSlot = value;
                OnPropertyChanged("CurrentHandsSlot");
            }
        }

        private int currentFeetSlot = -1;
        public int CurrentFeetSlot
        {
            get
            {
                return currentFeetSlot;
            }
            set
            {
                currentFeetSlot = value;
                OnPropertyChanged("CurrentFeetSlot");
            }
        }

        private int currentRingSlot = -1;
        public int CurrentRingSlot
        {
            get
            {
                return currentRingSlot;
            }
            set
            {
                currentRingSlot = value;
                OnPropertyChanged("CurrentRingSlot");
            }
        }

        public enum SlotType
        {
            Weapon,
            Armour
        }

        private SlotType auraSlotType = SlotType.Armour;
        public SlotType AuraSlotType
        {
            get
            {
                return auraSlotType;
            }
            set
            {
                auraSlotType = value;
                OnPropertyChanged("AuraSlotType");
            }
        }

        private SlotType headSlotType = SlotType.Armour;
        public SlotType HeadSlotType
        {
            get
            {
                return headSlotType;
            }
            set
            {
                headSlotType = value;
                OnPropertyChanged("HeadSlotType");
            }
        }

        private SlotType pocketSlotType = SlotType.Armour;
        public SlotType PocketSlotType
        {
            get
            {
                return pocketSlotType;
            }
            set
            {
                pocketSlotType = value;
                OnPropertyChanged("PocketSlotType");
            }
        }

        private SlotType capeSlotType = SlotType.Armour;
        public SlotType CapeSlotType
        {
            get
            {
                return capeSlotType;
            }
            set
            {
                capeSlotType = value;
                OnPropertyChanged("CapeSlotType");
            }
        }

        private SlotType neckSlotType = SlotType.Armour;
        public SlotType NeckSlotType
        {
            get
            {
                return neckSlotType;
            }
            set
            {
                neckSlotType = value;
                OnPropertyChanged("NeckSlotType");
            }
        }

        private SlotType ammoSlotType = SlotType.Armour;
        public SlotType AmmoSlotType
        {
            get
            {
                return ammoSlotType;
            }
            set
            {
                ammoSlotType = value;
                OnPropertyChanged("AmmoSlotType");
            }
        }

        private SlotType weaponSlotType = SlotType.Weapon;
        public SlotType WeaponSlotType
        {
            get
            {
                return weaponSlotType;
            }
            set
            {
                weaponSlotType = value;
                OnPropertyChanged("WeaponSlotType");
            }
        }

        private SlotType bodySlotType = SlotType.Armour;
        public SlotType BodySlotType
        {
            get
            {
                return bodySlotType;
            }
            set
            {
                bodySlotType = value;
                OnPropertyChanged("BodySlotType");
            }
        }

        private SlotType shieldSlotType = SlotType.Armour;
        public SlotType ShieldSlotType
        {
            get
            {
                return shieldSlotType;
            }
            set
            {
                shieldSlotType = value;
                OnPropertyChanged("ShieldSlotType");
            }
        }

        private SlotType legsSlotType = SlotType.Armour;
        public SlotType LegsSlotType
        {
            get
            {
                return legsSlotType;
            }
            set
            {
                legsSlotType = value;
                OnPropertyChanged("LegsSlotType");
            }
        }

        private SlotType handsSlotType = SlotType.Armour;
        public SlotType HandsSlotType
        {
            get
            {
                return handsSlotType;
            }
            set
            {
                handsSlotType = value;
                OnPropertyChanged("HandsSlotType");
            }
        }

        private SlotType feetSlotType = SlotType.Armour;
        public SlotType FeetSlotType
        {
            get
            {
                return feetSlotType;
            }
            set
            {
                feetSlotType = value;
                OnPropertyChanged("FeetSlotType");
            }
        }

        private SlotType ringSlotType = SlotType.Armour;
        public SlotType RingSlotType
        {
            get
            {
                return ringSlotType;
            }
            set
            {
                ringSlotType = value;
                OnPropertyChanged("RingSlotType");
            }
        }

        #endregion

        private long gp;
        public long GP
        {
            get
            {
                return gp;
            }
            set
            {
                gp = value;
                OnPropertyChanged("GP");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
