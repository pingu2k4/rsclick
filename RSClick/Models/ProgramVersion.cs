﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSClick.Models
{
    class ProgramVersion : INotifyPropertyChanged
    {
        private string version; //The version of the current program.

        //Initialises a new instance of the ProgramVersion class
        public ProgramVersion()
        {
            Version = "Version: " + Convert.ToString(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
        }

        //Gets or sets the Version
        public string Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
                OnPropertyChanged("Version");
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
