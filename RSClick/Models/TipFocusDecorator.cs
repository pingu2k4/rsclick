﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace RSClick.Models
{
    public class TipFocusDecorator : Decorator
    {

        public bool IsOpen
        {
            get { return (bool)GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }
        // Using a DependencyProperty as the backing store for Open.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsOpenProperty =
            DependencyProperty.Register("IsOpen", typeof(bool), typeof(TipFocusDecorator),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, IsOpenPropertyChanged));


        public string TipText
        {
            get { return (string)GetValue(TipTextProperty); }
            set { SetValue(TipTextProperty, value); }
        }
        // Using a DependencyProperty as the backing store for TipText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TipTextProperty =
            DependencyProperty.Register("TipText", typeof(string), typeof(TipFocusDecorator), new UIPropertyMetadata(string.Empty));


        public string TipHead
        {
            get { return (string)GetValue(TipHeadProperty); }
            set { SetValue(TipHeadProperty, value); }
        }
        // Using a DependencyProperty as the backing store for TipText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TipHeadProperty =
            DependencyProperty.Register("TipHead", typeof(string), typeof(TipFocusDecorator), new UIPropertyMetadata(string.Empty));


        public string TipPos
        {
            get { return (string)GetValue(TipPosProperty); }
            set { SetValue(TipPosProperty, value); }
        }
        // Using a DependencyProperty as the backing store for TipPos.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TipPosProperty =
            DependencyProperty.Register("TipPos", typeof(string), typeof(TipFocusDecorator), new UIPropertyMetadata(string.Empty));


        public bool HasBeenShown
        {
            get { return (bool)GetValue(HasBeenShownProperty); }
            set { SetValue(HasBeenShownProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasBeenShown.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasBeenShownProperty =
            DependencyProperty.Register("HasBeenShown", typeof(bool), typeof(TipFocusDecorator), new UIPropertyMetadata(false));

        private static void IsOpenPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var decorator = sender as TipFocusDecorator;

            if ((bool)e.NewValue)
            {
                if (!decorator.HasBeenShown)
                    decorator.HasBeenShown = true;

                decorator.Open();
            }

            if (!(bool)e.NewValue)
            {
                decorator.Close();
            }
        }

        TipFocusAdorner adorner;

        protected void Open()
        {
            adorner = new TipFocusAdorner(this.Child);
            var root = Window.GetWindow(this);
            var blurContainer = (Visual)root.Content;
            var adornerLayer = AdornerLayer.GetAdornerLayer(blurContainer);
            //var adornerLayer = AdornerLayer.GetAdornerLayer(this.Child);
            adornerLayer.Add(adorner);
            TutorialTip tip = new TutorialTip(TipHead,TipText,TipPos);
            tip.Owner = Application.Current.MainWindow;
            double width = tip.Width;
            double height = tip.Height;
            Point position = this.Child.PointToScreen(new Point(0d, 0d));
            switch (TipPos)
            {
                case "Bottom":
                    position.X += (this.Child.RenderSize.Width / 2) - (width / 2);
                    position.Y += this.Child.RenderSize.Height + 10;
                    break;
                case "Top":
                    position.X += (this.Child.RenderSize.Width / 2) - (width / 2);
                    position.Y += -height - 10;
                    break;
                case "Left":
                    position.X += -width - 10;
                    position.Y += (this.Child.RenderSize.Height / 2) - (height / 2);
                    break;
                case "Right":
                    position.X += this.Child.RenderSize.Width + 10;
                    position.Y += (this.Child.RenderSize.Height / 2) - (height / 2);
                    break;
            }
            tip.Left = position.X;
            tip.Top = position.Y;
            tip.ShowDialog();
            //MessageBox.Show(TipText + position);  // Change for your custom tip Window
            IsOpen = false;
        }

        protected void Close()
        {
            var root = Window.GetWindow(this);
            var blurContainer = (Visual)root.Content;
            var adornerLayer = AdornerLayer.GetAdornerLayer(blurContainer);
            //var adornerLayer = AdornerLayer.GetAdornerLayer(this.Child);
            adornerLayer.Remove(adorner);
            adorner = null;
        }

    }
}
