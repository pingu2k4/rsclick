﻿using Mantin.Controls.Wpf.Notification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace RSClick.Models
{
    class Toast
    {
        ToastPopUp toast;

        public Toast()
        {
            toast = new ToastPopUp("TITLE!", "Description?", "", NotificationType.Information);
            toast.Background = new SolidColorBrush(Colors.AliceBlue);
            toast.BorderBrush = new SolidColorBrush(Colors.Black);
            toast.FontColor = new SolidColorBrush(Colors.Black);
            //toast.HyperlinkClicked += this.ToastHyperlinkClicked;
            //toast.ClosedByUser += this.ToastClosedByUser;
            toast.Show();
        }

        public Toast(string Title, string Description, NotificationType Type = NotificationType.Information)
        {
            toast = new ToastPopUp(Title, Description, "", Type);
            toast.Background = new SolidColorBrush(Colors.AliceBlue);
            toast.BorderBrush = new SolidColorBrush(Colors.Black);
            toast.FontColor = new SolidColorBrush(Colors.Black);
            //toast.HyperlinkClicked += this.ToastHyperlinkClicked;
            //toast.ClosedByUser += this.ToastClosedByUser;
            toast.Show();
        }


    }
}
