﻿using RSClick.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RSClick
{
    /// <summary>
    /// Interaction logic for TutorialTip.xaml
    /// </summary>
    public partial class TutorialTip : Window, INotifyPropertyChanged
    {
        Timer LoadingTimer = new Timer(50);

        public TutorialTip()
        {
            InitializeComponent();
            Header = "TEST HEADER";
            Body = "TEST BODY";
        }

        public TutorialTip(string title, string description, string pos)
        {
            InitializeComponent();
            Header = title;
            Body = description;
            switch (pos)
            {
                case "Top":
                    BottomArrow = true;
                    break;
                case "Bottom":
                    TopArrow = true;
                    break;
                case "Right":
                    LeftArrow = true;
                    break;
                case "Left":
                    RightArrow = true;
                    break;
            }

            if (ParseDB.ParseUser == null)
            {
                LoadingTimer.Elapsed += TimerTick;
                LoadingTimer.Start();
            }
        }

        private string header;
        public string Header
        {
            get
            {
                return header;
            }
            set
            {
                header = value;
                OnPropertyChanged("Header");
            }
        }

        private string body;
        public string Body
        {
            get
            {
                return body;
            }
            set 
            { 
                body = value;
                OnPropertyChanged("Body");
            }
        }

        private bool topArrow = false;
        public bool TopArrow
        {
            get
            {
                return topArrow;
            }
            set
            {
                topArrow = value;
                OnPropertyChanged("TopArrow");
            }
        }

        private bool leftArrow = false;
        public bool LeftArrow
        {
            get
            {
                return leftArrow;
            }
            set
            {
                leftArrow = value;
                OnPropertyChanged("LeftARrow");
            }
        }

        private bool rightArrow = false;
        public bool RightArrow
        {
            get
            {
                return rightArrow;
            }
            set
            {
                rightArrow = value;
                OnPropertyChanged("RightArrow");
            }
        }

        private bool bottomArrow = false;
        public bool BottomArrow
        {
            get
            {
                return bottomArrow;
            }
            set
            {
                bottomArrow = value;
                OnPropertyChanged("BottomArrow");
            }
        }

        public string CloseText
        {
            get
            {
                if (ParseDB.ParseUser == null)
                {
                    if (TimerLoops > 50)
                    {
                        return "ERROR. Click to close.";
                    }
                    return "Loading...";
                }
                else
                {
                    return "(Click to close)";
                }
            }
        }

        private int TimerLoops = 0;

        public void TimerTick(object sender, ElapsedEventArgs e)
        {
            TimerLoops++;
            OnPropertyChanged("CloseText");
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (ParseDB.ParseUser != null)
            {
                LoadingTimer.Stop();
                this.Close();
            }
            else
            {
                if (TimerLoops > 50)
                {
                    System.Windows.Forms.MessageBox.Show("there was a problem loading user data. Please restart and try again.");
                    Application.Current.Shutdown();
                    this.Close();
                }
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
