﻿using log4net;
using RSClick.Commands;
using RSClick.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace RSClick.ViewModels
{
    class DBManagementViewModel : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public DBManagementViewModel(DBManagement window)
        {
            SetupEnemies();
            SetupWeapons();
            SetupArmours();
            SetupFamiliars();
        }

        #region Enemies
        private void SetupEnemies()
        {
            AddEnemies();
            AddEnemyWeaknesses();
            AddEnemyAttackStyles();
        }

        private ObservableCollection<Enemy> enemies;
        public ObservableCollection<Enemy> Enemies
        {
            get
            {
                return enemies;
            }
            set
            {
                enemies = value;
                OnPropertyChanged("Enemies");
            }
        }
        private void AddEnemies()
        {
            if (Enemies != null)
            {
                Enemies.Clear();
            }
            Enemies = Enemy.GetAll();
            Log.Info("Enemies Loaded.");
        }

        private string enemyName = "";
        public string EnemyName
        {
            get
            {
                return enemyName;
            }
            set
            {
                enemyName = value;
                OnPropertyChanged("EnemyName");
            }
        }

        private int enemyMaxHP;
        public int EnemyMaxHP
        {
            get
            {
                return enemyMaxHP;
            }
            set
            {
                enemyMaxHP = value;
                OnPropertyChanged("EnemyMaxHP");
            }
        }

        private int enemyAttack;
        public int EnemyAttack
        {
            get
            {
                return enemyAttack;
            }
            set
            {
                enemyAttack = value;
                OnPropertyChanged("EnemyAttack");
            }
        }

        private int enemyDefence;
        public int EnemyDefence
        {
            get
            {
                return enemyDefence;
            }
            set
            {
                enemyDefence = value;
                OnPropertyChanged("EnemyDefence");
            }
        }

        private int enemyRange;
        public int EnemyRange
        {
            get
            {
                return enemyRange;
            }
            set
            {
                enemyRange = value;
                OnPropertyChanged("EnemyRange");
            }
        }

        private int enemyMagic;
        public int EnemyMagic
        {
            get
            {
                return enemyMagic;
            }
            set
            {
                enemyMagic = value;
                OnPropertyChanged("EnemyMagic");
            }
        }

        private int enemyBaseDPS;
        public int EnemyBaseDPS
        {
            get
            {
                return enemyBaseDPS;
            }
            set
            {
                enemyBaseDPS = value;
                OnPropertyChanged("EnemyBaseDPS");
            }
        }

        private double enemySplitXP;
        public double EnemySplitXP
        {
            get
            {
                return enemySplitXP;
            }
            set
            {
                enemySplitXP = value;
                OnPropertyChanged("EnemySplitXP");
            }
        }

        private double enemyHPXP;
        public double EnemyHPXP
        {
            get
            {
                return enemyHPXP;
            }
            set
            {
                enemyHPXP = value;
                OnPropertyChanged("EnemyHPXP");
            }
        }

        private int enemyMinGP;
        public int EnemyMinGP
        {
            get
            {
                return enemyMinGP;
            }
            set
            {
                enemyMinGP = value;
                OnPropertyChanged("EnemyMinGP");
            }
        }

        private int enemyMaxGP;
        public int EnemyMaxGP
        {
            get
            {
                return enemyMaxGP;
            }
            set
            {
                enemyMaxGP = value;
                OnPropertyChanged("EnemyMaxGP");
            }
        }

        public ObservableCollection<string> EnemyWeaknessValues
        {
            get;
            set;
        }
        private void AddEnemyWeaknesses()
        {
            EnemyWeaknessValues = new ObservableCollection<string>();
            EnemyWeaknessValues.Add("None");
            EnemyWeaknessValues.Add("Melee");
            EnemyWeaknessValues.Add("Range");
            EnemyWeaknessValues.Add("Magic");
        }
        private string enemyWeakness = "None";
        public string EnemyWeakness
        {
            get
            {
                return enemyWeakness;
            }
            set
            {
                enemyWeakness = value;
                OnPropertyChanged("EnemyWeakness");
            }
        }

        public ObservableCollection<string> EnemyAttackStyles
        {
            get;
            set;
        }
        private void AddEnemyAttackStyles()
        {
            EnemyAttackStyles = new ObservableCollection<string>();
            EnemyAttackStyles.Add("None");
            EnemyAttackStyles.Add("Melee");
            EnemyAttackStyles.Add("Range");
            EnemyAttackStyles.Add("Magic");
        }
        private string enemyAttackstyle = "None";
        public string EnemyAttackStyle
        {
            get
            {
                return enemyAttackstyle;
            }
            set
            {
                enemyAttackstyle = value;
                OnPropertyChanged("EnemyAttackStyle");
            }
        }

        private string enemyImagePath = "";
        public string EnemyImagePath
        {
            get
            {
                return enemyImagePath;
            }
            set
            {
                enemyImagePath = value;
                OnPropertyChanged("EnemyImagePath");
            }
        }

        private string enemyStatus;
        public string EnemyStatus
        {
            get
            {
                return enemyStatus;
            }
            set
            {
                enemyStatus = value;
                OnPropertyChanged("EnemyStatus");
            }
        }

        private int? enemyDeleteID;
        public int? EnemyDeleteID
        {
            get
            {
                return enemyDeleteID;
            }
            set
            {
                enemyDeleteID = value;
                OnPropertyChanged("EnemyDeleteID");
            }
        }

        private int? enemySwapFrom;
        public int? EnemySwapFrom
        {
            get
            {
                return enemySwapFrom;
            }
            set
            {
                enemySwapFrom = value;
                OnPropertyChanged("EnemySwapFrom");
            }
        }

        private int? enemySwapTo;
        public int? EnemySwapTo
        {
            get
            {
                return enemySwapTo;
            }
            set
            {
                enemySwapTo = value;
                OnPropertyChanged("EnemySwapTo");
            }
        }

        private int? enemyUpdateID;
        public int? EnemyUpdateID
        {
            get
            {
                return enemyUpdateID;
            }
            set
            {
                enemyUpdateID = value;
                OnPropertyChanged("EnemyUpdateID");
            }
        }
        #endregion

        #region Weapons
        private void SetupWeapons()
        {
            AddWeapons();
            AddWeaponCategories();
            AddWeaponTypes();
            AddWeaponSlots();
            AddWeaponRequiredSkills();
        }

        private ObservableCollection<Equipment> weapons;
        public ObservableCollection<Equipment> Weapons
        {
            get
            {
                return weapons;
            }
            set
            {
                weapons = value;
                OnPropertyChanged("Weapons");
            }
        }
        private void AddWeapons()
        {
            if (Weapons != null)
            {
                Weapons.Clear();
            }
            Weapons = Equipment.GetAllWeapons();
            Log.Info("Weapons Loaded.");
        }

        private string weaponName;
        public string WeaponName
        {
            get 
            { 
                return weaponName;
            }
            set
            { 
                weaponName = value;
                OnPropertyChanged("WeaponName");
            }
        }

        public ObservableCollection<string> WeaponCategories
        {
            get;
            set;
        }
        private void AddWeaponCategories()
        {
            WeaponCategories = new ObservableCollection<string>();
            WeaponCategories.Add("Bronze");
            WeaponCategories.Add("Misc");
        }
        private string weaponCategory = "Bronze";
        public string WeaponCategory
        {
            get
            {
                return weaponCategory;
            }
            set
            {
                weaponCategory = value;
                OnPropertyChanged("WeaponCategory");
            }
        }

        public ObservableCollection<string> WeaponTypes
        {
            get;
            set;
        }
        private void AddWeaponTypes()
        {
            WeaponTypes = new ObservableCollection<string>();
            WeaponTypes.Add("Melee");
            WeaponTypes.Add("Range");
            WeaponTypes.Add("Magic");
        }
        private string weaponType = "Melee";
        public string WeaponType
        {
            get
            {
                return weaponType;
            }
            set
            {
                weaponType = value;
                OnPropertyChanged("WeaponType");
            }
        }

        public ObservableCollection<string> WeaponSlots
        {
            get;
            set;
        }
        private void AddWeaponSlots()
        {
            WeaponSlots = new ObservableCollection<string>();
            
            WeaponSlots.Add("Aura");
            WeaponSlots.Add("Head");
            WeaponSlots.Add("Pocket");
            WeaponSlots.Add("Cape");
            WeaponSlots.Add("Neck");
            WeaponSlots.Add("Ammo");
            WeaponSlots.Add("Weapon");
            WeaponSlots.Add("Body");
            WeaponSlots.Add("Shield");
            WeaponSlots.Add("Legs");
            WeaponSlots.Add("Hands");
            WeaponSlots.Add("Feet");
            WeaponSlots.Add("Ring");
            WeaponSlots.Add("TwoHand");
        }
        private string weaponSlot = "Weapon";
        public string WeaponSlot
        {
            get
            {
                return weaponSlot;
            }
            set
            {
                weaponSlot = value;
                OnPropertyChanged("WeaponSlot");
            }
        }

        public ObservableCollection<string> WeaponRequiredSkills
        {
            get;
            set;
        }
        private void AddWeaponRequiredSkills()
        {
            WeaponRequiredSkills = new ObservableCollection<string>();
            WeaponRequiredSkills.Add("Attack");
            WeaponRequiredSkills.Add("Defence");
        }
        private string weaponRequiredSkill = "Attack";
        public string WeaponRequiredSkill
        {
            get
            {
                return weaponRequiredSkill;
            }
            set
            {
                weaponRequiredSkill = value;
                OnPropertyChanged("WeaponRequiredSkill");
            }
        }

        private int weaponRequiredLevel;
        public int WeaponRequiredLevel
        {
            get
            {
                return weaponRequiredLevel;
            }
            set
            {
                weaponRequiredLevel = value;
                OnPropertyChanged("WeaponRequiredLevel");
            }
        }

        private int weaponDamage;
        public int WeaponDamage
        {
            get
            {
                return weaponDamage;
            }
            set
            {
                weaponDamage = value;
                OnPropertyChanged("WeaponDamage");
            }
        }

        private int weaponAccuracy;
        public int WeaponAccuracy
        {
            get
            {
                return weaponAccuracy;
            }
            set
            {
                weaponAccuracy = value;
                OnPropertyChanged("WeaponAccuracy");
            }
        }

        private int weaponArmour;
        public int WeaponArmour
        {
            get
            {
                return weaponArmour;
            }
            set
            {
                weaponArmour = value;
                OnPropertyChanged("WeaponArmour");
            }
        }

        private int weaponLifePoints;
        public int WeaponLifePoints
        {
            get
            {
                return weaponLifePoints;
            }
            set
            {
                weaponLifePoints = value;
                OnPropertyChanged("WeaponLifePoints");
            }
        }

        private int weaponPrayer;
        public int WeaponPrayer
        {
            get
            {
                return weaponPrayer;
            }
            set
            {
                weaponPrayer = value;
                OnPropertyChanged("WeaponPrayer");
            }
        }

        private int weaponStrBonus;
        public int WeaponStrBonus
        {
            get
            {
                return weaponStrBonus;
            }
            set
            {
                weaponStrBonus = value;
                OnPropertyChanged("WeaponStrBonus");
            }
        }

        private int weaponRangeBonus;
        public int WeaponRangeBonus
        {
            get
            {
                return weaponRangeBonus;
            }
            set
            {
                weaponRangeBonus = value;
                OnPropertyChanged("WeaponRangeBonus");
            }
        }

        private int weaponMagicBonus;
        public int WeaponMagicBonus
        {
            get
            {
                return weaponMagicBonus;
            }
            set
            {
                weaponMagicBonus = value;
                OnPropertyChanged("WeaponMagicBonus");
            }
        }

        private int weaponCost;
        public int WeaponCost
        {
            get
            {
                return weaponCost;
            }
            set
            {
                weaponCost = value;
                OnPropertyChanged("WeaponCost");
            }
        }

        private string weaponExamine;
        public string WeaponExamine
        {
            get
            {
                return weaponExamine;
            }
            set
            {
                weaponExamine = value;
                OnPropertyChanged("WeaponExamine");
            }
        }

        private string weaponImagePath;
        public string WeaponImagePath
        {
            get
            {
                return weaponImagePath;
            }
            set
            {
                weaponImagePath = value;
                OnPropertyChanged("WeaponImagePath");
            }
        }

        private string weaponStatus;
        public string WeaponStatus
        {
            get
            {
                return weaponStatus;
            }
            set
            {
                weaponStatus = value;
                OnPropertyChanged("WeaponStatus");
            }
        }

        private int? weaponDeleteID;
        public int? WeaponDeleteID
        {
            get
            {
                return weaponDeleteID;
            }
            set
            {
                weaponDeleteID = value;
                OnPropertyChanged("WeaponDeleteID");
            }
        }

        private int? weaponSwapFrom;
        public int? WeaponSwapFrom
        {
            get
            {
                return weaponSwapFrom;
            }
            set
            {
                weaponSwapFrom = value;
                OnPropertyChanged("WeaponSwapFrom");
            }
        }

        private int? weaponSwapTo;
        public int? WeaponSwapTo
        {
            get
            {
                return weaponSwapTo;
            }
            set
            {
                weaponSwapTo = value;
                OnPropertyChanged("WeaponSwapTo");
            }
        }
        #endregion

        #region Armours
        private void SetupArmours()
        {
            AddArmours();
            AddArmourCategories();
            AddArmourTypes();
            AddArmourSlots();
            AddArmourRequiredSkills();
        }

        private ObservableCollection<Equipment> armours;
        public ObservableCollection<Equipment> Armours
        {
            get
            {
                return armours;
            }
            set
            {
                armours = value;
                OnPropertyChanged("Armours");
            }
        }
        private void AddArmours()
        {
            if (Armours != null)
            {
                Armours.Clear();
            }
            Armours = Equipment.GetAllArmour();
            Log.Info("Armours Loaded.");
        }

        private string armourName;
        public string ArmourName
        {
            get
            {
                return armourName;
            }
            set
            {
                armourName = value;
                OnPropertyChanged("ArmourName");
            }
        }

        public ObservableCollection<string> ArmourCategories
        {
            get;
            set;
        }
        private void AddArmourCategories()
        {
            ArmourCategories = new ObservableCollection<string>();
            ArmourCategories.Add("Bronze");
            ArmourCategories.Add("Misc");
        }
        private string armourCategory = "Bronze";
        public string ArmourCategory
        {
            get
            {
                return armourCategory;
            }
            set
            {
                armourCategory = value;
                OnPropertyChanged("ArmourCategory");
            }
        }

        public ObservableCollection<string> ArmourTypes
        {
            get;
            set;
        }
        private void AddArmourTypes()
        {
            ArmourTypes = new ObservableCollection<string>();
            ArmourTypes.Add("Melee");
            ArmourTypes.Add("Range");
            ArmourTypes.Add("Magic");
        }
        private string armourType = "Melee";
        public string ArmourType
        {
            get
            {
                return armourType;
            }
            set
            {
                armourType = value;
                OnPropertyChanged("ArmourType");
            }
        }

        public ObservableCollection<string> ArmourSlots
        {
            get;
            set;
        }
        private void AddArmourSlots()
        {
            ArmourSlots = new ObservableCollection<string>();

            ArmourSlots.Add("Aura");
            ArmourSlots.Add("Head");
            ArmourSlots.Add("Pocket");
            ArmourSlots.Add("Cape");
            ArmourSlots.Add("Neck");
            ArmourSlots.Add("Ammo");
            ArmourSlots.Add("Shield");
            ArmourSlots.Add("Body");
            ArmourSlots.Add("Shield");
            ArmourSlots.Add("Legs");
            ArmourSlots.Add("Hands");
            ArmourSlots.Add("Feet");
            ArmourSlots.Add("Ring");
            ArmourSlots.Add("TwoHand");
        }
        private string armourSlot = "Shield";
        public string ArmourSlot
        {
            get
            {
                return armourSlot;
            }
            set
            {
                armourSlot = value;
                OnPropertyChanged("ArmourSlot");
            }
        }

        public ObservableCollection<string> ArmourRequiredSkills
        {
            get;
            set;
        }
        private void AddArmourRequiredSkills()
        {
            ArmourRequiredSkills = new ObservableCollection<string>();
            ArmourRequiredSkills.Add("Attack");
            ArmourRequiredSkills.Add("Defence");
        }
        private string armourRequiredSkill = "Defence";
        public string ArmourRequiredSkill
        {
            get
            {
                return armourRequiredSkill;
            }
            set
            {
                armourRequiredSkill = value;
                OnPropertyChanged("ArmourRequiredSkill");
            }
        }

        private int armourRequiredLevel;
        public int ArmourRequiredLevel
        {
            get
            {
                return armourRequiredLevel;
            }
            set
            {
                armourRequiredLevel = value;
                OnPropertyChanged("ArmourRequiredLevel");
            }
        }

        private int armourDamage;
        public int ArmourDamage
        {
            get
            {
                return armourDamage;
            }
            set
            {
                armourDamage = value;
                OnPropertyChanged("ArmourDamage");
            }
        }

        private int armourAccuracy;
        public int ArmourAccuracy
        {
            get
            {
                return armourAccuracy;
            }
            set
            {
                armourAccuracy = value;
                OnPropertyChanged("ArmourAccuracy");
            }
        }

        private int armourArmour;
        public int ArmourArmour
        {
            get
            {
                return armourArmour;
            }
            set
            {
                armourArmour = value;
                OnPropertyChanged("ArmourArmour");
            }
        }

        private int armourLifePoints;
        public int ArmourLifePoints
        {
            get
            {
                return armourLifePoints;
            }
            set
            {
                armourLifePoints = value;
                OnPropertyChanged("ArmourLifePoints");
            }
        }

        private int armourPrayer;
        public int ArmourPrayer
        {
            get
            {
                return armourPrayer;
            }
            set
            {
                armourPrayer = value;
                OnPropertyChanged("ArmourPrayer");
            }
        }

        private int armourStrBonus;
        public int ArmourStrBonus
        {
            get
            {
                return armourStrBonus;
            }
            set
            {
                armourStrBonus = value;
                OnPropertyChanged("ArmourStrBonus");
            }
        }

        private int armourRangeBonus;
        public int ArmourRangeBonus
        {
            get
            {
                return armourRangeBonus;
            }
            set
            {
                armourRangeBonus = value;
                OnPropertyChanged("ArmourRangeBonus");
            }
        }

        private int armourMagicBonus;
        public int ArmourMagicBonus
        {
            get
            {
                return armourMagicBonus;
            }
            set
            {
                armourMagicBonus = value;
                OnPropertyChanged("ArmourMagicBonus");
            }
        }

        private int armourCost;
        public int ArmourCost
        {
            get
            {
                return armourCost;
            }
            set
            {
                armourCost = value;
                OnPropertyChanged("ArmourCost");
            }
        }

        private string armourExamine;
        public string ArmourExamine
        {
            get
            {
                return armourExamine;
            }
            set
            {
                armourExamine = value;
                OnPropertyChanged("ArmourExamine");
            }
        }

        private string armourImagePath;
        public string ArmourImagePath
        {
            get
            {
                return armourImagePath;
            }
            set
            {
                armourImagePath = value;
                OnPropertyChanged("ArmourImagePath");
            }
        }

        private string armourStatus;
        public string ArmourStatus
        {
            get
            {
                return armourStatus;
            }
            set
            {
                armourStatus = value;
                OnPropertyChanged("ArmourStatus");
            }
        }

        private int? armourDeleteID;
        public int? ArmourDeleteID
        {
            get
            {
                return armourDeleteID;
            }
            set
            {
                armourDeleteID = value;
                OnPropertyChanged("ArmourDeleteID");
            }
        }

        private int? armourSwapFrom;
        public int? ArmourSwapFrom
        {
            get
            {
                return armourSwapFrom;
            }
            set
            {
                armourSwapFrom = value;
                OnPropertyChanged("ArmourSwapFrom");
            }
        }

        private int? armourSwapTo;
        public int? ArmourSwapTo
        {
            get
            {
                return armourSwapTo;
            }
            set
            {
                armourSwapTo = value;
                OnPropertyChanged("ArmourSwapTo");
            }
        }
        #endregion

        #region Familiars
        private void SetupFamiliars()
        {
            AddFamiliars();
        }

        private ObservableCollection<Familiar> familiars;
        public ObservableCollection<Familiar> Familiars
        {
            get
            {
                return familiars;
            }
            set
            {
                familiars = value;
                OnPropertyChanged("Familiars");
            }
        }
        private void AddFamiliars()
        {
            if (Familiars != null)
            {
                Familiars.Clear();
            }
            Familiars = Familiar.GetAllFamiliars();
            Log.Info("Familiars Loaded.");
        }

        private string familiarName;
        public string FamiliarName
        {
            get
            {
                return familiarName;
            }
            set
            {
                familiarName = value;
                OnPropertyChanged("FamiliarName");
            }
        }

        private int familiarQuantity;
        public int FamiliarQuantity
        {
            get
            {
                return familiarQuantity;
            }
            set
            {
                familiarQuantity = value;
                OnPropertyChanged("FamiliarQuantity");
            }
        }

        private int familiarCost;
        public int FamiliarCost
        {
            get
            {
                return familiarCost;
            }
            set
            {
                familiarCost = value;
                OnPropertyChanged("FamiliarCost");
            }
        }

        private double familiarCostModifier;
        public double FamiliarCostModifier
        {
            get
            {
                return familiarCostModifier;
            }
            set
            {
                familiarCostModifier = value;
                OnPropertyChanged("FamiliarCostModifier");
            }
        }

        private int familiarLevel;
        public int FamiliarLevel
        {
            get
            {
                return familiarLevel;
            }
            set
            {
                familiarLevel = value;
                OnPropertyChanged("FamiliarLevel");
            }
        }

        private int familiarDPS;
        public int FamiliarDPS
        {
            get
            {
                return familiarDPS;
            }
            set
            {
                familiarDPS = value;
                OnPropertyChanged("FamiliarDPS");
            }
        }

        private string familiarImagePath;
        public string FamiliarImagePath
        {
            get
            {
                return familiarImagePath;
            }
            set
            {
                familiarImagePath = value;
                OnPropertyChanged("FamiliarImagePath");
            }
        }

        private string familiarStatus;
        public string FamiliarStatus
        {
            get
            {
                return familiarStatus;
            }
            set
            {
                familiarStatus = value;
                OnPropertyChanged("FamiliarStatus");
            }
        }

        private int? familiarDeleteID;
        public int? FamiliarDeleteID
        {
            get
            {
                return familiarDeleteID;
            }
            set
            {
                familiarDeleteID = value;
                OnPropertyChanged("FamiliarDeleteID");
            }
        }

        private int? familiarSwapFrom;
        public int? FamiliarSwapFrom
        {
            get
            {
                return familiarSwapFrom;
            }
            set
            {
                familiarSwapFrom = value;
                OnPropertyChanged("FamiliarSwapFrom");
            }
        }

        private int? familiarSwapTo;
        public int? FamiliarSwapTo
        {
            get
            {
                return familiarSwapTo;
            }
            set
            {
                familiarSwapTo = value;
                OnPropertyChanged("FamiliarSwapTo");
            }
        }
        #endregion

        #region Commands

        #region Enemies
        private ICommand enemyAddEntry;
        public ICommand EnemyAddEntry
        {
            get
            {
                if (enemyAddEntry == null)
                {
                    enemyAddEntry = new RelayCommand(EnemyAddEntryEx, null);
                }
                return enemyAddEntry;
            }
        }
        private void EnemyAddEntryEx(object p)
        {
            if (EnemyUpdateID != null)
            {
                EnemyUpdateEntryEx();
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "INSERT INTO Enemies (Name,MaxHP,Attack,Defence,Range,Magic,BaseDPS,SplitXP,HPXP,MinGP,MaxGP,Weakness,AttackStyle,ImagePath) ";

            query += "VALUES ('" + EnemyName + "'," + EnemyMaxHP + "," + EnemyAttack + "," + EnemyDefence + "," + EnemyRange + "," + EnemyMagic + ",";
            query += EnemyBaseDPS + "," + EnemySplitXP + "," + EnemyHPXP + "," + EnemyMinGP + "," + EnemyMaxGP + ",'" + EnemyWeakness + "','";
            query += EnemyAttackStyle + "','" + EnemyImagePath + "');";

            x = db.ExecuteNonQuery(query);

            EnemyStatus = x + " Record(s) Added.";

            EnemyName = "";
            EnemyMaxHP = 0;
            EnemyAttack = 0;
            EnemyDefence = 0;
            EnemyRange = 0;
            EnemyMagic = 0;
            EnemyBaseDPS = 0;
            EnemySplitXP = 0;
            EnemyHPXP = 0;
            EnemyMinGP = 0;
            EnemyMaxGP = 0;
            EnemyWeakness = "None";
            EnemyAttackStyle = "None";
            EnemyImagePath = "";

            EnemyUpdateID = null;

            AddEnemies();
        }
        private void EnemyUpdateEntryEx()
        {
            bool Found = false;

            foreach (Enemy E in Enemies)
            {
                if (E.ID == Convert.ToInt32(EnemyUpdateID))
                {
                    Found = true;
                }
            }

            if (!Found)
            {
                EnemyStatus = "Entry ID Not found!";
                return;
            }

            int x;
            int itemsSet = 0;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE Enemies SET";

            if (EnemyName != "")
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " Name = '" + EnemyName + "'";
            }

            if (EnemyMaxHP != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " MaxHP = " + EnemyMaxHP;
            }

            if (EnemyAttack != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " Attack = " + EnemyAttack;
            }

            if (EnemyDefence != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " Defence = " + EnemyDefence;
            }

            if (EnemyRange != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " Range = " + EnemyRange;
            }

            if (EnemyMagic != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " Magic = " + EnemyMagic;
            }

            if (EnemyBaseDPS != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " BaseDPS = " + EnemyBaseDPS;
            }

            if (EnemySplitXP != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " SplitXP = " + EnemySplitXP;
            }

            if (EnemyHPXP != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " HPXP = " + EnemyHPXP;
            }

            if (EnemyMinGP != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " MinGP = " + EnemyMinGP;
            }

            if (EnemyMaxGP != 0)
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " MaxGP = " + EnemyMaxGP;
            }

            if (EnemyWeakness != "None")
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " Weakness = '" + EnemyWeakness + "'";
            }

            if (EnemyAttackStyle != "None")
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " AttackStyle = '" + EnemyAttackStyle + "'";
            }

            if (EnemyImagePath != "")
            {
                if (itemsSet > 0)
                {
                    query += ",";
                }
                itemsSet++;
                query += " ImagePath = '" + EnemyImagePath + "'";
            }

            if (itemsSet == 0)
            {
                MessageBoxResult check = System.Windows.MessageBox.Show("No field changes were found. Would you like to reset Weakness and AttackStyle? ", "No changes found?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                if (check == MessageBoxResult.No)
                {
                    EnemyStatus = "Update Cancelled.";
                    return;
                }
                query += " Weakness = '" + EnemyWeakness + "'";
                query += ", AttackStyle = '" + EnemyAttackStyle + "'";
            }

            query += " WHERE ID = " + EnemyUpdateID + ";";

            x = db.ExecuteNonQuery(query);

            EnemyStatus = x + " Record(s) Updated.";

            EnemyName = "";
            EnemyMaxHP = 0;
            EnemyAttack = 0;
            EnemyDefence = 0;
            EnemyRange = 0;
            EnemyMagic = 0;
            EnemyBaseDPS = 0;
            EnemySplitXP = 0;
            EnemyHPXP = 0;
            EnemyMinGP = 0;
            EnemyMaxGP = 0;
            EnemyWeakness = "None";
            EnemyAttackStyle = "None";
            EnemyImagePath = "";

            EnemyUpdateID = null;

            AddEnemies();
        }

        private ICommand enemyDeleteEntry;
        public ICommand EnemyDeleteEntry
        {
            get
            {
                if (enemyDeleteEntry == null)
                {
                    enemyDeleteEntry = new RelayCommand(EnemyDeleteEntryEx, null);
                }
                return enemyDeleteEntry;
            }
        }
        private void EnemyDeleteEntryEx(object p)
        {
            if (EnemyDeleteID == null)
            {
                EnemyStatus = "Please enter an ID before attempting to delete.";
                return;
            }

            MessageBoxResult check = System.Windows.MessageBox.Show("Deleting this will be irreversible. Are you sure you want to delete entry " + EnemyDeleteID + "?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (check == MessageBoxResult.No)
            {
                EnemyStatus = "Delete Cancelled.";
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "DELETE FROM Enemies WHERE ID = " + EnemyDeleteID;

            x = db.ExecuteNonQuery(query);

            EnemyStatus = x + " Record(s) Deleted.";

            EnemyDeleteID = null;

            AddEnemies();
        }

        private ICommand enemySwapEntry;
        public ICommand EnemySwapEntry
        {
            get
            {
                if (enemySwapEntry == null)
                {
                    enemySwapEntry = new RelayCommand(EnemySwapEntryEx, null);
                }
                return enemySwapEntry;
            }
        }
        private void EnemySwapEntryEx(object p)
        {
            bool Taken = false;

            foreach (Enemy E in Enemies)
            {
                if (E.ID == Convert.ToInt32(EnemySwapTo))
                {
                    Taken = true;
                }
            }

            if (Taken)
            {
                EnemyStatus = "That slot is already taken! Be careful where you swap an entry to!";
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE Enemies SET ID = " + EnemySwapTo + " WHERE ID = " + EnemySwapFrom + ";";

            x = db.ExecuteNonQuery(query);

            EnemyStatus = x + " Record(s) Swapped.";

            EnemySwapFrom = null;
            EnemySwapTo = null;

            AddEnemies();

        }

        private ICommand enemyReseedTable;
        public ICommand EnemyReseedTable
        {
            get
            {
                if (enemyReseedTable == null)
                {
                    enemyReseedTable = new RelayCommand(EnemyReseedTableEx, null);
                }
                return enemyReseedTable;
            }
        }
        private void EnemyReseedTableEx(object p)
        {
            int c = Enemies.Count - 1;

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE SQLITE_SEQUENCE SET seq = " + c + " WHERE name = 'Enemies';";

            x = db.ExecuteNonQuery(query);

            EnemyStatus = x + " Table(s) Reseeded.";

            AddEnemies();
        }

        private ICommand enemyRowSel;
        public ICommand EnemyRowSel
        {
            get
            {
                if(enemyRowSel == null)
                {
                    enemyRowSel = new RelayCommand(EnemyRowSelEx, null);
                }
                return enemyRowSel;
            }
        }
        private void EnemyRowSelEx(object p)
        {
            System.Windows.Forms.MessageBox.Show("Test");
        }
        #endregion

        #region Weapons
        private ICommand weaponAddEntry;
        public ICommand WeaponAddEntry
        {
            get
            {
                if (weaponAddEntry == null)
                {
                    weaponAddEntry = new RelayCommand(WeaponAddEntryEx, null);
                }
                return weaponAddEntry;
            }
        }
        private void WeaponAddEntryEx(object p)
        {
            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "INSERT INTO Weapons (Name,Category,Type,Slot,RequiredSkill,RequiredLevel,Damage,Accuracy,Armour,LifePoints,Prayer,StrBonus,";
            query += "RangeBonus,MagicBonus,Cost,Examine,ImagePath) ";

            query += "VALUES ('" + WeaponName + "','" + WeaponCategory + "','" + WeaponType + "','" + WeaponSlot + "','" + WeaponRequiredSkill + "',";
            query += WeaponRequiredLevel + "," + WeaponDamage + "," + WeaponAccuracy + "," + WeaponArmour + "," + WeaponLifePoints + "," + WeaponPrayer + ",";
            query += WeaponStrBonus + "," + WeaponRangeBonus + "," + WeaponMagicBonus + "," + WeaponCost + ",'" + WeaponExamine + "','" + WeaponImagePath + "');";

            x = db.ExecuteNonQuery(query);

            WeaponStatus = x + " Record(s) Added.";

            WeaponName = "";
            WeaponCategory = "Bronze";
            WeaponType = "Melee";
            WeaponSlot = "Weapon";
            WeaponRequiredSkill = "Attack";
            WeaponRequiredLevel = 0;
            WeaponDamage = 0;
            WeaponAccuracy = 0;
            WeaponArmour = 0;
            WeaponLifePoints = 0;
            WeaponPrayer = 0;
            WeaponStrBonus = 0;
            WeaponRangeBonus = 0;
            WeaponMagicBonus = 0;
            WeaponCost = 0;
            WeaponExamine = "";
            WeaponImagePath = "";

            AddWeapons();
        }

        private ICommand weaponDeleteEntry;
        public ICommand WeaponDeleteEntry
        {
            get
            {
                if (weaponDeleteEntry == null)
                {
                    weaponDeleteEntry = new RelayCommand(WeaponDeleteEntryEx, null);
                }
                return weaponDeleteEntry;
            }
        }
        private void WeaponDeleteEntryEx(object p)
        {
            if (WeaponDeleteID == null)
            {
                WeaponStatus = "Please enter an ID before attempting to delete.";
                return;
            }

            MessageBoxResult check = System.Windows.MessageBox.Show("Deleting this will be irreversible. Are you sure you want to delete entry " + WeaponDeleteID + "?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (check == MessageBoxResult.No)
            {
                WeaponStatus = "Delete Cancelled.";
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "DELETE FROM Weapons WHERE ID = " + WeaponDeleteID;

            x = db.ExecuteNonQuery(query);

            WeaponStatus = x + " Record(s) Deleted.";

            WeaponDeleteID = null;

            AddWeapons();
        }

        private ICommand weaponSwapEntry;
        public ICommand WeaponSwapEntry
        {
            get
            {
                if (weaponSwapEntry == null)
                {
                    weaponSwapEntry = new RelayCommand(WeaponSwapEntryEx, null);
                }
                return weaponSwapEntry;
            }
        }
        private void WeaponSwapEntryEx(object p)
        {
            bool Taken = false;

            foreach (Equipment E in Weapons)
            {
                if (E.ID == Convert.ToInt32(WeaponSwapTo))
                {
                    Taken = true;
                }
            }

            if (Taken)
            {
                WeaponStatus = "That slot is already taken! Be careful where you swap an entry to!";
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE Weapons SET ID = " + WeaponSwapTo + " WHERE ID = " + WeaponSwapFrom + ";";

            x = db.ExecuteNonQuery(query);

            WeaponStatus = x + " Record(s) Swapped.";

            WeaponSwapFrom = null;
            WeaponSwapTo = null;

            AddWeapons();
        }

        private ICommand weaponReseedTable;
        public ICommand WeaponReseedTable
        {
            get
            {
                if (weaponReseedTable == null)
                {
                    weaponReseedTable = new RelayCommand(WeaponReseedTableEx, null);
                }
                return weaponReseedTable;
            }
        }
        private void WeaponReseedTableEx(object p)
        {
            int c = Weapons.Count - 1;

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE SQLITE_SEQUENCE SET seq = " + c + " WHERE name = 'Weapons';";

            x = db.ExecuteNonQuery(query);

            WeaponStatus = x + " Table(s) Reseeded.";

            AddWeapons();
        }

        #endregion

        #region Armours
        private ICommand armourAddEntry;
        public ICommand ArmourAddEntry
        {
            get
            {
                if (armourAddEntry == null)
                {
                    armourAddEntry = new RelayCommand(ArmourAddEntryEx, null);
                }
                return armourAddEntry;
            }
        }
        private void ArmourAddEntryEx(object p)
        {
            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "INSERT INTO Armours (Name,Category,Type,Slot,RequiredSkill,RequiredLevel,Damage,Accuracy,Armour,LifePoints,Prayer,StrBonus,";
            query += "RangeBonus,MagicBonus,Cost,Examine,ImagePath) ";

            query += "VALUES ('" + ArmourName + "','" + ArmourCategory + "','" + ArmourType + "','" + ArmourSlot + "','" + ArmourRequiredSkill + "',";
            query += ArmourRequiredLevel + "," + ArmourDamage + "," + ArmourAccuracy + "," + ArmourArmour + "," + ArmourLifePoints + "," + ArmourPrayer + ",";
            query += ArmourStrBonus + "," + ArmourRangeBonus + "," + ArmourMagicBonus + "," + ArmourCost + ",'" + ArmourExamine + "','" + ArmourImagePath + "');";

            x = db.ExecuteNonQuery(query);

            ArmourStatus = x + " Record(s) Added.";

            ArmourName = "";
            ArmourCategory = "Bronze";
            ArmourType = "Melee";
            ArmourSlot = "Shield";
            ArmourRequiredSkill = "Defence";
            ArmourRequiredLevel = 0;
            ArmourDamage = 0;
            ArmourAccuracy = 0;
            ArmourArmour = 0;
            ArmourLifePoints = 0;
            ArmourPrayer = 0;
            ArmourStrBonus = 0;
            ArmourRangeBonus = 0;
            ArmourMagicBonus = 0;
            ArmourCost = 0;
            ArmourExamine = "";
            ArmourImagePath = "";

            AddArmours();
        }

        private ICommand armourDeleteEntry;
        public ICommand ArmourDeleteEntry
        {
            get
            {
                if (armourDeleteEntry == null)
                {
                    armourDeleteEntry = new RelayCommand(ArmourDeleteEntryEx, null);
                }
                return armourDeleteEntry;
            }
        }
        private void ArmourDeleteEntryEx(object p)
        {
            if (ArmourDeleteID == null)
            {
                ArmourStatus = "Please enter an ID before attempting to delete.";
                return;
            }

            MessageBoxResult check = System.Windows.MessageBox.Show("Deleting this will be irreversible. Are you sure you want to delete entry " + ArmourDeleteID + "?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (check == MessageBoxResult.No)
            {
                ArmourStatus = "Delete Cancelled.";
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "DELETE FROM Armours WHERE ID = " + ArmourDeleteID;

            x = db.ExecuteNonQuery(query);

            ArmourStatus = x + " Record(s) Deleted.";

            ArmourDeleteID = null;

            AddArmours();
        }

        private ICommand armourSwapEntry;
        public ICommand ArmourSwapEntry
        {
            get
            {
                if (armourSwapEntry == null)
                {
                    armourSwapEntry = new RelayCommand(ArmourSwapEntryEx, null);
                }
                return armourSwapEntry;
            }
        }
        private void ArmourSwapEntryEx(object p)
        {
            bool Taken = false;

            foreach (Equipment E in Armours)
            {
                if (E.ID == Convert.ToInt32(ArmourSwapTo))
                {
                    Taken = true;
                }
            }

            if (Taken)
            {
                ArmourStatus = "That slot is already taken! Be careful where you swap an entry to!";
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE Armours SET ID = " + ArmourSwapTo + " WHERE ID = " + ArmourSwapFrom + ";";

            x = db.ExecuteNonQuery(query);

            ArmourStatus = x + " Record(s) Swapped.";

            ArmourSwapFrom = null;
            ArmourSwapTo = null;

            AddArmours();
        }

        private ICommand armourReseedTable;
        public ICommand ArmourReseedTable
        {
            get
            {
                if (armourReseedTable == null)
                {
                    armourReseedTable = new RelayCommand(ArmourReseedTableEx, null);
                }
                return armourReseedTable;
            }
        }
        private void ArmourReseedTableEx(object p)
        {
            int c = Armours.Count - 1;

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE SQLITE_SEQUENCE SET seq = " + c + " WHERE name = 'Armours';";

            x = db.ExecuteNonQuery(query);

            ArmourStatus = x + " Table(s) Reseeded.";

            AddArmours();
        }
        #endregion

        #region Familiars
        private ICommand familiarAddEntry;
        public ICommand FamiliarAddEntry
        {
            get
            {
                if (familiarAddEntry == null)
                {
                    familiarAddEntry = new RelayCommand(FamiliarAddEntryEx, null);
                }
                return familiarAddEntry;
            }
        }
        private void FamiliarAddEntryEx(object p)
        {
            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "INSERT INTO Familiars (Name,Quantity,Cost,CostModifier,Level,DPS,ImagePath) ";

            query += "VALUES ('" + FamiliarName + "'," + FamiliarQuantity + "," + FamiliarCost + "," + FamiliarCostModifier + "," + FamiliarLevel + ",";
            query += FamiliarDPS + ",'" + FamiliarImagePath + "');";

            x = db.ExecuteNonQuery(query);

            FamiliarStatus = x + " Record(s) Added.";

            FamiliarName = "";
            FamiliarQuantity = 0;
            FamiliarCost = 0;
            FamiliarCostModifier = 0;
            FamiliarLevel = 0;
            FamiliarDPS = 0;
            FamiliarImagePath = "";

            AddFamiliars();
        }

        private ICommand familiarDeleteEntry;
        public ICommand FamiliarDeleteEntry
        {
            get
            {
                if (familiarDeleteEntry == null)
                {
                    familiarDeleteEntry = new RelayCommand(FamiliarDeleteEntryEx, null);
                }
                return familiarDeleteEntry;
            }
        }
        private void FamiliarDeleteEntryEx(object p)
        {
            if (FamiliarDeleteID == null)
            {
                FamiliarStatus = "Please enter an ID before attempting to delete.";
                return;
            }

            MessageBoxResult check = System.Windows.MessageBox.Show("Deleting this will be irreversible. Are you sure you want to delete entry " + ArmourDeleteID + "?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (check == MessageBoxResult.No)
            {
                FamiliarStatus = "Delete Cancelled.";
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "DELETE FROM Familiars WHERE ID = " + FamiliarDeleteID;

            x = db.ExecuteNonQuery(query);

            FamiliarStatus = x + " Record(s) Deleted.";

            FamiliarDeleteID = null;

            AddFamiliars();
        }

        private ICommand familiarSwapEntry;
        public ICommand FamiliarSwapEntry
        {
            get
            {
                if (familiarSwapEntry == null)
                {
                    familiarSwapEntry = new RelayCommand(FamiliarSwapEntryEx, null);
                }
                return familiarSwapEntry;
            }
        }
        private void FamiliarSwapEntryEx(object p)
        {
            bool Taken = false;

            foreach (Familiar F in Familiars)
            {
                if (F.ID == Convert.ToInt32(FamiliarSwapTo))
                {
                    Taken = true;
                }
            }

            if (Taken)
            {
                FamiliarStatus = "That slot is already taken! Be careful where you swap an entry to!";
                return;
            }

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE Familiars SET ID = " + FamiliarSwapTo + " WHERE ID = " + FamiliarSwapFrom + ";";

            x = db.ExecuteNonQuery(query);

            FamiliarStatus = x + " Record(s) Swapped.";

            FamiliarSwapFrom = null;
            FamiliarSwapTo = null;

            AddFamiliars();
        }

        private ICommand familiarReseedTable;
        public ICommand FamiliarReseedTable
        {
            get
            {
                if (familiarReseedTable == null)
                {
                    familiarReseedTable = new RelayCommand(FamiliarReseedTableEx, null);
                }
                return familiarReseedTable;
            }
        }
        private void FamiliarReseedTableEx(object p)
        {
            int c = Familiars.Count - 1;

            int x;
            var db = new SQLiteDatabase();

            string query;

            query = "UPDATE SQLITE_SEQUENCE SET seq = " + c + " WHERE name = 'Familiars';";

            x = db.ExecuteNonQuery(query);

            FamiliarStatus = x + " Table(s) Reseeded.";

            AddFamiliars();
        }

        #endregion

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
