﻿using log4net;
using log4net.Config;
using Mantin.Controls.Wpf.Notification;
using Parse;
using RSClick.Commands;
using RSClick.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace RSClick.ViewModels
{
    internal class MainWindowViewModel : INotifyPropertyChanged
    {
        int idleKills = 0;
        int idleEnemy = 0;

        MainWindow view;
        Player player = new Player();
        Enemy enemy = new Enemy();
        ParseDB PDB = new ParseDB(true);
        Familiar familiar = new Familiar();
        Timer DamageTimer = new Timer(1000);
        Timer WindowLoadedTimer = new Timer(100);
        Timer WildernessLeaveTimer = new Timer(100);
        Timer WildernessEntryTimer = new Timer(100);
        Timer WildernessCountdownTimer = new Timer(1000);
        Timer WildernessBattleTimer = new Timer(1000);
        Timer WildernessBattleOverTimer = new Timer(5000);
        Random rnd = new Random();
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly ILog DamageLog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType + ".Damage");
        private static readonly ILog DebugLog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType + ".Debug");
        private static readonly ILog ParseLog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType + ".Parse");

        public MainWindowViewModel(MainWindow win)
        {
            view = win;
            programVersion = new ProgramVersion();
            player.PropertyChanged += (s, e) => UpdateEntity();
            enemy.PropertyChanged += (s, e) => UpdateEntity();
            PDB.PropertyChanged += (s, e) => UpdateEntity();
            //weapon.PropertyChanged += (s, e) => UpdateEntity();

            DBMan();

            AddEnemies();
            UpdateEnemy();

            AddWeapons();
            AddArmours();

            AddFamiliars();

            AddWildernessPlayers();

            //TIMERS coming here.

            LoadSettings();

            PlayerHP = PlayerMaxHP;

            DamageTimer.Elapsed += DamageTimerTick;
            DamageTimer.Start();

            WindowLoadedTimer.Elapsed += WindowLoadedTimerTick;

            WildernessLeaveTimer.Elapsed += WildernessTimerTick;
            WildernessEntryTimer.Elapsed += WildernessEntryTimerTick;
            WildernessCountdownTimer.Elapsed += WildernessCountdown;
            WildernessBattleTimer.Elapsed += WildernessBattleSequence;
            WildernessBattleOverTimer.Elapsed += WildernessBattleOver;

            testparse();

            ParseDB.SetStats(
                Att: AttackLevel,
                Str: StrengthLevel,
                Def: DefenceLevel,
                HP: ConstitutionLevel,
                Rng: RangeLevel,
                Mage: MagicLevel,
                Summ: SummoningLevel,
                Cbt: CombatLevel);

            System.Threading.ThreadPool.QueueUserWorkItem(delegate { ParseDB ParseConnector = new ParseDB(); }, null);

            ParseAnalytics.TrackAppOpenedAsync();




            Log.Debug("MainViewModel Initialised. Program Version: " + ProgramVersion);
        }

        private async void testparse()
        {
            var query = new ParseQuery<ParseObject>("TestObject");
            IEnumerable<ParseObject> result = await query.FindAsync();

            foreach (ParseObject row in result)
            {
                //System.Windows.Forms.MessageBox.Show(row.Get<string>("foo"));
            }

            var testObject = new ParseObject("TestObject");
            testObject["foo"] = "bar";
            await testObject.SaveAsync();
            //System.Windows.Forms.MessageBox.Show(testObject.ObjectId);





            var dimensions = new Dictionary<string, string> {
              // What type of news is this?
              { "category", "politics" },
              // Is it a weekday or the weekend?
              { "dayType", "weekday" }
            };
            // Send the dimensions to Parse along with the 'read' event

            await ParseAnalytics.TrackEventAsync("read", dimensions);

        }

        #region Boot, Refresh and Unload
        private void UpdateEntity()
        {
            OnPropertyChanged(null); //Listens to Player model and updates whenever there is a change. :)
        }

        private void LoadSettings()
        {
            Log.Debug("Begin Loading Settings.");
            if (Settings.Player.Default.SettingNeedUpgrade)
            {
                Log.Debug("Player Settings being upgraded!");
                Settings.Player.Default.Upgrade();
                Settings.Player.Default.SettingNeedUpgrade = false;
            }
            if (Settings.Tutorial.Default.NeedUpgrade)
            {
                Log.Debug("Tutorial Settings being upgraded!");
                Settings.Tutorial.Default.Upgrade();
                Settings.Tutorial.Default.NeedUpgrade = false;
            }
            if (Settings.ParseSettings.Default.NeedUpgrade)
            {
                Log.Debug("ParseSettings being upgraded!");
                Settings.ParseSettings.Default.Upgrade();
                Settings.ParseSettings.Default.NeedUpgrade = false;
            }

            AttackXP = Settings.Player.Default.SettingAttackXP;
            StrengthXP = Settings.Player.Default.SettingStrengthXP;
            DefenceXP = Settings.Player.Default.SettingDefenceXP;
            ConstitutionXP = Settings.Player.Default.SettingConstitutionXP;
            RangeXP = Settings.Player.Default.SettingRangeXP;
            MagicXP = Settings.Player.Default.SettingMagicXP;
            SummoningXP = Settings.Player.Default.SettingSummoningXP;
            PlayerGP = Settings.Player.Default.SettingGP;
            EnemySelectEx(Settings.Player.Default.SettingCurrentEnemy);
            SetStance(Settings.Player.Default.SettingCombatStance);
            CurrentWeaponSlot = Settings.Player.Default.SettingCurrentWeapon;
            CurrentHeadSlot = Settings.Player.Default.SettingCurrentHead;
            CurrentAuraSlot = Settings.Player.Default.SettingCurrentAura;
            CurrentPocketSlot = Settings.Player.Default.SettingCurrentPocket;
            CurrentCapeSlot = Settings.Player.Default.SettingCurrentCape;
            CurrentNeckSlot = Settings.Player.Default.SettingCurrentNeck;
            CurrentAmmoSlot = Settings.Player.Default.SettingCurrentAmmo;
            CurrentBodySlot = Settings.Player.Default.SettingCurrentBody;
            CurrentShieldSlot = Settings.Player.Default.SettingCurrentShield;
            CurrentLegsSlot = Settings.Player.Default.SettingCurrentLegs;
            CurrentHandsSlot = Settings.Player.Default.SettingCurrentHands;
            CurrentFeetSlot = Settings.Player.Default.SettingCurrentFeet;
            CurrentRingSlot = Settings.Player.Default.SettingCurrentRing;
            AuraSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingAuraType);
            HeadSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingHeadType);
            PocketSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingPocketType);
            CapeSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingCapeType);
            NeckSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingNeckType);
            AmmoSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingAmmoType);
            WeaponSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingWeaponType);
            BodySlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingBodyType);
            ShieldSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingShieldType);
            LegsSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingLegsType);
            HandsSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingHandsType);
            FeetSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingFeetType);
            RingSlotType = (Player.SlotType)Enum.Parse(typeof(Player.SlotType), Settings.Player.Default.SettingRingType);


            if (TotalDPS > 0) //Implement Resting Bonus thing here. (Calculate what happened whilst the app was closed)
            {
                TimeSpan duration;
                duration = DateTime.UtcNow - Settings.Player.Default.SettingDateTime;
                int seconds = Convert.ToInt32(Math.Round(duration.TotalSeconds));

                int EnemyID = 0;
                int Kills = 0;

                foreach (Enemy e in enemies)
                {
                    int SecondsToKill = 0;
                    int SecondsToDeath = 0;

                    if (TotalDPS > 0)
                    {
                        SecondsToKill = Convert.ToInt32(Math.Ceiling(e.MaxHP / Convert.ToDouble(TotalDPS)));
                    }
                    if (e.BaseDPS > 0)
                    {
                        SecondsToDeath = Convert.ToInt32(Math.Ceiling(PlayerMaxHP / Convert.ToDouble(e.BaseDPS)));
                    }

                    if (SecondsToKill <= SecondsToDeath)
                    {
                        EnemyID = e.ID;
                    }
                }

                if (EnemyID > 0)
                {
                    int SecondsToKill = Convert.ToInt32(Math.Ceiling(enemies[EnemyID].MaxHP / Convert.ToDouble(TotalDPS)));

                    Kills = Convert.ToInt32(Math.Floor(seconds / Convert.ToDouble(SecondsToKill)));
                }

                idleKills = Kills;
                idleEnemy = EnemyID;

            }

            OnPropertyChanged("WildernessEntryLevel");

            Log.Debug("Settings Loaded.");
        }

        private void AwardIdleDPS()
        {

        }

        private void DBMan()
        {
            try
            {
                var db = new SQLiteDatabase();
                DataTable recipe;
                String query = "create table if not exists Weapons ([ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[Name] NVARCHAR(128)  NULL,[Category] NVARCHAR(50)  NULL,[Type] NVARCHAR(50)  NULL,[Slot] NVARCHAR(50)  NULL,[RequiredSkill] NVARCHAR(50)  NULL,[RequiredLevel] INTEGER  NULL,[Damage] INTEGER  NULL,[Accuracy] INTEGER  NULL,[Armour] INTEGER  NULL,[LifePoints] INTEGER  NULL,[Prayer] INTEGER  NULL,[StrBonus] INTEGER  NULL,[RangeBonus] INTEGER  NULL,[MagicBonus] INTEGER  NULL,[Cost] INTEGER  NULL,[Examine] NVARCHAR(512)  NULL,[ImagePath] NVARCHAR(128)  NULL)";
                recipe = db.GetDataTable(query);
                query = "create table if not exists Armours ([ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[Name] NVARCHAR(128)  NULL,[Category] NVARCHAR(50)  NULL,[Type] NVARCHAR(50)  NULL,[Slot] NVARCHAR(50)  NULL,[RequiredSkill] NVARCHAR(50)  NULL,[RequiredLevel] INTEGER  NULL,[Damage] INTEGER  NULL,[Accuracy] INTEGER  NULL,[Armour] INTEGER  NULL,[LifePoints] INTEGER  NULL,[Prayer] INTEGER  NULL,[StrBonus] INTEGER  NULL,[RangeBonus] INTEGER  NULL,[MagicBonus] INTEGER  NULL,[Cost] INTEGER  NULL,[Examine] NVARCHAR(512)  NULL,[ImagePath] NVARCHAR(128)  NULL)";
                recipe = db.GetDataTable(query);
                query = "create table if not exists Enemies ([ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[Name] NVARCHAR(50)  NULL,[MaxHP] INTEGER  NULL,[Attack] INTEGER  NULL,[Defence] INTEGER  NULL,[Range] INTEGER  NULL,[Magic] INTEGER  NULL,[BaseDPS] INTEGER  NULL,[SplitXP] REAL  NULL,[HPXP] REAL  NULL,[MinGP] INTEGER  NULL,[MaxGP] INTEGER  NULL,[Weakness] NVARCHAR(15)  NULL,[AttackStyle] NVARCHAR(15)  NULL,[ImagePath] NVARCHAR(50)  NULL)";
                recipe = db.GetDataTable(query);
                query = "create table if not exists Familiars ([ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,[Name] NVARCHAR(50)  NULL,[Quantity] INTEGER DEFAULT '0' NULL,[Cost] INTEGER  NULL,[CostModifier] REAL  NULL,[Level] INTEGER  NULL,[DPS] INTEGER  NULL,[ImagePath] NVARCHAR(128)  NULL)";
                recipe = db.GetDataTable(query);
            }
            catch (Exception err)
            {
                Log.Error("DBMan Error: " + err.Message, err);
            }
        }

        internal void OnWindowClosing(object sender, CancelEventArgs e)
        {
            Log.Info("Window Closing! Saving Game...");
            //SAVE all settings here!

            ParseDB.UpdateDB();

            Settings.Player.Default.SettingAttackXP = AttackXP;
            Settings.Player.Default.SettingStrengthXP = StrengthXP;
            Settings.Player.Default.SettingDefenceXP = DefenceXP;
            Settings.Player.Default.SettingConstitutionXP = ConstitutionXP;
            Settings.Player.Default.SettingRangeXP = RangeXP;
            Settings.Player.Default.SettingMagicXP = MagicXP;
            Settings.Player.Default.SettingSummoningXP = SummoningXP;
            Settings.Player.Default.SettingGP = PlayerGP;
            Settings.Player.Default.SettingCurrentEnemy = CurrentEnemy;
            Settings.Player.Default.SettingCombatStance = GetStance();
            Settings.Player.Default.SettingCurrentWeapon = CurrentWeaponSlot;
            Settings.Player.Default.SettingCurrentHead = CurrentHeadSlot;
            Settings.Player.Default.SettingCurrentAura = CurrentAuraSlot;
            Settings.Player.Default.SettingCurrentPocket = CurrentPocketSlot;
            Settings.Player.Default.SettingCurrentCape = CurrentCapeSlot;
            Settings.Player.Default.SettingCurrentNeck = CurrentNeckSlot;
            Settings.Player.Default.SettingCurrentAmmo = CurrentAmmoSlot;
            Settings.Player.Default.SettingCurrentBody = CurrentBodySlot;
            Settings.Player.Default.SettingCurrentShield = CurrentShieldSlot;
            Settings.Player.Default.SettingCurrentLegs = CurrentLegsSlot;
            Settings.Player.Default.SettingCurrentHands = CurrentHandsSlot;
            Settings.Player.Default.SettingCurrentFeet = CurrentFeetSlot;
            Settings.Player.Default.SettingCurrentRing = CurrentRingSlot;
            Settings.Player.Default.SettingAuraType = AuraSlotType.ToString();
            Settings.Player.Default.SettingHeadType = HeadSlotType.ToString();
            Settings.Player.Default.SettingPocketType = PocketSlotType.ToString();
            Settings.Player.Default.SettingCapeType = CapeSlotType.ToString();
            Settings.Player.Default.SettingNeckType = NeckSlotType.ToString();
            Settings.Player.Default.SettingAmmoType = AmmoSlotType.ToString();
            Settings.Player.Default.SettingWeaponType = WeaponSlotType.ToString();
            Settings.Player.Default.SettingBodyType = BodySlotType.ToString();
            Settings.Player.Default.SettingShieldType = ShieldSlotType.ToString();
            Settings.Player.Default.SettingLegsType = LegsSlotType.ToString();
            Settings.Player.Default.SettingHandsType = HandsSlotType.ToString();
            Settings.Player.Default.SettingFeetType = FeetSlotType.ToString();
            Settings.Player.Default.SettingRingType = RingSlotType.ToString();

            foreach (Familiar f in familiars)
            {
                try
                {
                    var db = new SQLiteDatabase();
                    DataTable recipe;
                    string query = "UPDATE Familiars SET Quantity=" + f.Quantity + " WHERE ID=" + f.ID;
                    recipe = db.GetDataTable(query);
                }
                catch (Exception err)
                {
                    Log.Fatal("SAVE Error [FAMILIARS]: " + err.Message, err);
                }
            }


            Settings.Player.Default.SettingDateTime = DateTime.UtcNow;

            Settings.Player.Default.Save();
            Settings.Tutorial.Default.Save();
            Settings.ParseSettings.Default.Save();
            Log.Info("Game Saved. Safely exiting...");
        }

        #endregion

        #region Damage Functions
        //Timers
        private void DamageTimerTick(object sender, ElapsedEventArgs e)
        {
            if (CurrentEnemy != 0)
            {
                if (AutoAttack())
                {
                    return;
                }
            }

            int Damage = CurrentEnemyDPS();
            if (Damage > PlayerHP) { Damage = PlayerHP; }
            if (CurrentEnemy == 0) { Damage = 0; }
            PlayerHP -= Damage;
            if (CurrentEnemy > 0)
            {
                PlayerHitSplat = Damage.ToString();
            }
            DamageLog.Info("Enemy Hit the Player! Damage: " + Damage);
        }

        private bool AutoAttack()
        {
            int damage = 0;
            bool KO = false;
            damage = Convert.ToInt32(TotalDPS);

            if (damage >= EnemyHP)
            {
                damage = EnemyHP;
                KO = true;
            }

            EnemyHP -= damage;
            EnemyHitSplat = damage.ToString();
            return KO;
        }

        private int CurrentEnemyRelativeAttack()
        {
            switch (Enemies[CurrentEnemy].AttackStyle)
            {
                case Enemy.attackStyleEnum.Melee:
                    return Enemies[CurrentEnemy].AttackLevel;
                case Enemy.attackStyleEnum.Magic:
                    return Enemies[CurrentEnemy].MagicLevel;
                case Enemy.attackStyleEnum.Range:
                    return Enemies[CurrentEnemy].RangeLevel;
                case Enemy.attackStyleEnum.None:
                    Log.Debug("Whilst retrieving CurrentEnemyRelativeAttack, we recived value of none. Returning a mix of all 3? Check this is intended.");
                    return (Enemies[CurrentEnemy].AttackLevel + Enemies[CurrentEnemy].MagicLevel + Enemies[CurrentEnemy].RangeLevel) / 3;
                default:
                    Log.Error("Defaulted in CurrentEnemyRelativeAttack. Val: " + Enemies[CurrentEnemy].AttackStyle);
                    break;
            }

            return 0;
        }

        public int CurrentEnemyDPS()
        {
            int dps;

            double calc = (Enemies[CurrentEnemy].BaseDPS * CurrentEnemyRelativeAttack() * 7.5) / ((DefenceLevel * 7) + (PlayerTotalArmour / 5));

            //Factor in stance weaknesses etc.
            switch (Enemies[CurrentEnemy].AttackStyle)
            {
                case Enemy.attackStyleEnum.Melee:
                    if (!StanceAttack && !StanceStrength)
                    {
                        if (StanceRange)
                        {
                            calc *= 1.25;
                        }
                        else if (StanceMagic)
                        {
                            calc *= 0.75;
                        }
                    }
                    break;
                case Enemy.attackStyleEnum.Magic:
                    if (!StanceMagic)
                    {
                        if (StanceRange)
                        {
                            calc *= 0.75;
                        }
                        else if (StanceAttack && StanceStrength)
                        {
                            calc *= 1.25;
                        }
                    }
                    break;
                case Enemy.attackStyleEnum.Range:
                    if (!StanceRange)
                    {
                        if (StanceMagic)
                        {
                            calc *= 1.25;
                        }
                        else if (StanceAttack && StanceStrength)
                        {
                            calc *= 0.75;
                        }
                    }
                    break;
                case Enemy.attackStyleEnum.None:
                    Log.Debug("Whilst calculating CurrentEnemyDPS, we recived value of none. Check this is intended.");
                    break;
                default:
                    Log.Error("Defaulted in CurrentEnemyDPS. Val: " + Enemies[CurrentEnemy].AttackStyle);
                    break;
            }

            if (StanceDefence)
            {
                calc = calc / 3 * 2;
            }

            dps = (int)Math.Round(Math.Max(calc, 1));

            return dps;
        }

        private void ManualAttack()
        {
            int hit = rnd.Next(PlayerMinHit, PlayerMaxHit + 1);
            double crit = rnd.NextDouble();
            crit *= 100;

            if (crit <= PlayerCritHitChance)
            {
                hit = PlayerCritHit;
            }

            if (StanceDefence)
            {
                hit = hit / 3 * 2;
            }

            switch (Enemies[CurrentEnemy].Weakness)
            {
                case Enemy.weaknessEnum.None:
                    //no change to damage.
                    break;
                case Enemy.weaknessEnum.Melee:
                    if ((StanceAttack || StanceStrength) && !StanceMagic)
                    {
                        hit = (int)(hit * 1.25);
                    }
                    if (StanceMagic && !(StanceAttack || StanceStrength))
                    {
                        hit = (int)(hit * 0.75);
                    }
                    break;
                case Enemy.weaknessEnum.Range:
                    if (StanceRange && !(StanceAttack || StanceStrength))
                    {
                        hit = (int)(hit * 1.25);
                    }
                    if ((StanceAttack || StanceStrength) && !StanceRange)
                    {
                        hit = (int)(hit * 0.75);
                    }
                    break;
                case Enemy.weaknessEnum.Magic:
                    if (StanceMagic && !StanceRange)
                    {
                        hit = (int)(hit * 1.25);
                    }
                    if (StanceRange && !StanceMagic)
                    {
                        hit = (int)(hit * 0.75);
                    }
                    break;
                default:
                    Log.Error("Switch failed on enemy weakness in ManualAttack. Value: " + Enemies[CurrentEnemy].Weakness);
                    break;
            }

            if (hit > EnemyHP) { hit = EnemyHP; }

            EnemyHP -= hit;
            EnemyHitSplat = hit.ToString();
            DamageLog.Info("Player Hit Enemy: " + hit);
        }

        private void CheckEnemyDeath()
        {
            if (EnemyHP <= 0) //Player is dead.
            {
                if (!Settings.Tutorial.Default.Tut07)
                {
                    Application.Current.Dispatcher.Invoke(new Action(() =>
                    {
                        LeftTabIndex = 0;
                        TutorialTitle = "Congrats!";
                        TutorialDescription = "You killed your first enemy! You will now recieve exp rewards depnding on your combat stance, and will recieve a pile of GP to spend!";
                        TutorialPosition = "Left";
                        DamageTimer.Enabled = false;
                        TutorialBoolEnemy = true;
                        TutorialBoolEnemy = false;
                        DamageTimer.Enabled = true;
                        Settings.Tutorial.Default.Tut07 = true;
                    }));
                }


                ConstitutionXP += Enemies[CurrentEnemy].HPXP;
                if (CombatAttack)
                {
                    AttackXP += Enemies[CurrentEnemy].SplitXP;
                }
                else if (CombatAttackDefence)
                {
                    AttackXP += Enemies[CurrentEnemy].SplitXP / 2;
                    DefenceXP += Enemies[CurrentEnemy].SplitXP / 2;
                }
                else if (CombatMelee)
                {
                    AttackXP += Enemies[CurrentEnemy].SplitXP / 3;
                    StrengthXP += Enemies[CurrentEnemy].SplitXP / 3;
                    DefenceXP += Enemies[CurrentEnemy].SplitXP / 3;
                }
                else if (CombatStrength)
                {
                    StrengthXP += Enemies[CurrentEnemy].SplitXP;
                }
                else if (CombatStrengthDefence)
                {
                    StrengthXP += Enemies[CurrentEnemy].SplitXP / 2;
                    DefenceXP += Enemies[CurrentEnemy].SplitXP / 2;
                }
                else if (CombatRange)
                {
                    RangeXP += Enemies[CurrentEnemy].SplitXP;
                }
                else if (CombatRangeDefence)
                {
                    RangeXP += Enemies[CurrentEnemy].SplitXP / 2;
                    DefenceXP += Enemies[CurrentEnemy].SplitXP / 2;
                }
                else if (CombatMagic)
                {
                    MagicXP += Enemies[CurrentEnemy].SplitXP;
                }
                else if (CombatMagicDefence)
                {
                    MagicXP += Enemies[CurrentEnemy].SplitXP / 2;
                    DefenceXP += Enemies[CurrentEnemy].SplitXP / 2;
                }
                else if (CombatAll)
                {
                    AttackXP += Enemies[CurrentEnemy].SplitXP / 5;
                    StrengthXP += Enemies[CurrentEnemy].SplitXP / 5;
                    DefenceXP += Enemies[CurrentEnemy].SplitXP / 5;
                    RangeXP += Enemies[CurrentEnemy].SplitXP / 5;
                    MagicXP += Enemies[CurrentEnemy].SplitXP / 5;
                }

                double Roll = rnd.NextDouble();
                double Earnt = Roll * Roll * (Enemies[CurrentEnemy].MaxGP - Enemies[CurrentEnemy].MinGP) + Enemies[CurrentEnemy].MinGP;
                Earnt = Math.Round(Earnt, 0);
                PlayerGP += (int)Earnt;

                PlayerHP = PlayerMaxHP;
                EnemyHP = Enemies[CurrentEnemy].MaxHP;

                Log.Info("Enemy died! GP Gained: " + (int)Earnt);

            }
        }

        private void CheckPlayerDeath()
        {
            if (PlayerHP <= 0)
            {
                if (!Settings.Tutorial.Default.Tut06)
                {
                    Application.Current.Dispatcher.Invoke(new Action(() =>
                    {
                        LeftTabIndex = 0;
                        TutorialTitle = "Oh no!";
                        TutorialDescription = "You died! Not to worry, you will respawn right away. Consider choosing a different combat style, gearing up, or selecting an easier enemy.";
                        TutorialPosition = "Right";
                        DamageTimer.Enabled = false;
                        TutorialBoolLeftPanel = true;
                        TutorialBoolLeftPanel = false;
                        DamageTimer.Enabled = true;
                        Settings.Tutorial.Default.Tut06 = true;
                    }));
                }

                long lost = PlayerGP;
                PlayerGP = Convert.ToInt32(Math.Round(PlayerGP * 0.9));
                lost -= PlayerGP;

                PlayerHP = PlayerMaxHP;
                EnemyHP = EnemyMaxHP;

                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    Toast t = new Toast("You died!", "Oh no, you died! the " + Enemies[CurrentEnemy].Name + " killed you, and you lost " + lost + " GP!", NotificationType.Warning);
                }));

                Log.Info("Player Died!!! Enemy responsible: " + Enemies[CurrentEnemy].Name + ". GP Lost: " + lost + ".");
            }
        }

        #endregion

        #region Stats Variables
        public int AttackLevel
        {
            get
            {
                int x = Array.FindLastIndex<double>(player.XPTable, item => item <= AttackXP);
                return x;
            }
            set { }
        }
        public double AttackXP
        {
            get
            {
                return player.AttackXP;
            }
            set
            {
                player.AttackXP = value;
                OnPropertyChanged("AttackXP");
                ParseDB.Combat = CombatLevel;
                ParseDB.Attack = AttackLevel;
            }
        }

        public int StrengthLevel
        {
            get
            {
                int x = Array.FindLastIndex<double>(player.XPTable, item => item <= StrengthXP);
                return x;
            }
            set { }
        }
        public double StrengthXP
        {
            get
            {
                return player.StrengthXP;
            }
            set
            {
                player.StrengthXP = value;
                OnPropertyChanged("StrengthXP");
                ParseDB.Combat = CombatLevel;
                ParseDB.Strength = StrengthLevel;
            }
        }

        public int DefenceLevel
        {
            get
            {
                int x = Array.FindLastIndex<double>(player.XPTable, item => item <= DefenceXP);
                return x;
            }
            set { }
        }
        public double DefenceXP
        {
            get
            {
                return player.DefenceXP;
            }
            set
            {
                player.DefenceXP = value;
                OnPropertyChanged("DefenceXP");
                ParseDB.Combat = CombatLevel;
                ParseDB.Defence = DefenceLevel;
            }
        }

        public int ConstitutionLevel
        {
            get
            {
                int x = Array.FindLastIndex<double>(player.XPTable, item => item <= ConstitutionXP);
                return x;
            }
            set { }
        }
        public double ConstitutionXP
        {
            get
            {
                return player.ConstitutionXP;
            }
            set
            {
                player.ConstitutionXP = value;
                OnPropertyChanged("ConstitutionXP");
                ParseDB.Combat = CombatLevel;
                ParseDB.Constitution = ConstitutionLevel;
            }
        }

        public int RangeLevel
        {
            get
            {
                int x = Array.FindLastIndex<double>(player.XPTable, item => item <= RangeXP);
                return x;
            }
            set { }
        }
        public double RangeXP
        {
            get
            {
                return player.RangeXP;
            }
            set
            {
                player.RangeXP = value;
                OnPropertyChanged("RangeXP");
                ParseDB.Combat = CombatLevel;
                ParseDB.Range = RangeLevel;
            }
        }

        public int MagicLevel
        {
            get
            {
                int x = Array.FindLastIndex<double>(player.XPTable, item => item <= MagicXP);
                return x;
            }
            set { }
        }
        public double MagicXP
        {
            get
            {
                return player.MagicXP;
            }
            set
            {
                player.MagicXP = value;
                OnPropertyChanged("MagicXP");
                ParseDB.Combat = CombatLevel;
                ParseDB.Magic = MagicLevel;
            }
        }

        public int SummoningLevel
        {
            get
            {
                int x = Array.FindLastIndex<double>(player.XPTable, item => item <= SummoningXP);
                return x;
            }
            set { }
        }
        public double SummoningXP
        {
            get
            {
                return player.SummoningXP;
            }
            set
            {
                player.SummoningXP = value;
                OnPropertyChanged("SummoningXP");
                ParseDB.Combat = CombatLevel;
                ParseDB.Summoning = SummoningLevel;
            }
        }

        public int OverallLevel
        {
            get
            {
                return AttackLevel + StrengthLevel + DefenceLevel + ConstitutionLevel + RangeLevel + MagicLevel + SummoningLevel;
            }
            set { }
        }

        #endregion

        #region Stat Percentage Variables

        public int AttackPercent
        {
            get
            {
                double start = player.XPTable[AttackLevel];
                double end = player.XPTable[AttackLevel + 1];
                double comp = AttackXP - start;
                double needed = end - start;
                int ans = Convert.ToInt32(Math.Round(comp / needed * 100));
                return ans;
            }
            set { }
        }

        public int StrengthPercent
        {
            get
            {
                double start = player.XPTable[StrengthLevel];
                double end = player.XPTable[StrengthLevel + 1];
                double comp = StrengthXP - start;
                double needed = end - start;
                int ans = Convert.ToInt32(Math.Round(comp / needed * 100));
                return ans;
            }
            set { }
        }

        public int DefencePercent
        {
            get
            {
                double start = player.XPTable[DefenceLevel];
                double end = player.XPTable[DefenceLevel + 1];
                double comp = DefenceXP - start;
                double needed = end - start;
                int ans = Convert.ToInt32(Math.Round(comp / needed * 100));
                return ans;
            }
            set { }
        }

        public int ConstitutionPercent
        {
            get
            {
                double start = player.XPTable[ConstitutionLevel];
                double end = player.XPTable[ConstitutionLevel + 1];
                double comp = ConstitutionXP - start;
                double needed = end - start;
                int ans = Convert.ToInt32(Math.Round(comp / needed * 100));
                return ans;
            }
            set { }
        }

        public int RangePercent
        {
            get
            {
                double start = player.XPTable[RangeLevel];
                double end = player.XPTable[RangeLevel + 1];
                double comp = RangeXP - start;
                double needed = end - start;
                int ans = Convert.ToInt32(Math.Round(comp / needed * 100));
                return ans;
            }
            set { }
        }

        public int MagicPercent
        {
            get
            {
                double start = player.XPTable[MagicLevel];
                double end = player.XPTable[MagicLevel + 1];
                double comp = MagicXP - start;
                double needed = end - start;
                int ans = Convert.ToInt32(Math.Round(comp / needed * 100));
                return ans;
            }
            set { }
        }

        public int SummoningPercent
        {
            get
            {
                double start = player.XPTable[SummoningLevel];
                double end = player.XPTable[SummoningLevel + 1];
                double comp = SummoningXP - start;
                double needed = end - start;
                int ans = Convert.ToInt32(Math.Round(comp / needed * 100));
                return ans;
            }
            set { }
        }

        #endregion

        #region Stat XP Next Variables

        public double AttackXPNext
        {
            get
            {
                return player.XPTable[AttackLevel + 1];
            }
            set { }
        }

        public double StrengthXPNext
        {
            get
            {
                return player.XPTable[StrengthLevel + 1];
            }
            set { }
        }

        public double DefenceXPNext
        {
            get
            {
                return player.XPTable[DefenceLevel + 1];
            }
            set { }
        }

        public double ConstitutionXPNext
        {
            get
            {
                return player.XPTable[ConstitutionLevel + 1];
            }
            set { }
        }

        public double RangeXPNext
        {
            get
            {
                return player.XPTable[RangeLevel + 1];
            }
            set { }
        }

        public double MagicXPNext
        {
            get
            {
                return player.XPTable[MagicLevel + 1];
            }
            set { }
        }

        public double SummoningXPNext
        {
            get
            {
                return player.XPTable[SummoningLevel + 1];
            }
            set { }
        }

        #endregion

        #region Stat XP Remaining Variables

        public double AttackXPRemaining
        {
            get
            {
                return player.XPTable[AttackLevel + 1] - AttackXP;
            }
            set { }
        }

        public double StrengthXPRemaining
        {
            get
            {
                return player.XPTable[StrengthLevel + 1] - StrengthXP;
            }
            set { }
        }

        public double DefenceXPRemaining
        {
            get
            {
                return player.XPTable[DefenceLevel + 1] - DefenceXP;
            }
            set { }
        }

        public double ConstitutionXPRemaining
        {
            get
            {
                return player.XPTable[ConstitutionLevel + 1] - ConstitutionXP;
            }
            set { }
        }

        public double RangeXPRemaining
        {
            get
            {
                return player.XPTable[RangeLevel + 1] - RangeXP;
            }
            set { }
        }

        public double MagicXPRemaining
        {
            get
            {
                return player.XPTable[MagicLevel + 1] - MagicXP;
            }
            set { }
        }

        public double SummoningXPRemaining
        {
            get
            {
                return player.XPTable[SummoningLevel + 1] - SummoningXP;
            }
            set { }
        }

        #endregion

        #region Tooltip IsOpen Bools

        private bool attackTooltipOpen;
        public bool AttackTooltipOpen
        {
            get
            {
                return attackTooltipOpen;
            }
            set
            {
                attackTooltipOpen = value;
                OnPropertyChanged("AttackTooltipOpen");
            }
        }

        private bool strengthTooltipOpen;
        public bool StrengthTooltipOpen
        {
            get
            {
                return strengthTooltipOpen;
            }
            set
            {
                strengthTooltipOpen = value;
                OnPropertyChanged("StrengthTooltipOpen");
            }
        }

        private bool defenceTooltipOpen;
        public bool DefenceTooltipOpen
        {
            get
            {
                return defenceTooltipOpen;
            }
            set
            {
                defenceTooltipOpen = value;
                OnPropertyChanged("DefenceTooltipOpen");
            }
        }

        private bool constitutionTooltipOpen;
        public bool ConstitutionTooltipOpen
        {
            get
            {
                return constitutionTooltipOpen;
            }
            set
            {
                constitutionTooltipOpen = value;
                OnPropertyChanged("ConstitutionTooltipOpen");
            }
        }

        private bool rangeTooltipOpen;
        public bool RangeTooltipOpen
        {
            get
            {
                return rangeTooltipOpen;
            }
            set
            {
                rangeTooltipOpen = value;
                OnPropertyChanged("RangeTooltipOpen");
            }
        }

        private bool magicTooltipOpen;
        public bool MagicTooltipOpen
        {
            get
            {
                return magicTooltipOpen;
            }
            set
            {
                magicTooltipOpen = value;
                OnPropertyChanged("MagicTooltipOpen");
            }
        }

        private bool summoningTooltipOpen;
        public bool SummoningTooltipOpen
        {
            get
            {
                return summoningTooltipOpen;
            }
            set
            {
                summoningTooltipOpen = value;
                OnPropertyChanged("SummoningTooltipOpen");
            }
        }

        private bool enemyTooltipOpen;
        public bool EnemyTooltipOpen
        {
            get
            {
                return enemyTooltipOpen;
            }
            set
            {
                enemyTooltipOpen = value;
                OnPropertyChanged("EnemyTooltipOpen");
            }
        }

        #endregion

        #region Tooltip XY Offset's

        private double attackOffsetX;
        public double AttackOffsetX
        {
            get
            {
                return attackOffsetX;
            }
            set
            {
                attackOffsetX = value;
                OnPropertyChanged("AttackOffsetX");
            }
        }

        private double attackOffsetY;
        public double AttackOffsetY
        {
            get
            {
                return attackOffsetY;
            }
            set
            {
                attackOffsetY = value;
                OnPropertyChanged("AttackOffsetY");
            }
        }

        private double strengthOffsetX;
        public double StrengthOffsetX
        {
            get
            {
                return strengthOffsetX;
            }
            set
            {
                strengthOffsetX = value;
                OnPropertyChanged("StrengthOffsetX");
            }
        }

        private double strengthOffsetY;
        public double StrengthOffsetY
        {
            get
            {
                return strengthOffsetY;
            }
            set
            {
                strengthOffsetY = value;
                OnPropertyChanged("StrengthOffsetY");
            }
        }

        private double defenceOffsetX;
        public double DefenceOffsetX
        {
            get
            {
                return defenceOffsetX;
            }
            set
            {
                defenceOffsetX = value;
                OnPropertyChanged("DefenceOffsetX");
            }
        }

        private double defenceOffsetY;
        public double DefenceOffsetY
        {
            get
            {
                return defenceOffsetY;
            }
            set
            {
                defenceOffsetY = value;
                OnPropertyChanged("DefenceOffsetY");
            }
        }

        private double constitutionOffsetX;
        public double ConstitutionOffsetX
        {
            get
            {
                return constitutionOffsetX;
            }
            set
            {
                constitutionOffsetX = value;
                OnPropertyChanged("ConstitutionOffsetX");
            }
        }

        private double constitutionOffsetY;
        public double ConstitutionOffsetY
        {
            get
            {
                return constitutionOffsetY;
            }
            set
            {
                constitutionOffsetY = value;
                OnPropertyChanged("ConstitutionOffsetY");
            }
        }

        private double rangeOffsetX;
        public double RangeOffsetX
        {
            get
            {
                return rangeOffsetX;
            }
            set
            {
                rangeOffsetX = value;
                OnPropertyChanged("RangeOffsetX");
            }
        }

        private double rangeOffsetY;
        public double RangeOffsetY
        {
            get
            {
                return rangeOffsetY;
            }
            set
            {
                rangeOffsetY = value;
                OnPropertyChanged("RangeOffsetY");
            }
        }

        private double magicOffsetX;
        public double MagicOffsetX
        {
            get
            {
                return magicOffsetX;
            }
            set
            {
                magicOffsetX = value;
                OnPropertyChanged("MagicOffsetX");
            }
        }

        private double magicOffsetY;
        public double MagicOffsetY
        {
            get
            {
                return magicOffsetY;
            }
            set
            {
                magicOffsetY = value;
                OnPropertyChanged("MagicOffsetY");
            }
        }

        private double summoningOffsetX;
        public double SummoningOffsetX
        {
            get
            {
                return summoningOffsetX;
            }
            set
            {
                summoningOffsetX = value;
                OnPropertyChanged("SummoningOffsetX");
            }
        }

        private double summoningOffsetY;
        public double SummoningOffsetY
        {
            get
            {
                return summoningOffsetY;
            }
            set
            {
                summoningOffsetY = value;
                OnPropertyChanged("SummoningOffsetY");
            }
        }

        private double enemyOffsetX;
        public double EnemyOffsetX
        {
            get
            {
                return enemyOffsetX;
            }
            set
            {
                enemyOffsetX = value;
                OnPropertyChanged("EnemyOffsetX");
            }
        }

        private double enemyOffsetY;
        public double EnemyOffsetY
        {
            get
            {
                return enemyOffsetY;
            }
            set
            {
                enemyOffsetY = value;
                OnPropertyChanged("EnemyOffsetY");
            }
        }

        #endregion

        #region Combat Style Variables

        private bool StanceAttack { get; set; }
        private bool StanceStrength { get; set; }
        private bool StanceDefence { get; set; }
        private bool StanceRange { get; set; }
        private bool StanceMagic { get; set; }

        private void UpdateStance(string Stance)
        {
            StanceAttack = false;
            StanceStrength = false;
            StanceDefence = false;
            StanceRange = false;
            StanceMagic = false;

            switch (Stance)
            {
                case "Attack":
                    StanceAttack = true;
                    break;
                case "AttackDefence":
                    StanceAttack = true;
                    StanceDefence = true;
                    break;
                case "Melee":
                    StanceAttack = true;
                    StanceDefence = true;
                    StanceStrength = true;
                    break;
                case "Strength":
                    StanceStrength = true;
                    break;
                case "StrengthDefence":
                    StanceStrength = true;
                    StanceDefence = true;
                    break;
                case "Range":
                    StanceRange = true;
                    break;
                case "RangeDefence":
                    StanceRange = true;
                    StanceDefence = true;
                    break;
                case "Magic":
                    StanceMagic = true;
                    break;
                case "MagicDefence":
                    StanceMagic = true;
                    StanceDefence = true;
                    break;
                case "All":
                    StanceAttack = true;
                    StanceDefence = true;
                    StanceStrength = true;
                    StanceRange = true;
                    StanceMagic = true;
                    break;
                default:
                    Log.Warn("UpdateStance Fell through the switch statement. Defualting as if CombatAll, but this should be checked.");
                    StanceAttack = true;
                    StanceDefence = true;
                    StanceStrength = true;
                    StanceRange = true;
                    StanceMagic = true;
                    break;
            }
            OnPropertyChanged("PlayerCritHitChance");
            OnPropertyChanged("EnemyDPS");
            OnPropertyChanged("PlayerMinHitAdjusted");
            OnPropertyChanged("PlayerMaxHitAdjusted");
            OnPropertyChanged("PlayerCritHitAdjusted");
        }

        private void SetStance(string Stance)
        {
            switch (Stance)
            {
                case "Attack":
                    CombatAttack = true;
                    break;
                case "AttackDefence":
                    CombatAttackDefence = true;
                    break;
                case "Melee":
                    CombatMelee = true;
                    break;
                case "Strength":
                    CombatStrength = true;
                    break;
                case "StrengthDefence":
                    CombatStrengthDefence = true;
                    break;
                case "Range":
                    CombatRange = true;
                    break;
                case "RangeDefence":
                    CombatRangeDefence = true;
                    break;
                case "Magic":
                    CombatMagic = true;
                    break;
                case "MagicDefence":
                    CombatMagicDefence = true;
                    break;
                case "All":
                    CombatAll = true;
                    break;
                default:
                    Log.Warn("SetStance was called but failed the switch statement. Defaultint to CombatAll, but needs to be checked.");
                    CombatAll = true;
                    break;
            }
        }

        private string GetStance()
        {
            if (CombatAttack)
            {
                return "Attack";
            }
            else if (CombatAttackDefence)
            {
                return "AttackDefence";
            }
            else if (CombatMelee)
            {
                return "Melee";
            }
            else if (CombatStrength)
            {
                return "Strength";
            }
            else if (CombatStrengthDefence)
            {
                return "StrengthDefence";
            }
            else if (CombatRange)
            {
                return "Range";
            }
            else if (CombatRangeDefence)
            {
                return "RangeDefence";
            }
            else if (CombatMagic)
            {
                return "Magic";
            }
            else if (CombatMagicDefence)
            {
                return "MagicDefence";
            }
            else if (CombatAll)
            {
                return "All";
            }
            else
            {
                Log.Error("GetStance() Returned Error. Combat Stance is currently unknown!");
                return "Error";
            }
        }

        public bool CombatAttack
        {
            get
            {
                return player.CombatAttack;
            }
            set
            {
                player.CombatAttack = value;
                OnPropertyChanged("CombatAttack");
                UpdateStance("Attack");
            }
        }

        public bool CombatAttackDefence
        {
            get
            {
                return player.CombatAttackDefence;
            }
            set
            {
                player.CombatAttackDefence = value;
                OnPropertyChanged("CombatAttackDefence");
                UpdateStance("AttackDefence");
            }
        }

        public bool CombatMelee
        {
            get
            {
                return player.CombatMelee;
            }
            set
            {
                player.CombatMelee = value;
                OnPropertyChanged("CombatMelee");
                UpdateStance("Melee");
            }
        }

        public bool CombatStrength
        {
            get
            {
                return player.CombatStrength;
            }
            set
            {
                player.CombatStrength = value;
                OnPropertyChanged("CombatStrength");
                UpdateStance("Strength");
            }
        }

        public bool CombatStrengthDefence
        {
            get
            {
                return player.CombatStrengthDefence;
            }
            set
            {
                player.CombatStrengthDefence = value;
                OnPropertyChanged("CombatStrengthDefence");
                UpdateStance("StrengthDefence");
            }
        }

        public bool CombatRange
        {
            get
            {
                return player.CombatRange;
            }
            set
            {
                player.CombatRange = value;
                OnPropertyChanged("CombatRange");
                UpdateStance("Range");
            }
        }

        public bool CombatRangeDefence
        {
            get
            {
                return player.CombatRangeDefence;
            }
            set
            {
                player.CombatRangeDefence = value;
                OnPropertyChanged("CombatRangeDefence");
                UpdateStance("RangeDefence");
            }
        }

        public bool CombatMagic
        {
            get
            {
                return player.CombatMagic;
            }
            set
            {
                player.CombatMagic = value;
                OnPropertyChanged("CombatMagic");
                UpdateStance("Magic");
            }
        }

        public bool CombatMagicDefence
        {
            get
            {
                return player.CombatMagicDefence;
            }
            set
            {
                player.CombatMagicDefence = value;
                OnPropertyChanged("CombatMagicDefence");
                UpdateStance("MagicDefence");
            }
        }

        public bool CombatAll
        {
            get
            {
                return player.CombatAll;
            }
            set
            {
                player.CombatAll = value;
                OnPropertyChanged("CombatAll");
                UpdateStance("All");
            }
        }

        #endregion

        #region Exposed Enemies Stuff
        private ObservableCollection<Enemy> enemies;
        public ObservableCollection<Enemy> Enemies
        {
            get
            {
                return enemies;
            }
            set
            {
                enemies = value;
                OnPropertyChanged("Enemies");
            }
        }
        private void AddEnemies()
        {
            Enemies = Enemy.GetAll();
            Log.Info("Enemies Loaded.");
        }

        private int currentEnemy = 0;
        public int CurrentEnemy
        {
            get
            {
                return currentEnemy;
            }
            set
            {
                currentEnemy = value;
                OnPropertyChanged("CurrentEnemy");
                Log.Info("Enemy switched! Updating UI...");
                UpdateEnemy();
            }
        }
        private void UpdateEnemy()
        {
            for (int i = 0; i < Enemies.Count(); i++)
            {
                Enemies[i].Border = Color.FromRgb(0, 0, 0).ToString();
            }
            Enemies[CurrentEnemy].Border = Color.FromRgb(255, 0, 0).ToString();

            OnPropertyChanged("EnemyImagePath");
            OnPropertyChanged("EnemyName");
            OnPropertyChanged("EnemyDPS");
            OnPropertyChanged("EnemyMaxHP");
            OnPropertyChanged("PlayerMinHitAdjusted");
            OnPropertyChanged("PlayerMaxHitAdjusted");
            OnPropertyChanged("PlayerCritHitAdjusted");
            Log.Info("Enemy Updated!");
        }

        private string hoverEnemyName;
        public string HoverEnemyName
        {
            get
            {
                return hoverEnemyName;
            }
            set
            {
                hoverEnemyName = value;
                OnPropertyChanged("HoverEnemyName");
            }
        }

        private int hoverEnemyHP;
        public int HoverEnemyHP
        {
            get
            {
                return hoverEnemyHP;
            }
            set
            {
                hoverEnemyHP = value;
                OnPropertyChanged("HoverEnemyHP");
            }
        }

        private int hoverEnemyDPS;
        public int HoverEnemyDPS
        {
            get
            {
                return hoverEnemyDPS;
            }
            set
            {
                hoverEnemyDPS = value;
                OnPropertyChanged("HoverEnemyDPS");
            }
        }

        private string hoverEnemyWeakness;
        public string HoverEnemyWeakness
        {
            get
            {
                return hoverEnemyWeakness;
            }
            set
            {
                hoverEnemyWeakness = value;
                OnPropertyChanged("HoverEnemyWeakness");
            }
        }

        private string hoverEnemyAttackStyle;
        public string HoverEnemyAttackStyle
        {
            get
            {
                return hoverEnemyAttackStyle;
            }
            set
            {
                hoverEnemyAttackStyle = value;
                OnPropertyChanged("HoverEnemyAttackStyle");
            }
        }

        public string EnemyImagePath
        {
            get
            {
                return Enemies[CurrentEnemy].ImagePath;
            }
            set { }
        }
        public string EnemyName
        {
            get
            {
                return Enemies[CurrentEnemy].Name;
            }
            set { }
        }
        public int EnemyDPS
        {
            get
            {
                return CurrentEnemyDPS();
            }
            set { }
        }
        public int EnemyMaxHP
        {
            get
            {
                return Enemies[CurrentEnemy].MaxHP;
            }
            set { }
        }
        private int enemyHP;
        public int EnemyHP
        {
            get
            {
                return enemyHP;
            }
            set
            {
                if (value < 0) { value = 0; }
                enemyHP = value;
                OnPropertyChanged("EnemyHP");
                OnPropertyChanged("EnemyHPPercentage");
                DamageLog.Info("Enemy HP changed. New value: " + value);
                CheckEnemyDeath();
            }
        }
        public int EnemyHPPercentage
        {
            get
            {
                double current = EnemyHP;
                double max = EnemyMaxHP;
                return Convert.ToInt32(Math.Round((double)((current / max) * 100), 0));
            }
            set { }
        }
        public string EnemyWeakness
        {
            get
            {
                return Enum.ToObject((typeof(Enemy.weaknessEnum)), Enemies[CurrentEnemy].Weakness).ToString();
            }
            set { }
        }
        public string EnemyAttackStyle
        {
            get
            {
                return Enum.ToObject((typeof(Enemy.attackStyleEnum)), Enemies[CurrentEnemy].AttackStyle).ToString();
            }
            set { }
        }

        private string enemyHitSplat;
        public string EnemyHitSplat
        {
            get
            {
                return enemyHitSplat;
            }
            set
            {
                enemyHitSplat = value;
                OnPropertyChanged("EnemyHitSplat");
            }
        }

        private int GetEnemyRelativeAttack(int id)
        {
            switch (Enemies[id].AttackStyle)
            {
                case Enemy.attackStyleEnum.Melee:
                    return Enemies[id].AttackLevel;
                case Enemy.attackStyleEnum.Magic:
                    return Enemies[id].MagicLevel;
                case Enemy.attackStyleEnum.Range:
                    return Enemies[id].RangeLevel;
                case Enemy.attackStyleEnum.None:
                    Log.Debug("Whilst retrieving GetEnemyRelativeAttack, we recived value of none. Returning a mix of all 3? Check this is intended.");
                    return (Enemies[id].AttackLevel + Enemies[id].MagicLevel + Enemies[id].RangeLevel) / 3;
                default:
                    Log.Error("Defaulted in CurrentEnemyRelativeAttack. Val: " + Enemies[id].AttackStyle);
                    break;
            }

            return 0;
        }

        private int GetEnemyDPS(int id)
        {
            if (id == 0) { return 0; }

            int dps;

            double calc = (Enemies[id].BaseDPS * GetEnemyRelativeAttack(id) * 7.5) / ((DefenceLevel * 7) + (PlayerTotalArmour / 5));

            //Factor in stance weaknesses etc.
            switch (Enemies[id].AttackStyle)
            {
                case Enemy.attackStyleEnum.Melee:
                    if (!StanceAttack && !StanceStrength)
                    {
                        if (StanceRange)
                        {
                            calc *= 1.25;
                        }
                        else if (StanceMagic)
                        {
                            calc *= 0.75;
                        }
                    }
                    break;
                case Enemy.attackStyleEnum.Magic:
                    if (!StanceMagic)
                    {
                        if (StanceRange)
                        {
                            calc *= 0.75;
                        }
                        else if (StanceAttack && StanceStrength)
                        {
                            calc *= 1.25;
                        }
                    }
                    break;
                case Enemy.attackStyleEnum.Range:
                    if (!StanceRange)
                    {
                        if (StanceMagic)
                        {
                            calc *= 1.25;
                        }
                        else if (StanceAttack && StanceStrength)
                        {
                            calc *= 0.75;
                        }
                    }
                    break;
                case Enemy.attackStyleEnum.None:
                    Log.Debug("Whilst calculating GetEnemyDPS, we recived value of none. Check this is intended.");
                    break;
                default:
                    Log.Error("Defaulted in CurrentEnemyDPS. Val: " + Enemies[id].AttackStyle);
                    break;
            }

            if (StanceDefence)
            {
                calc = calc / 3 * 2;
            }

            dps = (int)Math.Round(Math.Max(calc, 1));

            return dps;
        }

        #endregion

        #region Damage Related Values



        #endregion

        #region Exposed Player Stuff
        public int PlayerMaxHP
        {
            get
            {
                return ConstitutionLevel * 10;
            }
            set { }
        }
        private int playerHP;
        public int PlayerHP
        {
            get
            {
                return playerHP;
            }
            set
            {
                if (value < 0) { value = 0; }
                playerHP = value;
                OnPropertyChanged("PlayerHP");
                OnPropertyChanged("PlayerHPPercentage");
                DamageLog.Info("Player HP Changed. New value: " + value);
                CheckPlayerDeath();
            }
        }
        public int PlayerHPPercentage
        {
            get
            {
                double current = PlayerHP;
                double max = PlayerMaxHP;
                return Convert.ToInt32(Math.Round((double)((current / max) * 100), 0));
            }
            set { }
        }

        private string playerHitSplat;
        public string PlayerHitSplat
        {
            get
            {
                return playerHitSplat;
            }
            set
            {
                playerHitSplat = value;
                OnPropertyChanged("PlayerHitSplat");
            }
        }

        public long PlayerGP
        {
            get
            {
                return player.GP;
            }
            set
            {
                player.GP = value;
                OnPropertyChanged("PlayerGP");
                Log.Info("Player GP changed. New value: " + value);
            }
        }

        public int CombatLevel
        {
            get
            {
                int main = AttackLevel + StrengthLevel;
                if (main < MagicLevel * 2) { main = MagicLevel * 2; }
                if (main < RangeLevel * 2) { main = RangeLevel * 2; }
                double baseNumber = (main * 1.3) + DefenceLevel + ConstitutionLevel;
                double combat = 0.25 * baseNumber;
                int CombatLevel = Convert.ToInt32(Math.Floor(combat));
                return CombatLevel;
            }
            set { }
        }

        public int PlayerTotalDamage
        {
            get
            {
                int x = 0;

                #region Adding values
                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAuraSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAuraSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Aura. Value: " + AuraSlotType);
                            break;
                    }
                }
                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHeadSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHeadSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Head. Value: " + HeadSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentCapeSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentCapeSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Cape. Value: " + CapeSlotType);
                            break;
                    }
                }
                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentNeckSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentNeckSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Neck. Value: " + NeckSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentWeaponSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentWeaponSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Weapon. Value: " + WeaponSlotType);
                            break;
                    }
                }
                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentBodySlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentBodySlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Body. Value: " + BodySlotType);
                            break;
                    }
                }
                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentShieldSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentShieldSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Shield. Value: " + ShieldSlotType);
                            break;
                    }
                }
                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentLegsSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentLegsSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Legs. Value: " + LegsSlotType);
                            break;
                    }
                }
                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHandsSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHandsSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Hands. Value: " + HandsSlotType);
                            break;
                    }
                }
                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentFeetSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentFeetSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Feet. Value: " + FeetSlotType);
                            break;
                    }
                }
                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentRingSlot].Damage;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentRingSlot].Damage;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ring. Value: " + RingSlotType);
                            break;
                    }
                }
                #endregion

                return x;
            }
            set { }
        }
        public int PlayerTotalAccuracy
        {
            get
            {
                int x = 0;

                #region Adding values
                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAuraSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAuraSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Aura. Value: " + AuraSlotType);
                            break;
                    }
                }
                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHeadSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHeadSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Head. Value: " + HeadSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentCapeSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentCapeSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Cape. Value: " + CapeSlotType);
                            break;
                    }
                }
                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentNeckSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentNeckSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Neck. Value: " + NeckSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentWeaponSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentWeaponSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Weapon. Value: " + WeaponSlotType);
                            break;
                    }
                }
                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentBodySlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentBodySlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Body. Value: " + BodySlotType);
                            break;
                    }
                }
                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentShieldSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentShieldSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Shield. Value: " + ShieldSlotType);
                            break;
                    }
                }
                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentLegsSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentLegsSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Legs. Value: " + LegsSlotType);
                            break;
                    }
                }
                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHandsSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHandsSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Hands. Value: " + HandsSlotType);
                            break;
                    }
                }
                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentFeetSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentFeetSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Feet. Value: " + FeetSlotType);
                            break;
                    }
                }
                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentRingSlot].Accuracy;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentRingSlot].Accuracy;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ring. Value: " + RingSlotType);
                            break;
                    }
                }
                #endregion

                return x;
            }
            set { }
        }
        public int PlayerTotalArmour
        {
            get
            {
                int x = 0;

                #region Adding values
                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAuraSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAuraSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Aura. Value: " + AuraSlotType);
                            break;
                    }
                }
                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHeadSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHeadSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Head. Value: " + HeadSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentCapeSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentCapeSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Cape. Value: " + CapeSlotType);
                            break;
                    }
                }
                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentNeckSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentNeckSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Neck. Value: " + NeckSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentWeaponSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentWeaponSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Weapon. Value: " + WeaponSlotType);
                            break;
                    }
                }
                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentBodySlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentBodySlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Body. Value: " + BodySlotType);
                            break;
                    }
                }
                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentShieldSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentShieldSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Shield. Value: " + ShieldSlotType);
                            break;
                    }
                }
                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentLegsSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentLegsSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Legs. Value: " + LegsSlotType);
                            break;
                    }
                }
                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHandsSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHandsSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Hands. Value: " + HandsSlotType);
                            break;
                    }
                }
                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentFeetSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentFeetSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Feet. Value: " + FeetSlotType);
                            break;
                    }
                }
                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentRingSlot].Armour;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentRingSlot].Armour;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ring. Value: " + RingSlotType);
                            break;
                    }
                }
                #endregion

                return x;
            }
            set { }
        }
        public int PlayerTotalLifePoints
        {
            get
            {
                int x = 0;

                #region Adding values
                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAuraSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAuraSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Aura. Value: " + AuraSlotType);
                            break;
                    }
                }
                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHeadSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHeadSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Head. Value: " + HeadSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentCapeSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentCapeSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Cape. Value: " + CapeSlotType);
                            break;
                    }
                }
                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentNeckSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentNeckSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Neck. Value: " + NeckSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentWeaponSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentWeaponSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Weapon. Value: " + WeaponSlotType);
                            break;
                    }
                }
                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentBodySlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentBodySlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Body. Value: " + BodySlotType);
                            break;
                    }
                }
                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentShieldSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentShieldSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Shield. Value: " + ShieldSlotType);
                            break;
                    }
                }
                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentLegsSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentLegsSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Legs. Value: " + LegsSlotType);
                            break;
                    }
                }
                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHandsSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHandsSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Hands. Value: " + HandsSlotType);
                            break;
                    }
                }
                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentFeetSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentFeetSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Feet. Value: " + FeetSlotType);
                            break;
                    }
                }
                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentRingSlot].LifePoints;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentRingSlot].LifePoints;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ring. Value: " + RingSlotType);
                            break;
                    }
                }
                #endregion

                return x;
            }
            set { }
        }

        public int PlayerTotalPrayer
        {
            get
            {
                int x = 0;

                #region Adding values
                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAuraSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAuraSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Aura. Value: " + AuraSlotType);
                            break;
                    }
                }
                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHeadSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHeadSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Head. Value: " + HeadSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentCapeSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentCapeSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Cape. Value: " + CapeSlotType);
                            break;
                    }
                }
                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentNeckSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentNeckSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Neck. Value: " + NeckSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentWeaponSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentWeaponSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Weapon. Value: " + WeaponSlotType);
                            break;
                    }
                }
                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentBodySlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentBodySlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Body. Value: " + BodySlotType);
                            break;
                    }
                }
                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentShieldSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentShieldSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Shield. Value: " + ShieldSlotType);
                            break;
                    }
                }
                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentLegsSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentLegsSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Legs. Value: " + LegsSlotType);
                            break;
                    }
                }
                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHandsSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHandsSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Hands. Value: " + HandsSlotType);
                            break;
                    }
                }
                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentFeetSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentFeetSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Feet. Value: " + FeetSlotType);
                            break;
                    }
                }
                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentRingSlot].Prayer;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentRingSlot].Prayer;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ring. Value: " + RingSlotType);
                            break;
                    }
                }
                #endregion

                return x;
            }
            set { }
        }
        public int PlayerTotalStrBonus
        {
            get
            {
                int x = 0;

                #region Adding values
                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAuraSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAuraSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Aura. Value: " + AuraSlotType);
                            break;
                    }
                }
                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHeadSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHeadSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Head. Value: " + HeadSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentCapeSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentCapeSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Cape. Value: " + CapeSlotType);
                            break;
                    }
                }
                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentNeckSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentNeckSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Neck. Value: " + NeckSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentWeaponSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentWeaponSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Weapon. Value: " + WeaponSlotType);
                            break;
                    }
                }
                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentBodySlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentBodySlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Body. Value: " + BodySlotType);
                            break;
                    }
                }
                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentShieldSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentShieldSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Shield. Value: " + ShieldSlotType);
                            break;
                    }
                }
                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentLegsSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentLegsSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Legs. Value: " + LegsSlotType);
                            break;
                    }
                }
                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHandsSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHandsSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Hands. Value: " + HandsSlotType);
                            break;
                    }
                }
                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentFeetSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentFeetSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Feet. Value: " + FeetSlotType);
                            break;
                    }
                }
                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentRingSlot].StrBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentRingSlot].StrBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ring. Value: " + RingSlotType);
                            break;
                    }
                }
                #endregion

                return x;
            }
            set { }
        }
        public int PlayerTotalRangeBonus
        {
            get
            {
                int x = 0;

                #region Adding values
                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAuraSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAuraSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Aura. Value: " + AuraSlotType);
                            break;
                    }
                }
                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHeadSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHeadSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Head. Value: " + HeadSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentCapeSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentCapeSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Cape. Value: " + CapeSlotType);
                            break;
                    }
                }
                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentNeckSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentNeckSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Neck. Value: " + NeckSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentWeaponSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentWeaponSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Weapon. Value: " + WeaponSlotType);
                            break;
                    }
                }
                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentBodySlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentBodySlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Body. Value: " + BodySlotType);
                            break;
                    }
                }
                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentShieldSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentShieldSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Shield. Value: " + ShieldSlotType);
                            break;
                    }
                }
                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentLegsSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentLegsSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Legs. Value: " + LegsSlotType);
                            break;
                    }
                }
                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHandsSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHandsSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Hands. Value: " + HandsSlotType);
                            break;
                    }
                }
                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentFeetSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentFeetSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Feet. Value: " + FeetSlotType);
                            break;
                    }
                }
                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentRingSlot].RangeBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentRingSlot].RangeBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ring. Value: " + RingSlotType);
                            break;
                    }
                }
                #endregion

                return x;
            }
            set { }
        }
        public int PlayerTotalMagicBonus
        {
            get
            {
                int x = 0;

                #region Adding values
                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAuraSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAuraSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Aura. Value: " + AuraSlotType);
                            break;
                    }
                }
                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHeadSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHeadSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Head. Value: " + HeadSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentCapeSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentCapeSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Cape. Value: " + CapeSlotType);
                            break;
                    }
                }
                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentNeckSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentNeckSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Neck. Value: " + NeckSlotType);
                            break;
                    }
                }
                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentAmmoSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentAmmoSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ammo. Value: " + AmmoSlotType);
                            break;
                    }
                }
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentWeaponSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentWeaponSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Weapon. Value: " + WeaponSlotType);
                            break;
                    }
                }
                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentBodySlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentBodySlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Body. Value: " + BodySlotType);
                            break;
                    }
                }
                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentShieldSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentShieldSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Shield. Value: " + ShieldSlotType);
                            break;
                    }
                }
                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentLegsSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentLegsSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Legs. Value: " + LegsSlotType);
                            break;
                    }
                }
                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentHandsSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentHandsSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Hands. Value: " + HandsSlotType);
                            break;
                    }
                }
                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentFeetSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentFeetSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Feet. Value: " + FeetSlotType);
                            break;
                    }
                }
                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            x += Armours[CurrentRingSlot].MagicBonus;
                            break;
                        case Player.SlotType.Weapon:
                            x += Weapons[CurrentRingSlot].MagicBonus;
                            break;
                        default:
                            Log.Error("PlayerTotalDamage - Unsupported SlotType for Ring. Value: " + RingSlotType);
                            break;
                    }
                }
                #endregion

                return x;
            }
            set { }
        }

        public int PlayerRelativeStrengthLevel()
        {
            int str = 0;
            int styles = 0;

            if (StanceAttack || StanceStrength)
            {
                str += StrengthLevel;
                styles++;
            }
            if (StanceRange)
            {
                str += RangeLevel;
                styles++;
            }
            if (StanceMagic)
            {
                str += MagicLevel;
                styles++;
            }
            str /= styles;

            return str;
        }
        public int PlayerRelativeAttackLevel()
        {
            int att = 0;
            int styles = 0;

            if (StanceAttack || StanceStrength)
            {
                att += AttackLevel;
                styles++;
            }
            if (StanceRange)
            {
                att += RangeLevel;
                styles++;
            }
            if (StanceMagic)
            {
                att += MagicLevel;
                styles++;
            }
            att /= styles;

            return att;
        }

        public int PlayerMinHit
        {
            get
            {
                int x = PlayerTotalDamage;

                if (CurrentWeaponSlot >= 0)
                {
                    if (WeaponSlotType == Player.SlotType.Weapon)
                    {
                        x += Weapons[CurrentWeaponSlot].Damage;
                    }
                }

                if (CurrentShieldSlot >= 0)
                {
                    if (ShieldSlotType == Player.SlotType.Weapon)
                    {
                        x += Weapons[CurrentShieldSlot].Damage;
                    }
                }

                int str = PlayerRelativeStrengthLevel();

                x += ((str + PlayerTotalStrBonus) * 100);

                x = Convert.ToInt32(Math.Round(x / 8d, MidpointRounding.AwayFromZero));

                return x;
            }
            set { }
        }
        public int PlayerMaxHit
        {
            get
            {
                double y = PlayerMinHit * 1.5;

                int x = Convert.ToInt32(Math.Round(y, MidpointRounding.AwayFromZero));
                x += 3;

                return x;
            }
            set { }
        }
        public double PlayerCritHitChance
        {
            get
            {
                double x = 0;

                x += Math.Max(PlayerRelativeAttackLevel() / 10d, 1d);

                int accuracy = 0;

                accuracy = PlayerTotalAccuracy;
                if (CurrentWeaponSlot >= 0)
                {
                    if (WeaponSlotType == Player.SlotType.Weapon)
                    {
                        accuracy += Weapons[CurrentWeaponSlot].Accuracy;
                    }
                }

                if (CurrentShieldSlot >= 0)
                {
                    if (ShieldSlotType == Player.SlotType.Weapon)
                    {
                        accuracy += Weapons[CurrentShieldSlot].Accuracy;
                    }
                }

                x += Math.Max(accuracy / 250d, 1d);

                return x;
            }
            set { }
        }
        public int PlayerCritHit
        {
            get
            {
                return PlayerMaxHit * 2;
            }
            set { }
        }

        public int PlayerMinHitAdjusted
        {
            get
            {
                int hit = PlayerMinHit;

                if (StanceDefence)
                {
                    hit = hit / 3 * 2;
                }

                switch (Enemies[CurrentEnemy].Weakness)
                {
                    case Enemy.weaknessEnum.None:
                        //no change to damage.
                        break;
                    case Enemy.weaknessEnum.Melee:
                        if ((StanceAttack || StanceStrength) && !StanceMagic)
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if (StanceMagic && !(StanceAttack || StanceStrength))
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    case Enemy.weaknessEnum.Range:
                        if (StanceRange && !(StanceAttack || StanceStrength))
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if ((StanceAttack || StanceStrength) && !StanceRange)
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    case Enemy.weaknessEnum.Magic:
                        if (StanceMagic && !StanceRange)
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if (StanceRange && !StanceMagic)
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    default:
                        Log.Error("Switch failed on enemy weakness in PlayerMinHitAdjusted. Value: " + Enemies[CurrentEnemy].Weakness);
                        break;
                }

                return hit;
            }
            set { }
        }
        public int PlayerMaxHitAdjusted
        {
            get
            {
                int hit = PlayerMaxHit;

                if (StanceDefence)
                {
                    hit = hit / 3 * 2;
                }

                switch (Enemies[CurrentEnemy].Weakness)
                {
                    case Enemy.weaknessEnum.None:
                        //no change to damage.
                        break;
                    case Enemy.weaknessEnum.Melee:
                        if ((StanceAttack || StanceStrength) && !StanceMagic)
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if (StanceMagic && !(StanceAttack || StanceStrength))
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    case Enemy.weaknessEnum.Range:
                        if (StanceRange && !(StanceAttack || StanceStrength))
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if ((StanceAttack || StanceStrength) && !StanceRange)
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    case Enemy.weaknessEnum.Magic:
                        if (StanceMagic && !StanceRange)
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if (StanceRange && !StanceMagic)
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    default:
                        Log.Error("Switch failed on enemy weakness in PlayerMinHitAdjusted. Value: " + Enemies[CurrentEnemy].Weakness);
                        break;
                }

                return hit;
            }
            set { }
        }
        public int PlayerCritHitAdjusted
        {
            get
            {
                int hit = PlayerCritHit;

                if (StanceDefence)
                {
                    hit = hit / 3 * 2;
                }

                switch (Enemies[CurrentEnemy].Weakness)
                {
                    case Enemy.weaknessEnum.None:
                        //no change to damage.
                        break;
                    case Enemy.weaknessEnum.Melee:
                        if ((StanceAttack || StanceStrength) && !StanceMagic)
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if (StanceMagic && !(StanceAttack || StanceStrength))
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    case Enemy.weaknessEnum.Range:
                        if (StanceRange && !(StanceAttack || StanceStrength))
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if ((StanceAttack || StanceStrength) && !StanceRange)
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    case Enemy.weaknessEnum.Magic:
                        if (StanceMagic && !StanceRange)
                        {
                            hit = (int)(hit * 1.25);
                        }
                        if (StanceRange && !StanceMagic)
                        {
                            hit = (int)(hit * 0.75);
                        }
                        break;
                    default:
                        Log.Error("Switch failed on enemy weakness in PlayerCritHitAdjusted. Value: " + Enemies[CurrentEnemy].Weakness);
                        break;
                }

                return hit;
            }
            set { }
        }

        #endregion

        #region Exposed Armoury Stuff
        private ObservableCollection<Equipment> weapons;
        public ObservableCollection<Equipment> Weapons
        {
            get
            {
                return weapons;
            }
            set
            {
                weapons = value;
                OnPropertyChanged("Weapons");
            }
        }
        public ICollectionView GroupedWeapons { get; set; }
        private void AddWeapons()
        {
            Weapons = Equipment.GetAllWeapons();
            GroupedWeapons = new ListCollectionView(Weapons);
            GroupedWeapons.GroupDescriptions.Add(new PropertyGroupDescription("EquipmentCategory"));
            GroupedWeapons.Filter = new Predicate<object>(SearchArmoury);
            GroupedWeapons.SortDescriptions.Add(new SortDescription("RequiredLevel", ListSortDirection.Ascending));
            GroupedWeapons.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            Log.Info("Weapons Added and filter set.");
        }

        private ObservableCollection<Equipment> armours;
        public ObservableCollection<Equipment> Armours
        {
            get
            {
                return armours;
            }
            set
            {
                armours = value;
                OnPropertyChanged("Armours");
            }
        }
        public ICollectionView GroupedArmours { get; set; }
        private void AddArmours()
        {
            Armours = Equipment.GetAllArmour();
            GroupedArmours = new ListCollectionView(Armours);
            GroupedArmours.GroupDescriptions.Add(new PropertyGroupDescription("EquipmentCategory"));
            GroupedArmours.Filter = new Predicate<object>(SearchArmoury);
            GroupedArmours.SortDescriptions.Add(new SortDescription("RequiredLevel", ListSortDirection.Ascending));
            GroupedArmours.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            Log.Info("Armours Added and filter set.");
        }

        #region Armour Search Conditions
        private string textSearch = "";
        public string TextSearch
        {
            get
            {
                return textSearch;
            }
            set
            {
                textSearch = value;
                OnPropertyChanged("TextSearch");
            }
        }

        public bool SearchArmoury(object item)
        {
            Equipment e = item as Equipment;
            if ((e == null))
            {
                Log.Error("NullReferenceException: SearchArmoury Equipment is null!");
                return false;
            }

            if (e.Name.IndexOf(TextSearch, 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                if (e.RequiredLevel >= ArmouryMinLevel && e.RequiredLevel <= ArmouryMaxLevel)
                {
                    if (!(SearchAura || SearchHead || SearchPocket || SearchCape || SearchNeck || SearchAmmo || SearchWeapon || SearchBody || SearchShield || SearchLegs || SearchHands || SearchFeet || SearchRing))
                    {
                        Log.Debug(e.Name + " passes through the item search filter. (No Slots).");
                        return true;
                    }
                    else
                    {
                        switch (e.EquipmentSlot)
                        {
                            case Equipment.Slot.Aura:
                                return SearchAura;
                            case Equipment.Slot.Head:
                                return SearchHead;
                            case Equipment.Slot.Pocket:
                                return SearchPocket;
                            case Equipment.Slot.Cape:
                                return SearchCape;
                            case Equipment.Slot.Neck:
                                return SearchNeck;
                            case Equipment.Slot.Ammo:
                                return SearchAmmo;
                            case Equipment.Slot.Weapon:
                                return SearchWeapon;
                            case Equipment.Slot.Body:
                                return SearchBody;
                            case Equipment.Slot.Shield:
                                return SearchShield;
                            case Equipment.Slot.Twohand:
                                return SearchWeapon || SearchShield;
                            case Equipment.Slot.Legs:
                                return SearchLegs;
                            case Equipment.Slot.Hands:
                                return SearchHands;
                            case Equipment.Slot.Feet:
                                return SearchFeet;
                            case Equipment.Slot.Ring:
                                return SearchRing;
                            default:
                                Log.Error("SearchArmoury Function. The switch statement on Equipment slot fell through. Slot used: " + e.EquipmentSlot);
                                break;
                        }
                    }
                }
            }
            Log.Debug(e.Name + " fails to pass through the search filter.");
            return false;
        }

        private int armouryMinLevel = 1;
        public int ArmouryMinLevel
        {
            get
            {
                return armouryMinLevel;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                if (value > ArmouryMaxLevel)
                {
                    ArmouryMaxLevel = value;
                }
                armouryMinLevel = value;
                OnPropertyChanged("ArmouryMinLevel");
            }
        }

        private int armouryMaxLevel = 100;
        public int ArmouryMaxLevel
        {
            get
            {
                return armouryMaxLevel;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                if (value < ArmouryMinLevel)
                {
                    ArmouryMinLevel = value;
                }
                armouryMaxLevel = value;
                OnPropertyChanged("ArmouryMaxLevel");
            }
        }
        #endregion

        #region Equipment Slot Search Vars

        private bool searchAura = false;
        private bool SearchAura
        {
            get
            {
                return searchAura;
            }
            set
            {
                searchAura = value;
                if (value)
                {
                    SearchAuraColour = "#FFFF3535";
                }
                else
                {
                    SearchAuraColour = "#00000000";
                }
            }
        }
        private string searchAuraColour = "#00000000";
        public string SearchAuraColour
        {
            get
            {
                return searchAuraColour;
            }
            set
            {
                searchAuraColour = value;
                OnPropertyChanged("SearchAuraColour");
            }
        }

        private bool searchHead = false;
        private bool SearchHead
        {
            get
            {
                return searchHead;
            }
            set
            {
                searchHead = value;
                if (value)
                {
                    SearchHeadColour = "#FFFF3535";
                }
                else
                {
                    SearchHeadColour = "#00000000";
                }
            }
        }
        private string searchHeadColour = "#00000000";
        public string SearchHeadColour
        {
            get
            {
                return searchHeadColour;
            }
            set
            {
                searchHeadColour = value;
                OnPropertyChanged("SearchHeadColour");
            }
        }

        private bool searchPocket = false;
        private bool SearchPocket
        {
            get
            {
                return searchPocket;
            }
            set
            {
                searchPocket = value;
                if (value)
                {
                    SearchPocketColour = "#FFFF3535";
                }
                else
                {
                    SearchPocketColour = "#00000000";
                }
            }
        }
        private string searchPocketColour = "#00000000";
        public string SearchPocketColour
        {
            get
            {
                return searchPocketColour;
            }
            set
            {
                searchPocketColour = value;
                OnPropertyChanged("SearchPocketColour");
            }
        }

        private bool searchCape = false;
        private bool SearchCape
        {
            get
            {
                return searchCape;
            }
            set
            {
                searchCape = value;
                if (value)
                {
                    SearchCapeColour = "#FFFF3535";
                }
                else
                {
                    SearchCapeColour = "#00000000";
                }
            }
        }
        private string searchCapeColour = "#00000000";
        public string SearchCapeColour
        {
            get
            {
                return searchCapeColour;
            }
            set
            {
                searchCapeColour = value;
                OnPropertyChanged("SearchCapeColour");
            }
        }

        private bool searchNeck = false;
        private bool SearchNeck
        {
            get
            {
                return searchNeck;
            }
            set
            {
                searchNeck = value;
                if (value)
                {
                    SearchNeckColour = "#FFFF3535";
                }
                else
                {
                    SearchNeckColour = "#00000000";
                }
            }
        }
        private string searchNeckColour = "#00000000";
        public string SearchNeckColour
        {
            get
            {
                return searchNeckColour;
            }
            set
            {
                searchNeckColour = value;
                OnPropertyChanged("SearchNeckColour");
            }
        }

        private bool searchAmmo = false;
        private bool SearchAmmo
        {
            get
            {
                return searchAmmo;
            }
            set
            {
                searchAmmo = value;
                if (value)
                {
                    SearchAmmoColour = "#FFFF3535";
                }
                else
                {
                    SearchAmmoColour = "#00000000";
                }
            }
        }
        private string searchAmmoColour = "#00000000";
        public string SearchAmmoColour
        {
            get
            {
                return searchAmmoColour;
            }
            set
            {
                searchAmmoColour = value;
                OnPropertyChanged("SearchAmmoColour");
            }
        }

        private bool searchWeapon = false;
        private bool SearchWeapon
        {
            get
            {
                return searchWeapon;
            }
            set
            {
                searchWeapon = value;
                if (value)
                {
                    SearchWeaponColour = "#FFFF3535";
                }
                else
                {
                    SearchWeaponColour = "#00000000";
                }
            }
        }
        private string searchWeaponColour = "#00000000";
        public string SearchWeaponColour
        {
            get
            {
                return searchWeaponColour;
            }
            set
            {
                searchWeaponColour = value;
                OnPropertyChanged("SearchWeaponColour");
            }
        }

        private bool searchBody = false;
        private bool SearchBody
        {
            get
            {
                return searchBody;
            }
            set
            {
                searchBody = value;
                if (value)
                {
                    SearchBodyColour = "#FFFF3535";
                }
                else
                {
                    SearchBodyColour = "#00000000";
                }
            }
        }
        private string searchBodyColour = "#00000000";
        public string SearchBodyColour
        {
            get
            {
                return searchBodyColour;
            }
            set
            {
                searchBodyColour = value;
                OnPropertyChanged("SearchBodyColour");
            }
        }

        private bool searchShield = false;
        private bool SearchShield
        {
            get
            {
                return searchShield;
            }
            set
            {
                searchShield = value;
                if (value)
                {
                    SearchShieldColour = "#FFFF3535";
                }
                else
                {
                    SearchShieldColour = "#00000000";
                }
            }
        }
        private string searchShieldColour = "#00000000";
        public string SearchShieldColour
        {
            get
            {
                return searchShieldColour;
            }
            set
            {
                searchShieldColour = value;
                OnPropertyChanged("SearchShieldColour");
            }
        }

        private bool searchLegs = false;
        private bool SearchLegs
        {
            get
            {
                return searchLegs;
            }
            set
            {
                searchLegs = value;
                if (value)
                {
                    SearchLegsColour = "#FFFF3535";
                }
                else
                {
                    SearchLegsColour = "#00000000";
                }
            }
        }
        private string searchLegsColour = "#00000000";
        public string SearchLegsColour
        {
            get
            {
                return searchLegsColour;
            }
            set
            {
                searchLegsColour = value;
                OnPropertyChanged("SearchLegsColour");
            }
        }

        private bool searchHands = false;
        private bool SearchHands
        {
            get
            {
                return searchHands;
            }
            set
            {
                searchHands = value;
                if (value)
                {
                    SearchHandsColour = "#FFFF3535";
                }
                else
                {
                    SearchHandsColour = "#00000000";
                }
            }
        }
        private string searchHandsColour = "#00000000";
        public string SearchHandsColour
        {
            get
            {
                return searchHandsColour;
            }
            set
            {
                searchHandsColour = value;
                OnPropertyChanged("SearchHandsColour");
            }
        }

        private bool searchFeet = false;
        private bool SearchFeet
        {
            get
            {
                return searchFeet;
            }
            set
            {
                searchFeet = value;
                if (value)
                {
                    SearchFeetColour = "#FFFF3535";
                }
                else
                {
                    SearchFeetColour = "#00000000";
                }
            }
        }
        private string searchFeetColour = "#00000000";
        public string SearchFeetColour
        {
            get
            {
                return searchFeetColour;
            }
            set
            {
                searchFeetColour = value;
                OnPropertyChanged("SearchFeetColour");
            }
        }

        private bool searchRing = false;
        private bool SearchRing
        {
            get
            {
                return searchRing;
            }
            set
            {
                searchRing = value;
                if (value)
                {
                    SearchRingColour = "#FFFF3535";
                }
                else
                {
                    SearchRingColour = "#00000000";
                }
            }
        }
        private string searchRingColour = "#00000000";
        public string SearchRingColour
        {
            get
            {
                return searchRingColour;
            }
            set
            {
                searchRingColour = value;
                OnPropertyChanged("SearchRingColour");
            }
        }

        #endregion

        #region Current Equipment Slots

        public int CurrentWeaponSlot
        {
            get
            {
                return player.CurrentWeaponSlot;
            }
            set
            {
                player.CurrentWeaponSlot = value;
                OnPropertyChanged("CurrentWeaponSlot");
                Log.Info("CurrentWeaponSlot changed. New value: " + value);
            }
        }
        public int CurrentHeadSlot
        {
            get
            {
                return player.CurrentHeadSlot;
            }
            set
            {
                player.CurrentHeadSlot = value;
                OnPropertyChanged("CurrentHeadSlot");
                Log.Info("CurrentHeadSlot changed. New value: " + value);
            }
        }
        public int CurrentAuraSlot
        {
            get
            {
                return player.CurrentAuraSlot;
            }
            set
            {
                player.CurrentAuraSlot = value;
                OnPropertyChanged("CurrentAuraSlot");
                Log.Info("CurrentAuraSlot changed. New value: " + value);
            }
        }

        public int CurrentPocketSlot
        {
            get
            {
                return player.CurrentPocketSlot;
            }
            set
            {
                player.CurrentPocketSlot = value;
                OnPropertyChanged("CurrentPocketSlot");
                Log.Info("CurrentPocketSlot changed. New value: " + value);
            }
        }
        public int CurrentCapeSlot
        {
            get
            {
                return player.CurrentCapeSlot;
            }
            set
            {
                player.CurrentCapeSlot = value;
                OnPropertyChanged("CurrentCapeSlot");
                Log.Info("CurrentCapeSlot changed. New value: " + value);
            }
        }
        public int CurrentNeckSlot
        {
            get
            {
                return player.CurrentNeckSlot;
            }
            set
            {
                player.CurrentNeckSlot = value;
                OnPropertyChanged("CurrentNeckSlot");
                Log.Info("CurrentNeckSlot changed. New value: " + value);
            }
        }

        public int CurrentAmmoSlot
        {
            get
            {
                return player.CurrentAmmoSlot;
            }
            set
            {
                player.CurrentAmmoSlot = value;
                OnPropertyChanged("CurrentAmmoSlot");
                Log.Info("CurrentAmmoSlot changed. New value: " + value);
            }
        }
        public int CurrentBodySlot
        {
            get
            {
                return player.CurrentBodySlot;
            }
            set
            {
                player.CurrentBodySlot = value;
                OnPropertyChanged("CurrentBodySlot");
                Log.Info("CurrentBodySlot changed. New value: " + value);
            }
        }
        public int CurrentShieldSlot
        {
            get
            {
                return player.CurrentShieldSlot;
            }
            set
            {
                player.CurrentShieldSlot = value;
                OnPropertyChanged("CurrentShieldSlot");
                Log.Info("CurrentShieldSlot changed. New value: " + value);
            }
        }

        public int CurrentLegsSlot
        {
            get
            {
                return player.CurrentLegsSlot;
            }
            set
            {
                player.CurrentLegsSlot = value;
                OnPropertyChanged("CurrentLegsSlot");
                Log.Info("CurrentLegsSlot changed. New value: " + value);
            }
        }

        public int CurrentHandsSlot
        {
            get
            {
                return player.CurrentHandsSlot;
            }
            set
            {
                player.CurrentHandsSlot = value;
                OnPropertyChanged("CurrentHandsSlot");
                Log.Info("CurrentHandsSlot changed. New value: " + value);
            }
        }
        public int CurrentFeetSlot
        {
            get
            {
                return player.CurrentFeetSlot;
            }
            set
            {
                player.CurrentFeetSlot = value;
                OnPropertyChanged("CurrentFeetSlot");
                Log.Info("CurrentFeetSlot changed. New value: " + value);
            }
        }
        public int CurrentRingSlot
        {
            get
            {
                return player.CurrentRingSlot;
            }
            set
            {
                player.CurrentRingSlot = value;
                OnPropertyChanged("CurrentRingSlot");
                Log.Info("CurrentRingSlot changed. New value: " + value);
            }
        }

        public Player.SlotType AuraSlotType
        {
            get
            {
                return player.AuraSlotType;
            }
            set
            {
                player.AuraSlotType = value;
                OnPropertyChanged("AuraSlotType");
            }
        }
        public Player.SlotType HeadSlotType
        {
            get
            {
                return player.HeadSlotType;
            }
            set
            {
                player.HeadSlotType = value;
                OnPropertyChanged("HeadSlotType");
            }
        }
        public Player.SlotType PocketSlotType
        {
            get
            {
                return player.PocketSlotType;
            }
            set
            {
                player.PocketSlotType = value;
                OnPropertyChanged("PocketSlotType");
            }
        }

        public Player.SlotType CapeSlotType
        {
            get
            {
                return player.CapeSlotType;
            }
            set
            {
                player.CapeSlotType = value;
                OnPropertyChanged("CapeSlotType");
            }
        }
        public Player.SlotType NeckSlotType
        {
            get
            {
                return player.NeckSlotType;
            }
            set
            {
                player.NeckSlotType = value;
                OnPropertyChanged("NeckSlotType");
            }
        }
        public Player.SlotType AmmoSlotType
        {
            get
            {
                return player.AmmoSlotType;
            }
            set
            {
                player.AmmoSlotType = value;
                OnPropertyChanged("AmmoSlotType");
            }
        }

        public Player.SlotType WeaponSlotType
        {
            get
            {
                return player.WeaponSlotType;
            }
            set
            {
                player.WeaponSlotType = value;
                OnPropertyChanged("WeaponSlotType");
            }
        }
        public Player.SlotType BodySlotType
        {
            get
            {
                return player.BodySlotType;
            }
            set
            {
                player.BodySlotType = value;
                OnPropertyChanged("BodySlotType");
            }
        }
        public Player.SlotType ShieldSlotType
        {
            get
            {
                return player.ShieldSlotType;
            }
            set
            {
                player.ShieldSlotType = value;
                OnPropertyChanged("ShieldSlotType");
            }
        }

        public Player.SlotType LegsSlotType
        {
            get
            {
                return player.LegsSlotType;
            }
            set
            {
                player.LegsSlotType = value;
                OnPropertyChanged("LegsSlotType");
            }
        }

        public Player.SlotType HandsSlotType
        {
            get
            {
                return player.HandsSlotType;
            }
            set
            {
                player.HandsSlotType = value;
                OnPropertyChanged("HandsSlotType");
            }
        }
        public Player.SlotType FeetSlotType
        {
            get
            {
                return player.FeetSlotType;
            }
            set
            {
                player.FeetSlotType = value;
                OnPropertyChanged("FeetSlotType");
            }
        }
        public Player.SlotType RingSlotType
        {
            get
            {
                return player.RingSlotType;
            }
            set
            {
                player.RingSlotType = value;
                OnPropertyChanged("RingSlotType");
            }
        }

        #endregion

        #region Equipment Images

        public string CurrentAuraImage
        {
            get
            {

                if (CurrentAuraSlot >= 0)
                {
                    switch (AuraSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentAuraSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentAuraSlot].ImagePath;
                        default:
                            Log.Error("CurrentAuraImage - Missing SlotType in switch statement. Type: " + AuraSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentHeadImage
        {
            get
            {

                if (CurrentHeadSlot >= 0)
                {
                    switch (HeadSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentHeadSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentHeadSlot].ImagePath;
                        default:
                            Log.Error("CurrentHeadImage - Missing SlotType in switch statement. Type: " + HeadSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentPocketImage
        {
            get
            {

                if (CurrentPocketSlot >= 0)
                {
                    switch (PocketSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentPocketSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentPocketSlot].ImagePath;
                        default:
                            Log.Error("CurrentPocketImage - Missing SlotType in switch statement. Type: " + PocketSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentCapeImage
        {
            get
            {

                if (CurrentCapeSlot >= 0)
                {
                    switch (CapeSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentCapeSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentCapeSlot].ImagePath;
                        default:
                            Log.Error("CurrentCapeImage - Missing SlotType in switch statement. Type: " + CapeSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentNeckImage
        {
            get
            {

                if (CurrentNeckSlot >= 0)
                {
                    switch (NeckSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentNeckSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentNeckSlot].ImagePath;
                        default:
                            Log.Error("CurrentNeckImage - Missing SlotType in switch statement. Type: " + NeckSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentAmmoImage
        {
            get
            {

                if (CurrentAmmoSlot >= 0)
                {
                    switch (AmmoSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentAmmoSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentAmmoSlot].ImagePath;
                        default:
                            Log.Error("CurrentAmmoImage - Missing SlotType in switch statement. Type: " + AmmoSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentWeaponImage
        {
            get
            {
                if (CurrentWeaponSlot >= 0)
                {
                    switch (WeaponSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentWeaponSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentWeaponSlot].ImagePath;
                        default:
                            Log.Error("CurrentWeaponImage - Missing SlotType in switch statement. Type: " + WeaponSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentBodyImage
        {
            get
            {

                if (CurrentBodySlot >= 0)
                {
                    switch (BodySlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentBodySlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentBodySlot].ImagePath;
                        default:
                            Log.Error("CurrentBodyImage - Missing SlotType in switch statement. Type: " + BodySlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentShieldImage
        {
            get
            {

                if (CurrentShieldSlot >= 0)
                {
                    switch (ShieldSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentShieldSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentShieldSlot].ImagePath;
                        default:
                            Log.Error("CurrentShieldImage - Missing SlotType in switch statement. Type: " + ShieldSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentLegsImage
        {
            get
            {

                if (CurrentLegsSlot >= 0)
                {
                    switch (LegsSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentLegsSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentLegsSlot].ImagePath;
                        default:
                            Log.Error("CurrentLegsImage - Missing SlotType in switch statement. Type: " + LegsSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentHandsImage
        {
            get
            {

                if (CurrentHandsSlot >= 0)
                {
                    switch (HandsSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentHandsSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentHandsSlot].ImagePath;
                        default:
                            Log.Error("CurrentHandsImage - Missing SlotType in switch statement. Type: " + HandsSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentFeetImage
        {
            get
            {

                if (CurrentFeetSlot >= 0)
                {
                    switch (FeetSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentFeetSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentFeetSlot].ImagePath;
                        default:
                            Log.Error("CurrentFeetImage - Missing SlotType in switch statement. Type: " + FeetSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        public string CurrentRingImage
        {
            get
            {

                if (CurrentRingSlot >= 0)
                {
                    switch (RingSlotType)
                    {
                        case Player.SlotType.Armour:
                            return Armours[CurrentRingSlot].ImagePath;
                        case Player.SlotType.Weapon:
                            return Weapons[CurrentRingSlot].ImagePath;
                        default:
                            Log.Error("CurrentRingImage - Missing SlotType in switch statement. Type: " + RingSlotType);
                            return "";
                    }
                }
                return "";
            }
            set { }
        }

        #endregion

        #region Equipment Popup Variables

        private bool armouryTooltipOpen;
        public bool ArmouryTooltipOpen
        {
            get
            {
                return armouryTooltipOpen;
            }
            set
            {
                armouryTooltipOpen = value;
                OnPropertyChanged("ArmouryTooltipOpen");
            }
        }

        private double armouryOffsetX;
        public double ArmouryOffsetX
        {
            get
            {
                return armouryOffsetX;
            }
            set
            {
                armouryOffsetX = value;
                OnPropertyChanged("ArmouryOffsetX");
            }
        }

        private double armouryOffsetY;
        public double ArmouryOffsetY
        {
            get
            {
                return armouryOffsetY;
            }
            set
            {
                armouryOffsetY = value;
                OnPropertyChanged("ArmouryOffsetY");
            }
        }

        private Equipment GetHoveredEquipment()
        {
            switch (CurrentHoveredArmouryItem)
            {
                case "Aura":
                    if (CurrentAuraSlot >= 0)
                    {
                        switch (AuraSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentAuraSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentAuraSlot];
                            default:
                                Log.Error("Aura Slot Type unrecognisable in GetHoveredEquipment. Value used: " + AuraSlotType);
                                break;
                        }
                    }
                    return null;
                case "Head":
                    if (CurrentHeadSlot >= 0)
                    {
                        switch (HeadSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentHeadSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentHeadSlot];
                            default:
                                Log.Error("Head Slot Type unrecognisable in GetHoveredEquipment. Value used: " + HeadSlotType);
                                break;
                        }
                    }
                    return null;
                case "Pocket":
                    if (CurrentPocketSlot >= 0)
                    {
                        switch (PocketSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentPocketSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentPocketSlot];
                            default:
                                Log.Error("Pocket Slot Type unrecognisable in GetHoveredEquipment. Value used: " + PocketSlotType);
                                break;
                        }
                    }
                    return null;
                case "Cape":
                    if (CurrentCapeSlot >= 0)
                    {
                        switch (CapeSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentCapeSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentCapeSlot];
                            default:
                                Log.Error("Cape Slot Type unrecognisable in GetHoveredEquipment. Value used: " + CapeSlotType);
                                break;
                        }
                    }
                    return null;
                case "Neck":
                    if (CurrentNeckSlot >= 0)
                    {
                        switch (NeckSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentNeckSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentNeckSlot];
                            default:
                                Log.Error("Neck Slot Type unrecognisable in GetHoveredEquipment. Value used: " + NeckSlotType);
                                break;
                        }
                    }
                    return null;
                case "Ammo":
                    if (CurrentAmmoSlot >= 0)
                    {
                        switch (AmmoSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentAmmoSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentAmmoSlot];
                            default:
                                Log.Error("Ammo Slot Type unrecognisable in GetHoveredEquipment. Value used: " + AmmoSlotType);
                                break;
                        }
                    }
                    return null;
                case "Weapon":
                    if (CurrentWeaponSlot >= 0)
                    {
                        switch (WeaponSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentWeaponSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentWeaponSlot];
                            default:
                                Log.Error("Weapon Slot Type unrecognisable in GetHoveredEquipment. Value used: " + WeaponSlotType);
                                break;
                        }
                    }
                    return null;
                case "Body":
                    if (CurrentBodySlot >= 0)
                    {
                        switch (BodySlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentBodySlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentBodySlot];
                            default:
                                Log.Error("Body Slot Type unrecognisable in GetHoveredEquipment. Value used: " + BodySlotType);
                                break;
                        }
                    }
                    return null;
                case "Shield":
                    if (CurrentShieldSlot >= 0)
                    {
                        switch (ShieldSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentShieldSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentShieldSlot];
                            default:
                                Log.Error("Shield Slot Type unrecognisable in GetHoveredEquipment. Value used: " + ShieldSlotType);
                                break;
                        }
                    }
                    return null;
                case "Legs":
                    if (CurrentLegsSlot >= 0)
                    {
                        switch (LegsSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentLegsSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentLegsSlot];
                            default:
                                Log.Error("Legs Slot Type unrecognisable in GetHoveredEquipment. Value used: " + LegsSlotType);
                                break;
                        }
                    }
                    return null;
                case "Hands":
                    if (CurrentHandsSlot >= 0)
                    {
                        switch (HandsSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentHandsSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentHandsSlot];
                            default:
                                Log.Error("Hands Slot Type unrecognisable in GetHoveredEquipment. Value used: " + HandsSlotType);
                                break;
                        }
                    }
                    return null;
                case "Feet":
                    if (CurrentFeetSlot >= 0)
                    {
                        switch (FeetSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentFeetSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentFeetSlot];
                            default:
                                Log.Error("Feet Slot Type unrecognisable in GetHoveredEquipment. Value used: " + FeetSlotType);
                                break;
                        }
                    }
                    return null;
                case "Ring":
                    if (CurrentRingSlot >= 0)
                    {
                        switch (RingSlotType)
                        {
                            case Player.SlotType.Armour:
                                return Armours[CurrentRingSlot];
                            case Player.SlotType.Weapon:
                                return Weapons[CurrentRingSlot];
                            default:
                                Log.Error("Ring Slot Type unrecognisable in GetHoveredEquipment. Value used: " + RingSlotType);
                                break;
                        }
                    }
                    return null;
                default:
                    Log.Warn("CurrentHoveredArmouryItem unrecognisable in GetHoveredEquipment. Value used: " + CurrentHoveredArmouryItem + ". This can be ignored upon startup.");
                    return null;
            }
        }

        private string CurrentHoveredArmouryItem = "Aura";
        public string CurrentArmouryItemName
        {
            get
            {
                Equipment eq = GetHoveredEquipment();
                if (eq != null)
                {
                    return eq.Name;
                }
                else
                {
                    return "No item Equipped.";
                }
            }
            set { }
        }
        public string CurrentArmouryItemExamine
        {
            get
            {
                Equipment eq = GetHoveredEquipment();
                if (eq != null)
                {
                    return eq.Examine;
                }
                else
                {
                    return "";
                }
            }
            set { }
        }
        public string CurrentArmouryItemStat1
        {
            get
            {
                Equipment eq = GetHoveredEquipment();
                if (eq != null)
                {
                    if (eq.Damage > eq.Armour)
                    {
                        return "Damage: ";
                    }
                    else
                    {
                        return "Armour: ";
                    }
                }
                else
                {
                    return "";
                }
            }
            set { }
        }
        public string CurrentArmouryItemStat2
        {
            get
            {
                Equipment eq = GetHoveredEquipment();
                if (eq != null)
                {
                    if (eq.Damage > eq.Armour)
                    {
                        return "Accuracy: ";
                    }
                    else
                    {
                        return "LifePoints: ";
                    }
                }
                else
                {
                    return "";
                }
            }
            set { }
        }
        public string CurrentArmouryItemStat1Value
        {
            get
            {
                Equipment eq = GetHoveredEquipment();
                if (eq != null)
                {
                    if (eq.Damage > eq.Armour)
                    {
                        return eq.Damage.ToString();
                    }
                    else
                    {
                        return eq.Armour.ToString();
                    }
                }
                else
                {
                    return "";
                }
            }
            set { }
        }
        public string CurrentArmouryItemStat2Value
        {
            get
            {
                Equipment eq = GetHoveredEquipment();
                if (eq != null)
                {
                    if (eq.Damage > eq.Armour)
                    {
                        return eq.Accuracy.ToString();
                    }
                    else
                    {
                        return eq.LifePoints.ToString();
                    }
                }
                else
                {
                    return "";
                }
            }
            set { }
        }

        #endregion

        #endregion

        #region Exposed Tutorial Tip Stuff

        private string tutorialTitle;
        public string TutorialTitle
        {
            get
            {
                return tutorialTitle;
            }
            set
            {
                tutorialTitle = value;
                OnPropertyChanged("TutorialTitle");
            }
        }

        private string tutorialDescription;
        public string TutorialDescription
        {
            get
            {
                return tutorialDescription;
            }
            set
            {
                tutorialDescription = value;
                OnPropertyChanged("TutorialDescription");
            }
        }

        private string tutorialPosition;
        public string TutorialPosition
        {
            get
            {
                return tutorialPosition;
            }
            set
            {
                tutorialPosition = value;
                OnPropertyChanged("TutorialPosition");
            }
        }

        private bool tutorialBoolEnemy = false;
        public bool TutorialBoolEnemy
        {
            get
            {
                return tutorialBoolEnemy;
            }
            set
            {
                tutorialBoolEnemy = value;
                OnPropertyChanged("TutorialBoolEnemy");
            }
        }
        private bool tutorialBoolStatusBar = false;
        public bool TutorialBoolStatusBar
        {
            get
            {
                return tutorialBoolStatusBar;
            }
            set
            {
                tutorialBoolStatusBar = value;
                OnPropertyChanged("TutorialBoolStatusBar");
            }
        }
        private bool tutorialBoolLeftPanel = false;
        public bool TutorialBoolLeftPanel
        {
            get
            {
                return tutorialBoolLeftPanel;
            }
            set
            {
                tutorialBoolLeftPanel = value;
                OnPropertyChanged("TutorialBoolLeftPanel");
            }
        }
        private bool tutorialBoolRightPanel = false;
        public bool TutorialBoolRightPanel
        {
            get
            {
                return tutorialBoolRightPanel;
            }
            set
            {
                tutorialBoolRightPanel = value;
                OnPropertyChanged("TutorialBoolRightPanel");
            }
        }
        private bool tutorialBoolEnemySelect = false;
        public bool TutorialBoolEnemySelect
        {
            get
            {
                return tutorialBoolEnemySelect;
            }
            set
            {
                tutorialBoolEnemySelect = value;
                OnPropertyChanged("TutorialBoolEnemySelect");
            }
        }
        private bool tutorialBoolPlayerHP = false;
        public bool TutorialBoolPlayerHP
        {
            get
            {
                return tutorialBoolPlayerHP;
            }
            set
            {
                tutorialBoolPlayerHP = value;
                OnPropertyChanged("TutorialBoolPlayerHP");
            }
        }
        private bool tutorialBoolGP = false;
        public bool TutorialBoolGP
        {
            get
            {
                return tutorialBoolGP;
            }
            set
            {
                tutorialBoolGP = value;
                OnPropertyChanged("TutorialBoolGP");
            }
        }
        private bool tutorialBoolCombatLevel = false;
        public bool TutorialBoolCombatLevel
        {
            get
            {
                return tutorialBoolCombatLevel;
            }
            set
            {
                tutorialBoolCombatLevel = value;
                OnPropertyChanged("TutorialBoolCombatLevel");
            }
        }
        private bool tutorialBoolAttackLevel = false;
        public bool TutorialBoolAttackLevel
        {
            get
            {
                return tutorialBoolAttackLevel;
            }
            set
            {
                tutorialBoolAttackLevel = value;
                OnPropertyChanged("TutorialBoolAttackLevel");
            }
        }
        private bool tutorialBoolStrengthLevel = false;
        public bool TutorialBoolStrengthLevel
        {
            get
            {
                return tutorialBoolStrengthLevel;
            }
            set
            {
                tutorialBoolStrengthLevel = value;
                OnPropertyChanged("TutorialBoolStrengthLevel");
            }
        }
        private bool tutorialBoolDefenceLevel = false;
        public bool TutorialBoolDefenceLevel
        {
            get
            {
                return tutorialBoolDefenceLevel;
            }
            set
            {
                tutorialBoolDefenceLevel = value;
                OnPropertyChanged("TutorialBoolDefenceLevel");
            }
        }
        private bool tutorialBoolConstitutionLevel = false;
        public bool TutorialBoolConstitutionLevel
        {
            get
            {
                return tutorialBoolConstitutionLevel;
            }
            set
            {
                tutorialBoolConstitutionLevel = value;
                OnPropertyChanged("TutorialBoolConstitutionLevel");
            }
        }
        private bool tutorialBoolRangeLevel = false;
        public bool TutorialBoolRangeLevel
        {
            get
            {
                return tutorialBoolRangeLevel;
            }
            set
            {
                tutorialBoolRangeLevel = value;
                OnPropertyChanged("TutorialBoolRangeLevel");
            }
        }
        private bool tutorialBoolMagicLevel = false;
        public bool TutorialBoolMagicLevel
        {
            get
            {
                return tutorialBoolMagicLevel;
            }
            set
            {
                tutorialBoolMagicLevel = value;
                OnPropertyChanged("TutorialBoolMagicLevel");
            }
        }
        private bool tutorialBoolCombatStyle = false;
        public bool TutorialBoolCombatStyle
        {
            get
            {
                return tutorialBoolCombatStyle;
            }
            set
            {
                tutorialBoolCombatStyle = value;
                OnPropertyChanged("TutorialBoolCombatStyle");
            }
        }
        private bool tutorialBoolArmouryLeftStats = false;
        public bool TutorialBoolArmouryLeftStats
        {
            get
            {
                return tutorialBoolArmouryLeftStats;
            }
            set
            {
                tutorialBoolArmouryLeftStats = value;
                OnPropertyChanged("TutorialBoolArmouryLeftStats");
            }
        }
        private bool tutorialBoolArmouryRightStats = false;
        public bool TutorialBoolArmouryRightStats
        {
            get
            {
                return tutorialBoolArmouryRightStats;
            }
            set
            {
                tutorialBoolArmouryRightStats = value;
                OnPropertyChanged("TutorialBoolArmouryRightStats");
            }
        }
        private bool tutorialBoolArmourySlots = false;
        public bool TutorialBoolArmourySlots
        {
            get
            {
                return tutorialBoolArmourySlots;
            }
            set
            {
                tutorialBoolArmourySlots = value;
                OnPropertyChanged("TutorialBoolArmourySlots");
            }
        }
        private bool tutorialBoolArmourySearch = false;
        public bool TutorialBoolArmourySearch
        {
            get
            {
                return tutorialBoolArmourySearch;
            }
            set
            {
                tutorialBoolArmourySearch = value;
                OnPropertyChanged("TutorialBoolArmourySearch");
            }
        }
        private bool tutorialBoolArmouryItems = false;
        public bool TutorialBoolArmouryItems
        {
            get
            {
                return tutorialBoolArmouryItems;
            }
            set
            {
                tutorialBoolArmouryItems = value;
                OnPropertyChanged("TutorialBoolArmouryItems");
            }
        }
        private bool tutorialBoolSummoningCharms = false;
        public bool TutorialBoolSummoningCharms
        {
            get
            {
                return tutorialBoolSummoningCharms;
            }
            set
            {
                tutorialBoolSummoningCharms = value;
                OnPropertyChanged("TutorialBoolSummoningCharms");
            }
        }
        private bool tutorialBoolSummoningLevels = false;
        public bool TutorialBoolSummoningLevels
        {
            get
            {
                return tutorialBoolSummoningLevels;
            }
            set
            {
                tutorialBoolSummoningLevels = value;
                OnPropertyChanged("TutorialBoolSummoningLevels");
            }
        }
        private bool tutorialBoolSummoningPouches = false;
        public bool TutorialBoolSummoningPouches
        {
            get
            {
                return tutorialBoolSummoningPouches;
            }
            set
            {
                tutorialBoolSummoningPouches = value;
                OnPropertyChanged("TutorialBoolSummoningPouches");
            }
        }

        #endregion

        #region Summoning

        private ObservableCollection<Familiar> familiars;
        public ObservableCollection<Familiar> Familiars
        {
            get
            {
                return familiars;
            }
            set
            {
                familiars = value;
                OnPropertyChanged("Familiars");
            }
        }
        private void AddFamiliars()
        {
            Familiars = Familiar.GetAllFamiliars();
            Log.Info("Familiars Loaded");
        }

        public int GoldCharmCost
        {
            get
            {
                if (SummoningLevel < 4)
                {
                    return 175;
                }
                if (SummoningLevel < 10)
                {
                    return 200;
                }
                if (SummoningLevel < 13)
                {
                    return 200;
                }
                if (SummoningLevel < 16)
                {
                    return 225;
                }
                if (SummoningLevel < 17)
                {
                    return 175;
                }
                if (SummoningLevel < 40)
                {
                    return 25;
                }
                if (SummoningLevel < 52)
                {
                    return 275;
                }
                if (SummoningLevel < 66)
                {
                    return 300;
                }
                if (SummoningLevel < 67)
                {
                    return 275;
                }
                if (SummoningLevel < 71)
                {
                    return 25;
                }
                if (SummoningLevel < 100)
                {
                    return 350;
                }
                return 350;
            }
            set { }
        }
        public double GoldCharmXP
        {
            get
            {
                if (SummoningLevel < 4)
                {
                    return 4.85;
                }
                if (SummoningLevel < 10)
                {
                    return 9.3;
                }
                if (SummoningLevel < 13)
                {
                    return 12.6;
                }
                if (SummoningLevel < 16)
                {
                    return 12.6;
                }
                if (SummoningLevel < 17)
                {
                    return 21.6;
                }
                if (SummoningLevel < 40)
                {
                    return 46.5;
                }
                if (SummoningLevel < 52)
                {
                    return 52.8;
                }
                if (SummoningLevel < 66)
                {
                    return 68.4;
                }
                if (SummoningLevel < 67)
                {
                    return 87;
                }
                if (SummoningLevel < 71)
                {
                    return 58.6;
                }
                if (SummoningLevel < 100)
                {
                    return 93.2;
                }
                return 93.2;
            }
            set { }
        }

        public int GreenCharmCost
        {
            get
            {
                if (SummoningLevel < 18)
                {
                    return 0;
                }
                if (SummoningLevel < 28)
                {
                    return 1125;
                }
                if (SummoningLevel < 33)
                {
                    return 1175;
                }
                if (SummoningLevel < 34)
                {
                    return 1800;
                }
                if (SummoningLevel < 41)
                {
                    return 1850;
                }
                if (SummoningLevel < 43)
                {
                    return 1950;
                }
                if (SummoningLevel < 47)
                {
                    return 2200;
                }
                if (SummoningLevel < 54)
                {
                    return 2200;
                }
                if (SummoningLevel < 56)
                {
                    return 2650;
                }
                if (SummoningLevel < 62)
                {
                    return 2725;
                }
                if (SummoningLevel < 68)
                {
                    return 2975;
                }
                if (SummoningLevel < 69)
                {
                    return 2750;
                }
                if (SummoningLevel < 76)
                {
                    return 3250;
                }
                if (SummoningLevel < 78)
                {
                    return 3525;
                }
                if (SummoningLevel < 80)
                {
                    return 3100;
                }
                if (SummoningLevel < 88)
                {
                    return 3200;
                }
                if (SummoningLevel < 93)
                {
                    return 3500;
                }
                if (SummoningLevel < 100)
                {
                    return 2825;
                }
                return 2825;
            }
            set { }
        }
        public double GreenCharmXP
        {
            get
            {
                if (SummoningLevel < 18)
                {
                    return 0;
                }
                if (SummoningLevel < 28)
                {
                    return 31.2;
                }
                if (SummoningLevel < 33)
                {
                    return 49.8;
                }
                if (SummoningLevel < 34)
                {
                    return 57.6;
                }
                if (SummoningLevel < 41)
                {
                    return 59.6;
                }
                if (SummoningLevel < 43)
                {
                    return 72.4;
                }
                if (SummoningLevel < 47)
                {
                    return 75.2;
                }
                if (SummoningLevel < 54)
                {
                    return 83.2;
                }
                if (SummoningLevel < 56)
                {
                    return 94.8;
                }
                if (SummoningLevel < 62)
                {
                    return 98.8;
                }
                if (SummoningLevel < 68)
                {
                    return 109.6;
                }
                if (SummoningLevel < 69)
                {
                    return 119.2;
                }
                if (SummoningLevel < 76)
                {
                    return 121.2;
                }
                if (SummoningLevel < 78)
                {
                    return 134;
                }
                if (SummoningLevel < 80)
                {
                    return 136.8;
                }
                if (SummoningLevel < 88)
                {
                    return 140.8;
                }
                if (SummoningLevel < 93)
                {
                    return 154.4;
                }
                if (SummoningLevel < 100)
                {
                    return 163.2;
                }
                return 163.2;
            }
            set { }
        }

        public int CrimsonCharmCost
        {
            get
            {
                if (SummoningLevel < 19)
                {
                    return 0;
                }
                if (SummoningLevel < 22)
                {
                    return 1425;
                }
                if (SummoningLevel < 31)
                {
                    return 1600;
                }
                if (SummoningLevel < 32)
                {
                    return 2025;
                }
                if (SummoningLevel < 42)
                {
                    return 2100;
                }
                if (SummoningLevel < 46)
                {
                    return 2600;
                }
                if (SummoningLevel < 49)
                {
                    return 2775;
                }
                if (SummoningLevel < 61)
                {
                    return 2925;
                }
                if (SummoningLevel < 63)
                {
                    return 3525;
                }
                if (SummoningLevel < 64)
                {
                    return 2900;
                }
                if (SummoningLevel < 70)
                {
                    return 3200;
                }
                if (SummoningLevel < 72)
                {
                    return 1975;
                }
                if (SummoningLevel < 74)
                {
                    return 4125;
                }
                if (SummoningLevel < 75)
                {
                    return 4150;
                }
                if (SummoningLevel < 77)
                {
                    return 4200;
                }
                if (SummoningLevel < 83)
                {
                    return 4350;
                }
                if (SummoningLevel < 84)
                {
                    return 25;
                }
                if (SummoningLevel < 85)
                {
                    return 4325;
                }
                if (SummoningLevel < 92)
                {
                    return 3750;
                }
                if (SummoningLevel < 95)
                {
                    return 5075;
                }
                if (SummoningLevel < 96)
                {
                    return 4950;
                }
                if (SummoningLevel < 99)
                {
                    return 5275;
                }
                if (SummoningLevel < 100)
                {
                    return 4450;
                }
                return 4450;
            }
            set { }
        }
        public double CrimsonCharmXP
        {
            get
            {
                if (SummoningLevel < 19)
                {
                    return 0;
                }
                if (SummoningLevel < 22)
                {
                    return 83.2;
                }
                if (SummoningLevel < 31)
                {
                    return 96.8;
                }
                if (SummoningLevel < 32)
                {
                    return 136;
                }
                if (SummoningLevel < 42)
                {
                    return 140.8;
                }
                if (SummoningLevel < 46)
                {
                    return 184.8;
                }
                if (SummoningLevel < 49)
                {
                    return 202.4;
                }
                if (SummoningLevel < 61)
                {
                    return 215.2;
                }
                if (SummoningLevel < 63)
                {
                    return 268;
                }
                if (SummoningLevel < 64)
                {
                    return 276.8;
                }
                if (SummoningLevel < 70)
                {
                    return 281.6;
                }
                if (SummoningLevel < 72)
                {
                    return 132;
                }
                if (SummoningLevel < 74)
                {
                    return 301.8;
                }
                if (SummoningLevel < 75)
                {
                    return 325.6;
                }
                if (SummoningLevel < 77)
                {
                    return 329.6;
                }
                if (SummoningLevel < 83)
                {
                    return 1015.2;
                }
                if (SummoningLevel < 84)
                {
                    return 364.8;
                }
                if (SummoningLevel < 85)
                {
                    return 371.5;
                }
                if (SummoningLevel < 92)
                {
                    return 373.6;
                }
                if (SummoningLevel < 95)
                {
                    return 404.8;
                }
                if (SummoningLevel < 96)
                {
                    return 417.6;
                }
                if (SummoningLevel < 99)
                {
                    return 422.4;
                }
                if (SummoningLevel < 100)
                {
                    return 435.2;
                }
                return 435.2;
            }
            set { }
        }

        public int BlueCharmCost
        {
            get
            {
                if (SummoningLevel < 23)
                {
                    return 0;
                }
                if (SummoningLevel < 25)
                {
                    return 1875;
                }
                if (SummoningLevel < 29)
                {
                    return 1275;
                }
                if (SummoningLevel < 34)
                {
                    return 2100;
                }
                if (SummoningLevel < 36)
                {
                    return 1850;
                }
                if (SummoningLevel < 46)
                {
                    return 2250;
                }
                if (SummoningLevel < 55)
                {
                    return 3125;
                }
                if (SummoningLevel < 56)
                {
                    return 3775;
                }
                if (SummoningLevel < 57)
                {
                    return 3525;
                }
                if (SummoningLevel < 58)
                {
                    return 3825;
                }
                if (SummoningLevel < 66)
                {
                    return 3600;
                }
                if (SummoningLevel < 73)
                {
                    return 3800;
                }
                if (SummoningLevel < 76)
                {
                    return 4875;
                }
                if (SummoningLevel < 79)
                {
                    return 3600;
                }
                if (SummoningLevel < 83)
                {
                    return 4875;
                }
                if (SummoningLevel < 86)
                {
                    return 5475;
                }
                if (SummoningLevel < 88)
                {
                    return 25;
                }
                if (SummoningLevel < 89)
                {
                    return 5100;
                }
                if (SummoningLevel < 100)
                {
                    return 5550;
                }
                return 5550;
            }
            set { }
        }
        public double BlueCharmXP
        {
            get
            {
                if (SummoningLevel < 23)
                {
                    return 0;
                }
                if (SummoningLevel < 25)
                {
                    return 202.8;
                }
                if (SummoningLevel < 29)
                {
                    return 220;
                }
                if (SummoningLevel < 34)
                {
                    return 255.2;
                }
                if (SummoningLevel < 36)
                {
                    return 59.6;
                }
                if (SummoningLevel < 46)
                {
                    return 316.8;
                }
                if (SummoningLevel < 55)
                {
                    return 404.8;
                }
                if (SummoningLevel < 56)
                {
                    return 484;
                }
                if (SummoningLevel < 57)
                {
                    return 492.8;
                }
                if (SummoningLevel < 58)
                {
                    return 501.6;
                }
                if (SummoningLevel < 66)
                {
                    return 510.4;
                }
                if (SummoningLevel < 73)
                {
                    return 580.8;
                }
                if (SummoningLevel < 76)
                {
                    return 642.4;
                }
                if (SummoningLevel < 79)
                {
                    return 668.8;
                }
                if (SummoningLevel < 83)
                {
                    return 695.2;
                }
                if (SummoningLevel < 86)
                {
                    return 730.4;
                }
                if (SummoningLevel < 88)
                {
                    return 756.8;
                }
                if (SummoningLevel < 89)
                {
                    return 771.6;
                }
                if (SummoningLevel < 100)
                {
                    return 783.2;
                }
                return 783.2;
            }
            set { }
        }

        public int SummoningBuyQuantity
        {
            get
            {
                return familiar.BuyQuantity;
            }
            set
            {
                familiar.BuyQuantity = value;
                foreach (Familiar f in familiars)
                {
                    f.BuyQuantity = value;
                }
                OnPropertyChanged("SummoningBuyQuantity");
            }
        }

        public long TotalDPS
        {
            get
            {
                long dps = 0;
                foreach (Familiar f in familiars)
                {
                    dps += f.TotalDPS;
                }
                return dps;
            }
            set { }
        }

        #endregion

        #region Wilderness

        public bool DisplayWildyOutside
        {
            get
            {
                return !(bool)ParseDB.Wilderness;
            }
            set { }
        }
        public bool DisplayWildyInside
        {
            get
            {
                return (bool)ParseDB.Wilderness;
            }
            set { }
        }

        public int WildernessEntryLevel
        {
            get
            {
                if (ParseDB.WildernessLevel < 1)
                {
                    return 1;
                }
                else
                {
                    return ParseDB.WildernessLevel;
                }
            }
            set
            {
                ParseDB.WildernessLevel = value;
            }
        }

        public int WildernessMoveMinimum
        {
            get
            {
                if(ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                if (Convert.ToInt32(ParseDB.ParseUser["WildernessLevel"]) >= 20)
                {
                    return 20;
                }
                else
                {
                    return 1;
                }
            }
        }

        public string WildernessRemainingTime
        {
            get
            {
                TimeSpan t = ParseDB.WildernessExit - DateTime.UtcNow;
                string time = string.Format("{0:mm\\:ss}", t);
                return time.ToString();
            }
            set { }
        }

        public bool WildernessLeaveButtonEnabled
        {
            get
            {
                if (ParseDB.WildernessExit > DateTime.UtcNow)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public string WildernessLeaveButtonText
        {
            get
            {
                if (ParseDB.WildernessExit > DateTime.UtcNow)
                {
                    return WildernessRemainingTime;
                }
                else
                {
                    return "Leave!";
                }
            }
            set { }
        }

        public int CurrentWildernessLevel
        {
            get
            {
                return ParseDB.WildernessLevel;
            }
            set { }
        }

        public bool WildernessShowReward
        {
            get
            {
                return ParseDB.ShowReward;
            }
            set { }
        }

        public bool WildernessShowEntry
        {
            get
            {
                if (DisplayWildyOutside)
                {
                    return !WildernessShowReward;
                }
                return false;
            }
            set { }
        }

        public double WildernessRewardTotal
        {
            get
            {
                if (ParseDB.WildernessExit > ParseDB.WildernessEntry)
                {
                    return Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]);
                }
                else
                {
                    return ParseDB.CalculateReward();
                }
            }
            set { }
        }

        public int WildernessAttackXPReward
        {
            get
            {
                if (ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                return Convert.ToInt32(Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]) * AttackLevel);
            }
            set { }
        }

        public int WildernessStrengthXPReward
        {
            get
            {
                if (ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                return Convert.ToInt32(Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]) * StrengthLevel);
            }
            set { }
        }

        public int WildernessDefenceXPReward
        {
            get
            {
                if (ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                return Convert.ToInt32(Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]) * DefenceLevel);
            }
            set { }
        }

        public int WildernessConstitutionXPReward
        {
            get
            {
                if (ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                return Convert.ToInt32(Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]) * ConstitutionLevel);
            }
            set { }
        }

        public int WildernessMagicXPReward
        {
            get
            {
                if (ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                return Convert.ToInt32(Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]) * MagicLevel);
            }
            set { }
        }

        public int WildernessRangeXPReward
        {
            get
            {
                if (ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                return Convert.ToInt32(Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]) * RangeLevel);
            }
            set { }
        }

        public int WildernessSummoningXPReward
        {
            get
            {
                if (ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                return Convert.ToInt32(Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]) * SummoningLevel);
            }
            set { }
        }

        public int WildernessGPReward
        {
            get
            {
                if (ParseDB.ParseUser == null) { return 1; } //Fix for Await not completing yet.
                return Convert.ToInt32(Convert.ToDouble(ParseDB.ParseUser["WildernessReward"]) * OverallLevel);
            }
            set { }
        }

        public ObservableCollection<EnemyPlayer> WildernessPlayerList
        {
            get
            {
                return ParseDB.EnemyPlayers;
            }
            set { }
        }
        private async void AddWildernessPlayers()
        {
            await ParseDB.GetPlayerList();
        }

        public bool IsWildernessPlayerListEmpty
        {
            get
            {
                if (WildernessPlayerList == null) { return true; } //Fix for Await not completing yet.
                if (WildernessPlayerList.Count == 0)
                {
                    return true;
                }
                return false;
            }
            set { }
        }

        private bool wildernessEnemyAttacked = false;
        public bool WildernessEnemyAttacked
        {
            get
            {
                return wildernessEnemyAttacked;
            }
            set
            {
                wildernessEnemyAttacked = value;
                OnPropertyChanged("WildernessEnemyAttacked");
                OnPropertyChanged("WildernessEnemyRested");
            }
        }
        public bool WildernessEnemyRested
        {
            get
            {
                return !wildernessEnemyAttacked;
            }
            set
            {
                wildernessEnemyAttacked = !value;
                OnPropertyChanged("WildernessEnemyAttacked");
                OnPropertyChanged("WildernessEnemyRested");
            }
        }

        private int WildernessEnemyID;

        private string wildernessDescText = "4!";
        public string WildernessDescText
        {
            get
            {
                return wildernessDescText;
            }
            set
            {
                wildernessDescText = value;
                OnPropertyChanged("WildernessDescText");
            }
        }

        private int wildernessPlayerMaxHealth = 1;
        public int WildernessPlayerMaxHealth
        {
            get
            {
                return wildernessPlayerMaxHealth;
            }
            set
            {
                wildernessPlayerMaxHealth = value;
                OnPropertyChanged("WildernessPlayerMaxHealth");
            }
        }
        private int wildernessPlayerHealth;
        public int WildernessPlayerHealth
        {
            get
            {
                return wildernessPlayerHealth;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                wildernessPlayerHealth = value;
                if (value == 0)
                {
                    WildernessPlayerDied();
                }
                OnPropertyChanged("WildernessPlayerHealth");
                OnPropertyChanged("WildernessPlayerHealthPercent");
            }
        }
        public int WildernessPlayerHealthPercent
        {
            get
            {
                double current = WildernessPlayerHealth;
                double max = WildernessPlayerMaxHealth;
                return Convert.ToInt32(Math.Round((double)((current / max) * 100), 0));
            }
            set { }
        }
        private int wildernessPlayerDamaged = 0;
        public int WildernessPlayerDamaged
        {
            get
            {
                return wildernessPlayerDamaged;
            }
            set
            {
                wildernessPlayerDamaged = value;
                OnPropertyChanged("WildernessPlayerDamaged");
            }
        }

        private int wildernessEnemyMaxHealth = 1;
        public int WildernessEnemyMaxHealth
        {
            get
            {
                return wildernessEnemyMaxHealth;
            }
            set
            {
                wildernessEnemyMaxHealth = value;
                OnPropertyChanged("WildernessEnemyMaxHealth");
            }
        }
        private int wildernessEnemyHealth;
        public int WildernessEnemyHealth
        {
            get
            {
                return wildernessEnemyHealth;
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                wildernessEnemyHealth = value;
                if (value == 0)
                {
                    WildernessEnemyDied();
                }
                OnPropertyChanged("WildernessEnemyHealth");
                OnPropertyChanged("WildernessEnemyHealthPercent");
            }
        }
        public int WildernessEnemyHealthPercent
        {
            get
            {
                double current = WildernessEnemyHealth;
                double max = WildernessEnemyMaxHealth;
                return Convert.ToInt32(Math.Round((double)((current / max) * 100), 0));
            }
            set { }
        }
        private int wildernessEnemyDamaged = 0;
        public int WildernessEnemyDamaged
        {
            get
            {
                return wildernessEnemyDamaged;
            }
            set
            {
                wildernessEnemyDamaged = value;
                OnPropertyChanged("WildernessEnemyDamaged");
            }
        }

        private bool WildernessPlayersTurn = true;

        private async void WildernessPlayerDied()
        {
            WildernessDescText = "Oh no! You died!";
            WildernessBattleTimer.Stop();
            WildernessBattleOverTimer.Start();

            ParseDB.PlayerDied();

            ParseDB.RewardReward();
            OnPropertyChanged("WildernessShowReward");

            ParseDB.Wilderness = false;
            ParseDB.WildernessExit = DateTime.UtcNow;
            ParseDB.WildernessLevel = 0;
            await ParseDB.UpdateDB();


            OnPropertyChanged("DisplayWildyOutside");
            OnPropertyChanged("DisplayWildyInside");
            OnPropertyChanged("WildernessEntryLevel");
            OnPropertyChanged("WildernessLeaveButtonEnabled");
            OnPropertyChanged("WildernessShowEntry");

            OnPropertyChanged("WildernessAttackXPReward");
            OnPropertyChanged("WildernessStrengthXPReward");
            OnPropertyChanged("WildernessConstitutionXPReward");
            OnPropertyChanged("WildernessMagicXPReward");
            OnPropertyChanged("WildernessRangeXPReward");
            OnPropertyChanged("WildernessDefenceXPReward");
            OnPropertyChanged("WildernessSummoningXPReward");
            OnPropertyChanged("WildernessGPReward");
        }

        private void WildernessEnemyDied()
        {
            WildernessDescText = "You killed your enemy!";
            WildernessBattleTimer.Stop();
            WildernessBattleOverTimer.Start();

            ParseDB.EnemyDied(WildernessPlayerList[WildernessEnemyID].ObjectID);


            OnPropertyChanged("DisplayWildyOutside");
            OnPropertyChanged("DisplayWildyInside");
            OnPropertyChanged("WildernessEntryLevel");
            OnPropertyChanged("WildernessLeaveButtonEnabled");
            OnPropertyChanged("WildernessShowEntry");

            OnPropertyChanged("WildernessAttackXPReward");
            OnPropertyChanged("WildernessStrengthXPReward");
            OnPropertyChanged("WildernessConstitutionXPReward");
            OnPropertyChanged("WildernessMagicXPReward");
            OnPropertyChanged("WildernessRangeXPReward");
            OnPropertyChanged("WildernessDefenceXPReward");
            OnPropertyChanged("WildernessSummoningXPReward");
            OnPropertyChanged("WildernessGPReward");
        }

        private bool WildernessEntryExtras = false;

        #endregion

        #region MISCELLANIOUS

        private int leftTabIndex = 0;
        public int LeftTabIndex
        {
            get
            {
                return leftTabIndex;
            }
            set
            {
                leftTabIndex = value;
                OnPropertyChanged("LeftTabIndex");
                if (value > 0)
                {
                    CurrentEnemy = 0;
                }
            }
        }

        #endregion

        #region Commands

        #region Tooltip Commands
        private ICommand attackPopupTooltip;
        public ICommand AttackPopupTooltip
        {
            get
            {
                if (attackPopupTooltip == null)
                {
                    attackPopupTooltip = new RelayCommand(param => this.AttackPopupTooltipEx(param), null);
                }
                return attackPopupTooltip;
            }
        }
        private void AttackPopupTooltipEx(object p)
        {
            if (!AttackTooltipOpen)
            {
                AttackTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)p);

            AttackOffsetX = currentpos.X;
            AttackOffsetY = currentpos.Y + 30;
        }

        private ICommand attackPopupTooltipLeave;
        public ICommand AttackPopupTooltipLeave
        {
            get
            {
                if (attackPopupTooltipLeave == null)
                {
                    attackPopupTooltipLeave = new RelayCommand(param => this.AttackPopupTooltipLeaveEx(param), null);
                }
                return attackPopupTooltipLeave;
            }
        }
        private void AttackPopupTooltipLeaveEx(object p)
        {
            AttackTooltipOpen = false;
        }

        private ICommand strengthPopupTooltip;
        public ICommand StrengthPopupTooltip
        {
            get
            {
                if (strengthPopupTooltip == null)
                {
                    strengthPopupTooltip = new RelayCommand(param => this.StrengthPopupTooltipEx(param), null);
                }
                return strengthPopupTooltip;
            }
        }
        private void StrengthPopupTooltipEx(object p)
        {
            if (!StrengthTooltipOpen)
            {
                StrengthTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)p);

            StrengthOffsetX = currentpos.X;
            StrengthOffsetY = currentpos.Y + 30;
        }

        private ICommand strengthPopupTooltipLeave;
        public ICommand StrengthPopupTooltipLeave
        {
            get
            {
                if (strengthPopupTooltipLeave == null)
                {
                    strengthPopupTooltipLeave = new RelayCommand(param => this.StrengthPopupTooltipLeaveEx(param), null);
                }
                return strengthPopupTooltipLeave;
            }
        }
        private void StrengthPopupTooltipLeaveEx(object p)
        {
            StrengthTooltipOpen = false;
        }

        private ICommand defencePopupTooltip;
        public ICommand DefencePopupTooltip
        {
            get
            {
                if (defencePopupTooltip == null)
                {
                    defencePopupTooltip = new RelayCommand(param => this.DefencePopupTooltipEx(param), null);
                }
                return defencePopupTooltip;
            }
        }
        private void DefencePopupTooltipEx(object p)
        {
            if (!DefenceTooltipOpen)
            {
                DefenceTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)p);

            DefenceOffsetX = currentpos.X;
            DefenceOffsetY = currentpos.Y + 30;
        }

        private ICommand defencePopupTooltipLeave;
        public ICommand DefencePopupTooltipLeave
        {
            get
            {
                if (defencePopupTooltipLeave == null)
                {
                    defencePopupTooltipLeave = new RelayCommand(param => this.DefencePopupTooltipLeaveEx(param), null);
                }
                return defencePopupTooltipLeave;
            }
        }
        private void DefencePopupTooltipLeaveEx(object p)
        {
            DefenceTooltipOpen = false;
        }

        private ICommand constitutionPopupTooltip;
        public ICommand ConstitutionPopupTooltip
        {
            get
            {
                if (constitutionPopupTooltip == null)
                {
                    constitutionPopupTooltip = new RelayCommand(param => this.ConstitutionPopupTooltipEx(param), null);
                }
                return constitutionPopupTooltip;
            }
        }
        private void ConstitutionPopupTooltipEx(object p)
        {
            if (!ConstitutionTooltipOpen)
            {
                ConstitutionTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)p);

            ConstitutionOffsetX = currentpos.X;
            ConstitutionOffsetY = currentpos.Y + 30;
        }

        private ICommand constitutionPopupTooltipLeave;
        public ICommand ConstitutionPopupTooltipLeave
        {
            get
            {
                if (constitutionPopupTooltipLeave == null)
                {
                    constitutionPopupTooltipLeave = new RelayCommand(param => this.ConstitutionPopupTooltipLeaveEx(param), null);
                }
                return constitutionPopupTooltipLeave;
            }
        }
        private void ConstitutionPopupTooltipLeaveEx(object p)
        {
            ConstitutionTooltipOpen = false;
        }

        private ICommand rangePopupTooltip;
        public ICommand RangePopupTooltip
        {
            get
            {
                if (rangePopupTooltip == null)
                {
                    rangePopupTooltip = new RelayCommand(param => this.RangePopupTooltipEx(param), null);
                }
                return rangePopupTooltip;
            }
        }
        private void RangePopupTooltipEx(object p)
        {
            if (!RangeTooltipOpen)
            {
                RangeTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)p);

            RangeOffsetX = currentpos.X;
            RangeOffsetY = currentpos.Y + 30;
        }

        private ICommand rangePopupTooltipLeave;
        public ICommand RangePopupTooltipLeave
        {
            get
            {
                if (rangePopupTooltipLeave == null)
                {
                    rangePopupTooltipLeave = new RelayCommand(param => this.RangePopupTooltipLeaveEx(param), null);
                }
                return rangePopupTooltipLeave;
            }
        }
        private void RangePopupTooltipLeaveEx(object p)
        {
            RangeTooltipOpen = false;
        }

        private ICommand magicPopupTooltip;
        public ICommand MagicPopupTooltip
        {
            get
            {
                if (magicPopupTooltip == null)
                {
                    magicPopupTooltip = new RelayCommand(param => this.MagicPopupTooltipEx(param), null);
                }
                return magicPopupTooltip;
            }
        }
        private void MagicPopupTooltipEx(object p)
        {
            if (!MagicTooltipOpen)
            {
                MagicTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)p);

            MagicOffsetX = currentpos.X;
            MagicOffsetY = currentpos.Y + 30;
        }

        private ICommand magicPopupTooltipLeave;
        public ICommand MagicPopupTooltipLeave
        {
            get
            {
                if (magicPopupTooltipLeave == null)
                {
                    magicPopupTooltipLeave = new RelayCommand(param => this.MagicPopupTooltipLeaveEx(param), null);
                }
                return magicPopupTooltipLeave;
            }
        }
        private void MagicPopupTooltipLeaveEx(object p)
        {
            MagicTooltipOpen = false;
        }

        private ICommand summoningPopupTooltip;
        public ICommand SummoningPopupTooltip
        {
            get
            {
                if (summoningPopupTooltip == null)
                {
                    summoningPopupTooltip = new RelayCommand(SummoningPopupTooltipEx, null);
                }
                return summoningPopupTooltip;
            }
        }
        private void SummoningPopupTooltipEx(object p)
        {
            if (!SummoningTooltipOpen)
            {
                SummoningTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)p);

            SummoningOffsetX = currentpos.X;
            SummoningOffsetY = currentpos.Y + 30;
        }

        private ICommand summoningPopupTooltipLeave;
        public ICommand SummoningPopupTooltipLeave
        {
            get
            {
                if (summoningPopupTooltipLeave == null)
                {
                    summoningPopupTooltipLeave = new RelayCommand(SummoningTooltipLeaveEx, null);
                }
                return summoningPopupTooltipLeave;
            }
        }
        private void SummoningTooltipLeaveEx(object p)
        {
            SummoningTooltipOpen = false;
        }

        private ICommand enemyPopupTooltip;
        public ICommand EnemyPopupTooltip
        {
            get
            {
                if (enemyPopupTooltip == null)
                {
                    enemyPopupTooltip = new RelayCommand(EnemyPopupTooltipEx, null);
                }
                return enemyPopupTooltip;
            }
        }
        private void EnemyPopupTooltipEx(object p)
        {
            var values = (object[])p;

            if (!EnemyTooltipOpen)
            {
                EnemyTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)view);

            EnemyOffsetX = currentpos.X - ((double)values[0] / 2);
            EnemyOffsetY = currentpos.Y + 22;

            //string str = values[1].ToString().Replace("\"", "");

            HoverEnemyName = Enemies[Convert.ToInt32(values[1])].Name;
            HoverEnemyHP = Enemies[Convert.ToInt32(values[1])].MaxHP;
            HoverEnemyDPS = GetEnemyDPS(Convert.ToInt32(values[1]));
            HoverEnemyWeakness = Enum.ToObject((typeof(Enemy.weaknessEnum)), Enemies[Convert.ToInt32(values[1])].Weakness).ToString();
            HoverEnemyAttackStyle = Enum.ToObject((typeof(Enemy.attackStyleEnum)), Enemies[Convert.ToInt32(values[1])].AttackStyle).ToString();
        }

        private ICommand enemyPopupTooltipLeave;
        public ICommand EnemyPopupTooltipLeave
        {
            get
            {
                if (enemyPopupTooltipLeave == null)
                {
                    enemyPopupTooltipLeave = new RelayCommand(param => this.EnemyPopupTooltipLeaveEx(), null);
                }
                return enemyPopupTooltipLeave;
            }
        }
        private void EnemyPopupTooltipLeaveEx()
        {
            EnemyTooltipOpen = false;
        }

        #endregion

        #region Enemy Commands
        private ICommand enemySelect;
        public ICommand EnemySelect
        {
            get
            {
                if (enemySelect == null)
                {
                    enemySelect = new RelayCommand(param => this.EnemySelectEx(Convert.ToInt32(param)), null);
                }
                return enemySelect;
            }
        }
        private void EnemySelectEx(int id)
        {
            if (GetEnemyDPS(id) >= PlayerMaxHP)
            {
                Toast t = new Toast("I wouldn't try that...", "That enemy will kill you right away! Consider changing your combat style, levelling up or gearing up before attempting to kill this monster.", NotificationType.Warning);
                return;
            }
            if (GetEnemyDPS(id) >= PlayerMaxHP / 2)
            {
                MessageBoxResult check = System.Windows.MessageBox.Show("This enemy will kill you really fast! Are you sure you wish to continue to its lair?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                if (check == MessageBoxResult.No)
                {
                    return;
                }
            }
            if (LeftTabIndex != 0)
            {
                LeftTabIndex = 0;
            }
            CurrentEnemy = id;
            EnemyHP = Enemies[CurrentEnemy].MaxHP;
            PlayerHP = PlayerMaxHP;
            Log.Info("EnemySelectEx Initiated. Enemy ID: " + id);

            if (!Settings.Tutorial.Default.Tut05 && Settings.Tutorial.Default.Tut04)
            {
                TutorialTitle = "Your first battle";
                TutorialDescription = "The " + Enemies[CurrentEnemy].Name + " is able to deal " + GetEnemyDPS(CurrentEnemy) + " Damage per Second. Click the enemy to attack and kill it before it manages to drain all of your health!";
                TutorialPosition = "Left";
                DamageTimer.Enabled = false;
                TutorialBoolEnemy = true;
                TutorialBoolEnemy = false;
                DamageTimer.Enabled = true;
                Settings.Tutorial.Default.Tut05 = true;
            }
        }

        private ICommand enemyAttack;
        public ICommand EnemyAttack
        {
            get
            {
                if (enemyAttack == null)
                {
                    enemyAttack = new RelayCommand(param => this.EnemyAttackEx(), null);
                }
                return enemyAttack;
            }
        }
        private void EnemyAttackEx()
        {
            Log.Info("EnemyAttackEx Started.");
            if (CurrentEnemy == 0) { return; }
            System.Threading.ThreadPool.QueueUserWorkItem(delegate { ManualAttack(); }, null);
            if (!Settings.Tutorial.Default.Tut08 && PlayerGP >= 500)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    LeftTabIndex = 1;
                    TutorialTitle = "Armoury";
                    TutorialDescription = "You've now made enough gold to spend afford some items in the armoury! Why not find a suitable weapon you can afford and try it out?";
                    TutorialPosition = "Right";
                    DamageTimer.Enabled = false;
                    TutorialBoolLeftPanel = true;
                    TutorialBoolLeftPanel = false;
                    DamageTimer.Enabled = true;
                    Settings.Tutorial.Default.Tut08 = true;
                }));
            }
        }
        #endregion

        #region Armoury Commands

        private ICommand searchArmouryButton;
        public ICommand SearchArmouryButton
        {
            get
            {
                if (searchArmouryButton == null)
                {
                    searchArmouryButton = new RelayCommand(param => this.SearchArmouryButtonEx(), null);
                }
                return searchArmouryButton;
            }
        }
        private void SearchArmouryButtonEx()
        {
            Log.Info("SearchArmouryButtonEx Started");
            GroupedWeapons.Refresh();
            GroupedArmours.Refresh();
        }

        private ICommand armouryMinLevelUp;
        public ICommand ArmouryMinLevelUp
        {
            get
            {
                if (armouryMinLevelUp == null)
                {
                    armouryMinLevelUp = new RelayCommand(param => this.ArmouryMinLevelUpEx(), null);
                }
                return armouryMinLevelUp;
            }
        }
        private void ArmouryMinLevelUpEx()
        {
            ArmouryMinLevel++;
        }

        private ICommand armouryMinLevelDown;
        public ICommand ArmouryMinLevelDown
        {
            get
            {
                if (armouryMinLevelDown == null)
                {
                    armouryMinLevelDown = new RelayCommand(param => this.ArmouryMinLevelDownEx(), null);
                }
                return armouryMinLevelDown;
            }
        }
        private void ArmouryMinLevelDownEx()
        {
            ArmouryMinLevel--;
        }

        private ICommand armouryMaxLevelUp;
        public ICommand ArmouryMaxLevelUp
        {
            get
            {
                if (armouryMaxLevelUp == null)
                {
                    armouryMaxLevelUp = new RelayCommand(param => this.ArmouryMaxLevelUpEx(), null);
                }
                return armouryMaxLevelUp;
            }
        }
        private void ArmouryMaxLevelUpEx()
        {
            ArmouryMaxLevel++;
        }

        private ICommand armouryMaxLevelDown;
        public ICommand ArmouryMaxLevelDown
        {
            get
            {
                if (armouryMaxLevelDown == null)
                {
                    armouryMaxLevelDown = new RelayCommand(param => this.ArmouryMaxLevelDownEx(), null);
                }
                return armouryMaxLevelDown;
            }
        }
        private void ArmouryMaxLevelDownEx()
        {
            ArmouryMaxLevel--;
        }

        private ICommand armouryBuyWeapon;
        public ICommand ArmouryBuyWeapon
        {
            get
            {
                if (armouryBuyWeapon == null)
                {
                    armouryBuyWeapon = new RelayCommand(param => this.ArmouryBuyWeaponEx(Convert.ToInt32(param)), null);
                }
                return armouryBuyWeapon;
            }
        }
        private void ArmouryBuyWeaponEx(int id)
        {
            if (Weapons[id].Cost <= PlayerGP)
            {
                MessageBoxResult check = System.Windows.MessageBox.Show("Purchasing this weapon will replace your current weapon.\nAre you sure you want to purchase this?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                if (check == MessageBoxResult.Yes)
                {
                    switch (Weapons[id].EquipmentSlot)
                    {
                        case Equipment.Slot.Aura:
                            CurrentAuraSlot = id;
                            AuraSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Head:
                            CurrentPocketSlot = id;
                            HeadSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Pocket:
                            CurrentPocketSlot = id;
                            PocketSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Cape:
                            CurrentCapeSlot = id;
                            CapeSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Neck:
                            CurrentNeckSlot = id;
                            NeckSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Ammo:
                            CurrentAmmoSlot = id;
                            AmmoSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Weapon:
                            CurrentWeaponSlot = id;
                            WeaponSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Body:
                            CurrentBodySlot = id;
                            BodySlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Shield:
                            CurrentShieldSlot = id;
                            ShieldSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Legs:
                            CurrentLegsSlot = id;
                            LegsSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Hands:
                            CurrentHandsSlot = id;
                            HandsSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Feet:
                            CurrentFeetSlot = id;
                            FeetSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Ring:
                            CurrentRingSlot = id;
                            RingSlotType = Player.SlotType.Weapon;
                            break;
                        case Equipment.Slot.Twohand:
                            CurrentWeaponSlot = id;
                            CurrentShieldSlot = -1;
                            WeaponSlotType = Player.SlotType.Weapon;
                            ShieldSlotType = Player.SlotType.Weapon;
                            break;
                        default:
                            Log.Error("ArmouryBuyWeaponEx FAILED the switch! No default option available. Weapon ID: " + id);
                            break;
                    }
                    PlayerGP -= Weapons[id].Cost;
                }
                else
                {
                    Log.Debug("Weapon purchase canceled.");
                }
            }
            else
            {
                Log.Debug("Not enough Gold to purchase this weapon.");
                Toast t = new Toast("Not enough Gold.", "You don't have enough gold to purchase that weapon.");
            }
        }

        private ICommand armouryBuyArmour;
        public ICommand ArmouryBuyArmour
        {
            get
            {
                if (armouryBuyArmour == null)
                {
                    armouryBuyArmour = new RelayCommand(param => this.ArmouryBuyArmourEx(Convert.ToInt32(param)), null);
                }
                return armouryBuyArmour;
            }
        }
        private void ArmouryBuyArmourEx(int id)
        {
            if (Armours[id].Cost <= PlayerGP)
            {
                MessageBoxResult check = System.Windows.MessageBox.Show("Purchasing this armour will replace your current armour in " + Armours[id].SlotString + " slot.\nAre you sure you want to purchase this?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                if (check == MessageBoxResult.Yes)
                {
                    switch (Armours[id].EquipmentSlot)
                    {
                        case Equipment.Slot.Aura:
                            CurrentAuraSlot = id;
                            AuraSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Head:
                            CurrentPocketSlot = id;
                            HeadSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Pocket:
                            CurrentPocketSlot = id;
                            PocketSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Cape:
                            CurrentCapeSlot = id;
                            CapeSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Neck:
                            CurrentNeckSlot = id;
                            NeckSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Ammo:
                            CurrentAmmoSlot = id;
                            AmmoSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Weapon:
                            CurrentWeaponSlot = id;
                            WeaponSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Body:
                            CurrentBodySlot = id;
                            BodySlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Shield:
                            CurrentShieldSlot = id;
                            ShieldSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Legs:
                            CurrentLegsSlot = id;
                            LegsSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Hands:
                            CurrentHandsSlot = id;
                            HandsSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Feet:
                            CurrentFeetSlot = id;
                            FeetSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Ring:
                            CurrentRingSlot = id;
                            RingSlotType = Player.SlotType.Armour;
                            break;
                        case Equipment.Slot.Twohand:
                            CurrentWeaponSlot = id;
                            CurrentShieldSlot = -1;
                            WeaponSlotType = Player.SlotType.Armour;
                            ShieldSlotType = Player.SlotType.Armour;
                            break;
                        default:
                            Log.Error("ArmouryBuyArmourEx FAILED the switch! No default option available. Weapon ID: " + id);
                            break;
                    }
                    PlayerGP -= Armours[id].Cost;
                }
                else
                {
                    Log.Debug("Weapon purchase canceled.");
                }
            }
            else
            {
                Log.Debug("Not enough gold to purchase this Armour.");
                Toast t = new Toast("Not enough Gold.", "You don't have enough gold to purchase that weapon.");
            }
        }

        private ICommand unequipItem;
        public ICommand UnequipItem
        {
            get
            {
                if (unequipItem == null)
                {
                    unequipItem = new RelayCommand(param => this.UnequipItemEx(param.ToString()), null);
                }
                return unequipItem;
            }
        }
        private void UnequipItemEx(string slot)
        {
            switch (slot)
            {
                case "Aura":
                    if (CurrentAuraSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Aura item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Aura being removed.");
                            CurrentAuraSlot = -1;
                        }
                    }
                    break;
                case "Head":
                    if (CurrentHeadSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Head item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Head being removed.");
                            CurrentHeadSlot = -1;
                        }
                    }
                    break;
                case "Pocket":
                    if (CurrentPocketSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Pocket item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Pocket being removed.");
                            CurrentPocketSlot = -1;
                        }
                    }
                    break;
                case "Cape":
                    if (CurrentCapeSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Cape item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Cape being removed.");
                            CurrentCapeSlot = -1;
                        }
                    }
                    break;
                case "Neck":
                    if (CurrentNeckSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Neck item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Neck being removed.");
                            CurrentNeckSlot = -1;
                        }
                    }
                    break;
                case "Ammo":
                    if (CurrentAmmoSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Ammo item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Ammo being removed.");
                            CurrentAmmoSlot = -1;
                        }
                    }
                    break;
                case "Weapon":
                    if (CurrentWeaponSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Weapon item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Weapon being removed.");
                            CurrentWeaponSlot = -1;
                        }
                    }
                    break;
                case "Body":
                    if (CurrentBodySlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Body item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Body being removed.");
                            CurrentBodySlot = -1;
                        }
                    }
                    break;
                case "Shield":
                    if (CurrentShieldSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Shield item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Shield being removed.");
                            CurrentShieldSlot = -1;
                        }
                    }
                    break;
                case "Legs":
                    if (CurrentLegsSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Legs item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Legs being removed.");
                            CurrentLegsSlot = -1;
                        }
                    }
                    break;
                case "Hands":
                    if (CurrentHandsSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Hands item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Hands being removed.");
                            CurrentHandsSlot = -1;
                        }
                    }
                    break;
                case "Feet":
                    if (CurrentFeetSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Feet item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Feet being removed.");
                            CurrentFeetSlot = -1;
                        }
                    }
                    break;
                case "Ring":
                    if (CurrentRingSlot >= 0)
                    {
                        MessageBoxResult check = MessageBox.Show("Removing an item will destroy it forever. Are you sure you want to remove your Ring item?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                        if (check == MessageBoxResult.Yes)
                        {
                            Log.Debug("Ring being removed.");
                            CurrentRingSlot = -1;
                        }
                    }
                    break;
                default:
                    Log.Error("UnequipItemEx Switch fell through. Value: " + slot);
                    break;
            }



        }

        private ICommand armouryPopupTooltip;
        public ICommand ArmouryPopupTooltip
        {
            get
            {
                if (armouryPopupTooltip == null)
                {
                    armouryPopupTooltip = new RelayCommand(ArmouryPopupTooltipEx, null);
                }
                return armouryPopupTooltip;
            }
        }
        private void ArmouryPopupTooltipEx(object p)
        {
            var values = (object[])p;

            if (!ArmouryTooltipOpen)
            {
                ArmouryTooltipOpen = true;
            }

            Point currentpos = Mouse.GetPosition((IInputElement)view);

            ArmouryOffsetX = currentpos.X - (Convert.ToDouble(values[0]) / 2);
            ArmouryOffsetY = currentpos.Y + 22;

            CurrentHoveredArmouryItem = values[1].ToString();
            OnPropertyChanged("CurrentArmouryItemName");
            OnPropertyChanged("CurrentArmouryItemExamine");
            OnPropertyChanged("CurrentArmouryItemStat1");
            OnPropertyChanged("CurrentArmouryItemStat2");
            OnPropertyChanged("CurrentArmouryItemStat1Value");
            OnPropertyChanged("CurrentArmouryItemStat2Value");
            //System.Windows.Forms.MessageBox.Show(values[1].ToString());
        }

        private ICommand armouryPopupTooltipLeave;
        public ICommand ArmouryPopupTooltipLeave
        {
            get
            {
                if (armouryPopupTooltipLeave == null)
                {
                    armouryPopupTooltipLeave = new RelayCommand(param => this.ArmouryPopupTooltipLeaveEx(), null);
                }
                return armouryPopupTooltipLeave;
            }
        }
        private void ArmouryPopupTooltipLeaveEx()
        {
            ArmouryTooltipOpen = false;
        }

        private ICommand armourySlotSearch;
        public ICommand ArmourySlotSearch
        {
            get
            {
                if (armourySlotSearch == null)
                {
                    armourySlotSearch = new RelayCommand(param => this.ArmourySlotSearchEx(param.ToString()), null);
                }
                return armourySlotSearch;
            }
        }
        private void ArmourySlotSearchEx(string slot)
        {
            switch (slot)
            {
                case "Aura":
                    SearchAura = !SearchAura;
                    break;
                case "Head":
                    SearchHead = !SearchHead;
                    break;
                case "Pocket":
                    SearchPocket = !SearchPocket;
                    break;
                case "Cape":
                    SearchCape = !SearchCape;
                    break;
                case "Neck":
                    SearchNeck = !SearchNeck;
                    break;
                case "Ammo":
                    SearchAmmo = !SearchAmmo;
                    break;
                case "Weapon":
                    SearchWeapon = !SearchWeapon;
                    break;
                case "Body":
                    SearchBody = !SearchBody;
                    break;
                case "Shield":
                    SearchShield = !SearchShield;
                    break;
                case "Legs":
                    SearchLegs = !SearchLegs;
                    break;
                case "Hands":
                    SearchHands = !SearchHands;
                    break;
                case "Feet":
                    SearchFeet = !SearchFeet;
                    break;
                case "Ring":
                    SearchRing = !SearchRing;
                    break;
                default:
                    Log.Error("ArmourySlotSearchEx switch fell through. Slot used: " + slot);
                    break;
            }
        }

        #endregion

        #region Tutorial Commands

        private ICommand windowLoaded;
        public ICommand WindowLoaded
        {
            get
            {
                if (windowLoaded == null)
                {
                    windowLoaded = new RelayCommand(WindowLoadedEx, null);
                }
                return windowLoaded;
            }
        }
        private async void WindowLoadedEx(object p)
        {
            if (idleEnemy > 0 && idleKills > 0)
            {
                int GPGain = 0;
                int SharedXPGain = 0;
                int ConstitutionXPGain = 0;

                GPGain = enemies[idleEnemy].MinGP * idleKills;
                SharedXPGain = Convert.ToInt32(enemies[idleEnemy].SplitXP * idleKills / 4);
                ConstitutionXPGain = Convert.ToInt32(enemies[idleEnemy].HPXP * idleKills / 4);

                TutorialTitle = "Whilst you were away...";
                TutorialDescription = "Welcome back! Whilst you were away, you gained " + GPGain + " GP, " + SharedXPGain + " Split Combat XP and " + ConstitutionXPGain + " Constitution XP!";
                TutorialPosition = "Top";
                DamageTimer.Enabled = false;
                TutorialBoolStatusBar = true;
                TutorialBoolStatusBar = false;
                DamageTimer.Enabled = true;

                System.Threading.ThreadPool.QueueUserWorkItem(delegate
                {
                    PlayerGP += GPGain;
                    ConstitutionXP += ConstitutionXPGain;
                    AttackXP += SharedXPGain / 5;
                    StrengthXP += SharedXPGain / 5;
                    DefenceXP += SharedXPGain / 5;
                    RangeXP += SharedXPGain / 5;
                    MagicXP += SharedXPGain / 5;
                }, null);
            }

            if (!Settings.Tutorial.Default.Tut01)
            {
                TutorialTitle = "Welcome to RSClick! (RSIdle)";
                TutorialDescription = "Welcome to RSClick! These tutorial popups will aim to guide you through what you need to know to become the greatest adventurer in the land!";
                TutorialPosition = "Top";
                DamageTimer.Enabled = false;
                TutorialBoolStatusBar = true;
                TutorialBoolStatusBar = false;
                DamageTimer.Enabled = true;
                Settings.Tutorial.Default.Tut01 = true;
            }
            if (!Settings.Tutorial.Default.Tut02)
            {
                TutorialTitle = "Character & Stats Panels";
                TutorialDescription = "The left panel houses your character information and stats. Find information here for your levels, equipment and summoned monsters.";
                TutorialPosition = "Right";
                DamageTimer.Enabled = false;
                TutorialBoolLeftPanel = true;
                TutorialBoolLeftPanel = false;
                DamageTimer.Enabled = true;
                Settings.Tutorial.Default.Tut02 = true;
            }
            if (!Settings.Tutorial.Default.Tut03)
            {
                TutorialTitle = "Main Panel";
                TutorialDescription = "The right panel is home to the main battlefield. Select an enemy you wish to attack at the top, view their stats and click to attack!";
                TutorialPosition = "Left";
                DamageTimer.Enabled = false;
                TutorialBoolRightPanel = true;
                TutorialBoolRightPanel = false;
                DamageTimer.Enabled = true;
                Settings.Tutorial.Default.Tut03 = true;
            }
            if (!Settings.Tutorial.Default.Tut04)
            {
                TutorialTitle = "Select your first enemy";
                TutorialDescription = "You are currently in a safe zone. Select the " + Enemies[1].Name + " to battle!";
                TutorialPosition = "Bottom";
                DamageTimer.Enabled = false;
                TutorialBoolEnemySelect = true;
                TutorialBoolEnemySelect = false;
                DamageTimer.Enabled = true;
                Settings.Tutorial.Default.Tut04 = true;
            }

            if (ParseDB.ParseUser != null)
            {
                if (((DateTime)ParseDB.ParseUser["WildernessExit"] > (DateTime)ParseDB.ParseUser["WildernessEntry"]) && !(bool)ParseDB.ParseUser["WildernessAwarded"])
                {
                    WildernessLeaveTimer.Start();
                }

                if ((bool)ParseDB.ParseUser["Wilderness"] && !((DateTime)ParseDB.WildernessExit > (DateTime)ParseDB.WildernessEntry))
                {
                    await ParseDB.GrantReward();
                    WildernessEntryTimer.Start();
                }
                OnPropertyChanged("WildernessRewardTotal");
            }
            else
            {
                Log.Error("ParseUser was null??");
                WindowLoadedTimer.Start();
            }

            OnPropertyChanged("DisplayWildyOutside");
            OnPropertyChanged("DisplayWildyInside");
        }
        private async void WindowLoadedTimerTick(object sender, ElapsedEventArgs e)
        {
            if (ParseDB.ParseUser != null)
            {
                if (((DateTime)ParseDB.ParseUser["WildernessExit"] > (DateTime)ParseDB.ParseUser["WildernessEntry"]) && !(bool)ParseDB.ParseUser["WildernessAwarded"])
                {
                    WildernessLeaveTimer.Start();
                }

                if ((bool)ParseDB.ParseUser["Wilderness"] && !((DateTime)ParseDB.WildernessExit > (DateTime)ParseDB.WildernessEntry))
                {
                    await ParseDB.GrantReward();
                    WildernessEntryExtras = true;
                    WildernessEntryTimer.Start();
                }
                OnPropertyChanged("WildernessRewardTotal");
                WindowLoadedTimer.Stop();
            }
            else
            {
                Log.Error("ParseUser was null??");
            }
        }

        private ICommand helpCharacter;
        public ICommand HelpCharacter
        {
            get
            {
                if (helpCharacter == null)
                {
                    helpCharacter = new RelayCommand(HelpCharacterEx, null);
                }
                return helpCharacter;
            }
        }
        private void HelpCharacterEx(object p)
        {
            TutorialTitle = "Character Panel";
            TutorialDescription = "This is the character panel. It contains all sorts of information relating to your characters combat stats and stances.";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolLeftPanel = true;
            TutorialBoolLeftPanel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Player GP";
            TutorialDescription = "Your GP is your currency. Spend this on better weapons, better armour, and training & practicing your summoning!";
            TutorialPosition = "Bottom";
            DamageTimer.Enabled = false;
            TutorialBoolGP = true;
            TutorialBoolGP = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Combat Level";
            TutorialDescription = "Your combat level is calculated based upon your combat stats below. This will be important when entering the wilderness!";
            TutorialPosition = "Bottom";
            DamageTimer.Enabled = false;
            TutorialBoolCombatLevel = true;
            TutorialBoolCombatLevel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Attack Level";
            TutorialDescription = "Your attack level increases your critical hit rate whilst using melee! The higher your crit rate, the more frequently you willdeal a hugely damaging blow to your enemy!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolAttackLevel = true;
            TutorialBoolAttackLevel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Strength Level";
            TutorialDescription = "Your strength level increases your minimum and maximum hits for manual attacks whilst using melee! Level up strength to deal more damaging blows!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolStrengthLevel = true;
            TutorialBoolStrengthLevel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Defence Level";
            TutorialDescription = "Your defence level reduces the damage you take from enemy hits. A higher defence means you have more time to hit that killing blow on the enemy!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolDefenceLevel = true;
            TutorialBoolDefenceLevel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Constitution Level";
            TutorialDescription = "Your constitution level increases your maximum lifepoints! The higher your lifepoints, the longer you will be able to last in battle!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolConstitutionLevel = true;
            TutorialBoolConstitutionLevel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Range Level";
            TutorialDescription = "Your range level increases both your minimum & maxmimum hits, and your critical hit rate whilst using ranged combat!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolRangeLevel = true;
            TutorialBoolRangeLevel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Magic Level";
            TutorialDescription = "Your magic level increases both your minimum & maxmimum hits, and your critical hit rate whilst using magic combat!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolMagicLevel = true;
            TutorialBoolMagicLevel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Combat Style";
            TutorialDescription = "The combat style you select is important! Different enemies are weak to different combat types, and each style awards exp to different combat skills!";
            TutorialPosition = "Bottom";
            DamageTimer.Enabled = false;
            TutorialBoolCombatStyle = true;
            TutorialBoolCombatStyle = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Player HP";
            TutorialDescription = "This is your Health! You will start off with a full bar of health, but make sure not to allow an enemy to kill you! When an enemy dies or you change enemy, you will refill your HP.";
            TutorialPosition = "Bottom";
            DamageTimer.Enabled = false;
            TutorialBoolPlayerHP = true;
            TutorialBoolPlayerHP = false;
            DamageTimer.Enabled = true;
        }

        private ICommand helpArmoury;
        public ICommand HelpArmoury
        {
            get
            {
                if (helpArmoury == null)
                {
                    helpArmoury = new RelayCommand(HelpArmouryEx, null);
                }
                return helpArmoury;
            }
        }
        private void HelpArmouryEx(object p)
        {
            TutorialTitle = "Armoury Panel";
            TutorialDescription = "This is the armoury panel. It contains all your armour and weapon information, as well as a shop to purchase more or better items!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolLeftPanel = true;
            TutorialBoolLeftPanel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Armour Stats";
            TutorialDescription = "These are the stats generated by all your armour and weapons.\nDamage & accuracy effect the amount you hit as well as your critical hit rate.\nArmour and Lifepoint Boost effect the amount of damage you recieve and how much damage you are able to recieve.\nAll the bonus stats provide bonuses to other areas involved in combat.";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolArmouryLeftStats = true;
            TutorialBoolArmouryLeftStats = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Damage Stats";
            TutorialDescription = "These are the stats which show you how much you might be able to hit in combat. These are modified further by your combat stance, enemy weakness etc but are a good guide. minimum and maximum hit are the lowest and highest values you might hit in any one attack.\nCritical hit is how much you will hit when you manage to critically hit, and crit hit chance is how often you will hit the crit hit!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolArmouryRightStats = true;
            TutorialBoolArmouryRightStats = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Equipment Slots";
            TutorialDescription = "These are the stats which show you how much you might be able to hit in combat. These are modified further by your combat stance, enemy weakness etc but are a good guide. minimum and maximum hit are the lowest and highest values you might hit in any one attack.\nCritical hit is how much you will hit when you manage to critically hit, and crit hit chance is how often you will hit the crit hit!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolArmourySlots = true;
            TutorialBoolArmourySlots = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Search";
            TutorialDescription = "Use the fields here to search for the weapon or armour of your choosing!";
            TutorialPosition = "Bottom";
            DamageTimer.Enabled = false;
            TutorialBoolArmourySearch = true;
            TutorialBoolArmourySearch = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Items";
            TutorialDescription = "Your weapons and armour are listed here, and bundled into categories. take a look through here and decide what items to equip!";
            TutorialPosition = "Top";
            DamageTimer.Enabled = false;
            TutorialBoolArmouryItems = true;
            TutorialBoolArmouryItems = false;
            DamageTimer.Enabled = true;
        }

        private ICommand helpSummoning;
        public ICommand HelpSummoning
        {
            get
            {
                if (helpSummoning == null)
                {
                    helpSummoning = new RelayCommand(HelpSummoningEx, null);
                }
                return helpSummoning;
            }
        }
        private void HelpSummoningEx(object p)
        {
            TutorialTitle = "Summoning Panel";
            TutorialDescription = "This is the summoning panel. It contains information about your summoning level, a choice of methods to train your summoning and also the ability to purchase summoning pouches! Each pouch you purchase will deal damage over time to the selected enemy, and even whilst the game is closed! Get building your army of familiars!";
            TutorialPosition = "Right";
            DamageTimer.Enabled = false;
            TutorialBoolLeftPanel = true;
            TutorialBoolLeftPanel = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Summoning Training";
            TutorialDescription = "To train your summoning, you have a choice of 4 methods - using your gold, green, crimson or blue charms. Each charm will have a different cost per use, and a different return in exp associated with that. Kepe watch though - as you level up, these values will change! So what may be your favourite method for levelling one day, might change the next!";
            TutorialPosition = "Bottom";
            DamageTimer.Enabled = false;
            TutorialBoolSummoningCharms = true;
            TutorialBoolSummoningCharms = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Summoning Level";
            TutorialDescription = "This shows you your current summoning level, and your progress to the next level. You can only buy some summoning pouches when you have the level requirement for them - So work on getting your summoning trained higher to open up more possibilities!";
            TutorialPosition = "Bottom";
            DamageTimer.Enabled = false;
            TutorialBoolSummoningLevels = true;
            TutorialBoolSummoningLevels = false;
            DamageTimer.Enabled = true;

            TutorialTitle = "Summoning Pouches";
            TutorialDescription = "This area is where you can buy your summoning pouches! Costs are scaling, but each pouch you buy will deal damage each second to your current enemy! They will even fight for you whilst you are away or the game is closed!";
            TutorialPosition = "Top";
            DamageTimer.Enabled = false;
            TutorialBoolSummoningPouches = true;
            TutorialBoolSummoningPouches = false;
            DamageTimer.Enabled = true;
        }

        #endregion

        #region Summoning Commands

        private ICommand summoningTraining;
        public ICommand SummoningTraining
        {
            get
            {
                if (summoningTraining == null)
                {
                    summoningTraining = new RelayCommand(SummoningTrainingEx, null);
                }
                return summoningTraining;
            }
        }
        private void SummoningTrainingEx(object p)
        {
            System.Threading.ThreadPool.QueueUserWorkItem(delegate { SummoningTrainingDel(p); }, null);
        }
        private void SummoningTrainingDel(object p)
        {
            int cost;
            double xp;

            switch ((string)p)
            {
                case "Gold":
                    cost = GoldCharmCost;
                    xp = GoldCharmXP;
                    break;
                case "Green":
                    cost = GreenCharmCost;
                    xp = GreenCharmXP;
                    break;
                case "Crimson":
                    cost = CrimsonCharmCost;
                    xp = CrimsonCharmXP;
                    break;
                case "Blue":
                    cost = BlueCharmCost;
                    xp = BlueCharmXP;
                    break;
                default:
                    cost = 0;
                    xp = 0;
                    Log.Error("SummoningTrainingEx Fell through switch on charms. Value used: " + (string)p);
                    break;
            }

            if (cost == 0)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    Toast t = new Toast("Unavailable!", "Sorry, the " + (string)p + " charm is not currently available. Level up and it will become available!", NotificationType.Information);
                }));
                return;
            }
            if (cost > PlayerGP)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    Toast t = new Toast("Insufficient Funds", "You do not have the funds for that purchase! Try a cheaper charm, or hunt for more gold from enemies!", NotificationType.Warning);
                }));
                return;
            }
            PlayerGP -= cost;
            SummoningXP += xp;
        }

        private ICommand summoningQuantityChange;
        public ICommand SummoningQuantityChange
        {
            get
            {
                if (summoningQuantityChange == null)
                {
                    summoningQuantityChange = new RelayCommand(SummoningQuantityChangeEx, null);
                }
                return summoningQuantityChange;
            }
        }
        private void SummoningQuantityChangeEx(object p)
        {
            switch (SummoningBuyQuantity)
            {
                case 1:
                    SummoningBuyQuantity = 10;
                    break;
                case 10:
                    SummoningBuyQuantity = 25;
                    break;
                case 25:
                    SummoningBuyQuantity = 100;
                    break;
                case 100:
                    SummoningBuyQuantity = 1;
                    break;
                default:
                    Log.Error("SummoningBuyQuantity unexpected value. Call: SummoningQuantityChangeEx. Value: " + SummoningBuyQuantity);
                    SummoningBuyQuantity = 1;
                    break;
            }
        }

        private ICommand summoningPurchase;
        public ICommand SummoningPurchase
        {
            get
            {
                if (summoningPurchase == null)
                {
                    summoningPurchase = new RelayCommand(param => SummoningPurchaseEx(Convert.ToInt32(param)), null);
                }
                return summoningPurchase;
            }
        }
        private void SummoningPurchaseEx(int id)
        {
            long cost = 0;
            switch (familiars[id].BuyQuantity)
            {
                case 1:
                    cost = familiars[id].Next1Cost;
                    break;
                case 10:
                    cost = familiars[id].Next10Cost;
                    break;
                case 25:
                    cost = familiars[id].Next25Cost;
                    break;
                case 100:
                    cost = familiars[id].Next100Cost;
                    break;
                default:
                    Log.Error("SummonungPurchaseEx - BuyQuantity fell through. Value: " + familiars[id].BuyQuantity);
                    cost = familiars[id].Next1Cost;
                    break;
            }

            if (cost < PlayerGP)
            {
                System.Threading.ThreadPool.QueueUserWorkItem(delegate
                {
                    PlayerGP -= cost;
                    familiars[id].Quantity += familiars[id].BuyQuantity;
                }, null);
            }
            else
            {
                Toast t = new Toast("Not enough GP!", "You don't have enough gold to make that purchase!", NotificationType.Information);
            }
        }

        #endregion

        #region Wilderness Commands

        private ICommand wildernessEntry;
        public ICommand WildernessEntry
        {
            get
            {
                if (wildernessEntry == null)
                {
                    wildernessEntry = new RelayCommand(WildernessEntryEx, null);
                }
                return wildernessEntry;
            }
        }
        private async void WildernessEntryEx(object p)
        {
            MessageBoxResult check = System.Windows.MessageBox.Show("The wilderness is a dangerous area! Other players will be able to attack you whilst you are within the wilderness! If you pass level 20, you will also not be able to leave immediately! Are you sure you want to enter the wilderness?", "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
            if (check == MessageBoxResult.Yes)
            {
                ParseDB.Wilderness = true;
                ParseDB.WildernessEntry = DateTime.UtcNow;
                ParseDB.WildernessAwarded = false;
                ParseDB.WildernessLevel = WildernessEntryLevel;
                await ParseDB.UpdateDB();
                WildernessEntryTimer.Start();
                OnPropertyChanged("DisplayWildyOutside");
                OnPropertyChanged("DisplayWildyInside");
                OnPropertyChanged("WildernessShowEntry");
                OnPropertyChanged("WildernessEntryLevel");
                OnPropertyChanged("CurrentWildernessLevel");
                OnPropertyChanged("WildernessMoveMinimum");
                await ParseDB.GetPlayerList();
                OnPropertyChanged("WildernessPlayerList");
                OnPropertyChanged("IsWildernessPlayerListEmpty");
            }
        }

        private ICommand wildernessMove;
        public ICommand WildernessMove
        {
            get
            {
                if (wildernessMove == null)
                {
                    wildernessMove = new RelayCommand(WildernessMoveEx, null);
                }
                return wildernessMove;
            }
        }
        private async void WildernessMoveEx(object p)
        {
            await ParseDB.GrantReward();
            //ParseDB.UpdateDB();
            OnPropertyChanged("WildernessMoveMinimum");
            OnPropertyChanged("CurrentWildernessLevel");
            await ParseDB.GetPlayerList();
            OnPropertyChanged("WildernessPlayerList");
            OnPropertyChanged("IsWildernessPlayerListEmpty");
        }

        private ICommand wildernessLeave;
        public ICommand WildernessLeave
        {
            get
            {
                if (wildernessLeave == null)
                {
                    wildernessLeave = new RelayCommand(WildernessLeaveEx, null);
                }
                return wildernessLeave;
            }
        }
        private async void WildernessLeaveEx(object p)
        {
            if (Convert.ToInt32(ParseDB.ParseUser["WildernessLevel"]) >= 20)
            {
                ParseDB.WildernessExit = DateTime.UtcNow + TimeSpan.FromMinutes(1);
                await ParseDB.GrantReward();
                //ParseDB.UpdateDB();
                OnPropertyChanged("DisplayWildyOutside");
                OnPropertyChanged("DisplayWildyInside");
                OnPropertyChanged("WildernessShowEntry");
                OnPropertyChanged("WildernessEntryLevel");
                OnPropertyChanged("WildernessLeaveButtonText");
                OnPropertyChanged("WildernessLeaveButtonEnabled");
                WildernessLeaveTimer.Start();

            }
            else
            {

                ParseDB.RewardReward();
                OnPropertyChanged("WildernessShowReward");

                ParseDB.WildernessExit = DateTime.UtcNow;
                await ParseDB.GrantReward();
                ParseDB.Wilderness = false;
                ParseDB.WildernessLevel = 0;
                ParseDB.UpdateDB();
                WildernessEntryTimer.Stop();
                //ParseDB.UpdateDB();
                OnPropertyChanged("DisplayWildyOutside");
                OnPropertyChanged("DisplayWildyInside");
                OnPropertyChanged("WildernessEntryLevel");
                OnPropertyChanged("WildernessLeaveButtonEnabled");
                OnPropertyChanged("WildernessShowEntry");

                OnPropertyChanged("WildernessAttackXPReward");
                OnPropertyChanged("WildernessStrengthXPReward");
                OnPropertyChanged("WildernessConstitutionXPReward");
                OnPropertyChanged("WildernessMagicXPReward");
                OnPropertyChanged("WildernessRangeXPReward");
                OnPropertyChanged("WildernessDefenceXPReward");
                OnPropertyChanged("WildernessSummoningXPReward");
                OnPropertyChanged("WildernessGPReward");
            }
        }

        private async void WildernessTimerTick(object sender, ElapsedEventArgs e)
        {
            if (ParseDB.WildernessExit <= DateTime.UtcNow)
            {
                WildernessLeaveTimer.Stop();
                ParseDB.Wilderness = false;
                ParseDB.WildernessLevel = 0;
                //ParseDB.GrantReward();
                await ParseDB.UpdateDB();
                WildernessEntryTimer.Stop();


                ParseDB.RewardReward();
                OnPropertyChanged("WildernessShowReward");

                OnPropertyChanged("WildernessAttackXPReward");
                OnPropertyChanged("WildernessStrengthXPReward");
                OnPropertyChanged("WildernessConstitutionXPReward");
                OnPropertyChanged("WildernessMagicXPReward");
                OnPropertyChanged("WildernessRangeXPReward");
                OnPropertyChanged("WildernessDefenceXPReward");
                OnPropertyChanged("WildernessSummoningXPReward");
                OnPropertyChanged("WildernessGPReward");
            }

            OnPropertyChanged("WildernessRemainingTime");
            OnPropertyChanged("DisplayWildyOutside");
            OnPropertyChanged("DisplayWildyInside");
            OnPropertyChanged("WildernessEntryLevel");
            OnPropertyChanged("WildernessLeaveButtonText");
            OnPropertyChanged("WildernessLeaveButtonEnabled");
            OnPropertyChanged("WildernessShowEntry");
        }
        private void WildernessEntryTimerTick(object sender, ElapsedEventArgs e)
        {
            OnPropertyChanged("WildernessRewardTotal");

            if (WildernessEntryExtras)
            {
                OnPropertyChanged("DisplayWildyInside");
                OnPropertyChanged("DisplayWildyOutside");
                OnPropertyChanged("WildernessShowEntry");

                OnPropertyChanged("WildernessRewardTotal");
                OnPropertyChanged("WildernessPlayerList");
                OnPropertyChanged("IsWildernessPlayerListEmpty");
                OnPropertyChanged("CurrentWildernessLevel");
                OnPropertyChanged("WildernessMoveMinimum");
                OnPropertyChanged("WildernessEntryLevel");
                
                WildernessEntryExtras = false;
            }
        }

        private ICommand wildernessSpendReward;
        public ICommand WildernessSpendReward
        {
            get
            {
                if (wildernessSpendReward == null)
                {
                    wildernessSpendReward = new RelayCommand(WildernessSpendRewardEx, null);
                }
                return wildernessSpendReward;
            }
        }
        private async void WildernessSpendRewardEx(object p)
        {
            string skill = p.ToString();

            switch (skill)
            {
                case "Attack":
                    AttackXP += WildernessAttackXPReward;
                    ParseDB.WildernessAwarded = true;
                    ParseDB.WildernessReward = 0;
                    await ParseDB.UpdateDB();
                    break;
                case "Strength":
                    StrengthXP += WildernessStrengthXPReward;
                    ParseDB.WildernessAwarded = true;
                    ParseDB.WildernessReward = 0;
                    await ParseDB.UpdateDB();
                    break;
                case "Defence":
                    DefenceXP += WildernessDefenceXPReward;
                    ParseDB.WildernessAwarded = true;
                    ParseDB.WildernessReward = 0;
                    await ParseDB.UpdateDB();
                    break;
                case "Constitution":
                    ConstitutionXP += WildernessConstitutionXPReward;
                    ParseDB.WildernessAwarded = true;
                    ParseDB.WildernessReward = 0;
                    await ParseDB.UpdateDB();
                    break;
                case "Range":
                    RangeXP += WildernessRangeXPReward;
                    ParseDB.WildernessAwarded = true;
                    ParseDB.WildernessReward = 0;
                    await ParseDB.UpdateDB();
                    break;
                case "Magic":
                    MagicXP += WildernessMagicXPReward;
                    ParseDB.WildernessAwarded = true;
                    ParseDB.WildernessReward = 0;
                    await ParseDB.UpdateDB();
                    break;
                case "Summoning":
                    SummoningXP += WildernessSummoningXPReward;
                    ParseDB.WildernessAwarded = true;
                    ParseDB.WildernessReward = 0;
                    await ParseDB.UpdateDB();
                    break;
                case "GP":
                    PlayerGP += WildernessGPReward;
                    ParseDB.WildernessAwarded = true;
                    ParseDB.WildernessReward = 0;
                    await ParseDB.UpdateDB();
                    break;
            }

            ParseDB.ShowReward = false;
            OnPropertyChanged("WildernessShowEntry");
            OnPropertyChanged("WildernessShowReward");
        }

        private ICommand wildernessAttackEnemy;
        public ICommand WildernessAttackEnemy
        {
            get
            {
                if (wildernessAttackEnemy == null)
                {
                    wildernessAttackEnemy = new RelayCommand(WildernessAttackEnemyEx, null);
                }
                return wildernessAttackEnemy;
            }
        }
        private void WildernessAttackEnemyEx(object p)
        {
            WildernessEnemyID = Convert.ToInt32(p);

            WildernessPlayerMaxHealth = ConstitutionLevel * 10;
            WildernessPlayerHealth = ConstitutionLevel * 10;
            WildernessEnemyMaxHealth = WildernessPlayerList[WildernessEnemyID].Constitution * 10;
            WildernessEnemyHealth = WildernessPlayerList[WildernessEnemyID].Constitution * 10;

            WildernessEnemyAttacked = true;

            WildernessCountdownTimer.Start();
        }
        private void WildernessCountdown(object sender, ElapsedEventArgs e)
        {
            switch (WildernessDescText)
            {
                case "4!":
                    WildernessDescText = "3!";
                    break;
                case "3!":
                    WildernessDescText = "2!";
                    break;
                case "2!":
                    WildernessDescText = "1!";
                    break;
                case "1!":
                    WildernessDescText = "Fight!";
                    break;
                case "Fight!":
                    WildernessCountdownTimer.Stop();
                    WildernessBattleTimer.Start();
                    break;
            }
        }
        private void WildernessBattleSequence(object sender, ElapsedEventArgs e)
        {
            if (WildernessPlayersTurn)
            {
                int hit = WildernessDamageCalculater(AttackLevel, StrengthLevel, RangeLevel, MagicLevel, SummoningLevel,
                    WildernessPlayerList[WildernessEnemyID].Defence);

                WildernessEnemyDamaged = hit;
                WildernessEnemyHealth -= hit;
            }
            else
            {
                int hit = WildernessDamageCalculater(WildernessPlayerList[WildernessEnemyID].Attack, WildernessPlayerList[WildernessEnemyID].Strength,
                    WildernessPlayerList[WildernessEnemyID].Range, WildernessPlayerList[WildernessEnemyID].Magic,
                    WildernessPlayerList[WildernessEnemyID].Summoning, DefenceLevel);

                WildernessPlayerDamaged = hit;
                WildernessPlayerHealth -= hit;
            }

            WildernessPlayersTurn = !WildernessPlayersTurn;
        }
        private void WildernessBattleOver(object sender, ElapsedEventArgs e)
        {
            WildernessEnemyAttacked = false;
            WildernessDescText = "4!";
            WildernessBattleOverTimer.Stop();
        }
        private int WildernessDamageCalculater(int att, int str, int range, int mage, int summ, int enemydef)
        {
            int TotalAtt = att + range + mage;
            int TotalStr = str + range + mage;
            int Base = summ;
            double enemydefDouble = Convert.ToDouble(enemydef);
            double AverageAtt = TotalAtt / 3;

            TotalAtt = Math.Max(Convert.ToInt32(TotalAtt / 30), 1);
            TotalStr = rnd.Next(1, TotalStr);

            double hit = TotalStr + Base;
            if (rnd.Next(1, 100) < TotalAtt)
            {
                hit = Convert.ToInt32(hit * 1.5);
            }

            hit = hit * (AverageAtt / enemydefDouble);

            return Convert.ToInt32(Math.Floor(hit));
        }

        #endregion

        #region DEBUG
        public bool IsDebug
        {
            get
            {
#if DEBUG
                return true;
#else
                return false;
#endif
            }
        }
        private bool debugPanel;
        public bool DebugPanel
        {
            get
            {
                return debugPanel;
            }
            set
            {
                debugPanel = value;
                OnPropertyChanged("DebugPanel");
            }
        }
        private string debugSkillSetLevel = "0";
        public string DebugSkillSetLevel
        {
            get
            {
                return debugSkillSetLevel;
            }
            set
            {
                debugSkillSetLevel = value;
                OnPropertyChanged("DebugSkillSetLevel");
            }
        }

        private ICommand openDebug;
        public ICommand OpenDebug
        {
            get
            {
                if (openDebug == null)
                {
                    openDebug = new RelayCommand(param => this.OpenDebugEx(), null);
                }
                return openDebug;
            }
        }
        private void OpenDebugEx()
        {
            DebugPanel = !DebugPanel;
            DebugLog.Debug("Debug Panel Opened/Closed.");
        }

        private ICommand debugReset;
        public ICommand DebugReset
        {
            get
            {
                if (debugReset == null)
                {
                    debugReset = new RelayCommand(param => this.DebugResetEx(), null);
                }
                return debugReset;
            }
        }
        private void DebugResetEx()
        {
            Log.Debug("Debug - Reset Stats");
            DebugLog.Debug("Game Reset");
            AttackXP = 0;
            StrengthXP = 0;
            DefenceXP = 0;
            ConstitutionXP = 1154;
            RangeXP = 0;
            MagicXP = 0;
            PlayerGP = 0;
            CurrentWeaponSlot = -1;
            CurrentEnemy = 0;
            Settings.Tutorial.Default.Tut01 = false;
            Settings.Tutorial.Default.Tut02 = false;
            Settings.Tutorial.Default.Tut03 = false;
            Settings.Tutorial.Default.Tut04 = false;
            Settings.Tutorial.Default.Tut05 = false;
            Settings.Tutorial.Default.Tut06 = false;
            Settings.Tutorial.Default.Tut07 = false;
            Settings.Tutorial.Default.Tut08 = false;
            Settings.Tutorial.Default.Tut09 = false;
            Settings.Tutorial.Default.Tut10 = false;

            Settings.Player.Default.SettingDateTime = DateTime.MinValue;

            Settings.ParseSettings.Default.UserID = "NAN";

            Settings.Tutorial.Default.Save();
            Settings.Player.Default.Save();
            Settings.ParseSettings.Default.Save();

            foreach (Familiar f in Familiars)
            {
                f.Quantity = 0;
            }
        }

        private ICommand debugSkillSet;
        public ICommand DebugSkillSet
        {
            get
            {
                if (debugSkillSet == null)
                {
                    debugSkillSet = new RelayCommand(param => this.DebugSkillSetEx(param.ToString()), null);
                }
                return debugSkillSet;
            }
        }
        private void DebugSkillSetEx(string skill)
        {
            try
            {
                switch (skill)
                {
                    case "Attack":
                        AttackXP = player.XPTable[Convert.ToInt32(DebugSkillSetLevel)];
                        break;
                    case "Strength":
                        StrengthXP = player.XPTable[Convert.ToInt32(DebugSkillSetLevel)];
                        break;
                    case "Defence":
                        DefenceXP = player.XPTable[Convert.ToInt32(DebugSkillSetLevel)];
                        break;
                    case "Range":
                        RangeXP = player.XPTable[Convert.ToInt32(DebugSkillSetLevel)];
                        break;
                    case "Magic":
                        MagicXP = player.XPTable[Convert.ToInt32(DebugSkillSetLevel)];
                        break;
                    case "Constitution":
                        ConstitutionXP = player.XPTable[Convert.ToInt32(DebugSkillSetLevel)];
                        break;
                }
            }
            catch (Exception err)
            {
                System.Windows.Forms.MessageBox.Show(err.Message);
                DebugLog.Error("DebugSkillSetEx Failed - Inappropriatte inputs.", err);
            }
        }

        private ICommand debugAddGold;
        public ICommand DebugAddGold
        {
            get
            {
                if (debugAddGold == null)
                {
                    debugAddGold = new RelayCommand(param => this.DebugAddGoldEx(param.ToString()), null);
                }
                return debugAddGold;
            }
        }
        private void DebugAddGoldEx(string value)
        {
            try
            {
                PlayerGP += Convert.ToInt32(value);
            }
            catch (Exception Err)
            {
                DebugLog.Error("DebugAddGoldEx failed.", Err);
            }
        }

        private ICommand testDebug;
        public ICommand TestDebug
        {
            get
            {
                if (testDebug == null)
                {
                    testDebug = new RelayCommand(param => this.TestDebugEx(), null);
                }
                return testDebug;
            }
        }
        private void TestDebugEx()
        {
            //Window window = new Window()
            //{
            //    Title = "Modal Dialog",
            //    ShowInTaskbar = false,               // don't show the dialog on the taskbar
            //    Topmost = true,                      // ensure we're Always On Top
            //    ResizeMode = ResizeMode.NoResize,    // remove excess caption bar buttons
            //    Owner = Application.Current.MainWindow,
            //};

            //window.ShowDialog();
            //DamageTimer.Enabled = false;
            //DamageTimer.Enabled = true;
            //System.Windows.Forms.MessageBox.Show(Settings.Tutorial.Default.Tut01.ToString());

            if (!Settings.Tutorial.Default.Tut01)
            {
                TutorialTitle = "Welcome to RSClick! (RSIdle)";
                TutorialDescription = "Welcome to RSClick! These tutorial popups will aim to guide you through what you need to know to become the greatest adventurer in the land!";
                TutorialPosition = "Top";
                DamageTimer.Enabled = false;
                TutorialBoolStatusBar = true;
                TutorialBoolStatusBar = false;
                DamageTimer.Enabled = true;
            }
        }

        private ICommand debugDBManagement;
        public ICommand DebugDBManagement
        {
            get
            {
                if (debugDBManagement == null)
                {
                    debugDBManagement = new RelayCommand(DebugDBManagementEx, null);
                }
                return debugDBManagement;
            }
        }
        private void DebugDBManagementEx(object p)
        {
            var x = new DBManagement();
            x.Show();
            view.Close();
        }

        #endregion

        #endregion

        private ProgramVersion programVersion;
        public string ProgramVersion
        {
            get
            {
                return programVersion.Version;
            }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
